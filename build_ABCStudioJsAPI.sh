#
# 
# This file builds the Node js contexts for 
# ABC Music studio 
#
# It must be used for installing the web pages
# Or to build the ABC SVG Viewer based on abcjs
# (see abcjs.net for more information)
#
# - cleanup last build pages 
# - clone gitlab repository 
# - cd to ABCStudioJSAPI
# - yarn install
# - yarn build
# - copy new image to www pages
# - copy la binary to www for downloads
#
# Start Customizing HERE
SOURCEDIR="./abcstudiojsapi"
ABCJSDIR="./abcjsdev"
ABCJSURL="https://github.com/jymen/abcjs.git"
REPOURL="https://gitlab.com/jymen/abcstudiojsapi.git"
INSTALL_LOCATION="$HOME/www/tchoupi.jymengant.org"
DISTRIBUTION_FILE="./distribution/ABCMusicStudio.zip"
# end Customizing HERE
echo "cleaning up ..."
rm -Rf $SOURCEDIR
echo "$SOURCEDIR has been cleaned up"
rm -Rf $ABCJSDIR
echo "$ABCJSDIR has been cleaned up"
echo "cloning repos ..."
git clone $REPOURL
git clone $ABCJSURL $ABCJSDIR
# switch to dev branch on abcjs
cd $HOME/$ABCJSDIR
git checkout dev
echo "repos $REPOURL & $ABCJSDIR now cloned "
#
# Build Pages
#
echo "Installing pages..."
cd $HOME/$SOURCEDIR/pages
yarn install 
echo "installed => building ..."
yarn run build
echo "built => installing ..."
rm -Rf $INSTALL_LOCATION/$SOURCEDIR
mkdir $INSTALL_LOCATION/$SOURCEDIR
cp ./dist/index.html $INSTALL_LOCATION
cp -r ./dist/* $INSTALL_LOCATION/$SOURCEDIR
#
# install common
#
echo "Installing common..."
cd $HOME/$SOURCEDIR/common
yarn install
#
# Build client 
#
echo "Installing client..."
cd $HOME/$SOURCEDIR/client
yarn install 
echo "Client installed => building client..."
yarn run prod
#
# build server
#
echo "Client in place => Installing server..."
cd $HOME/$SOURCEDIR/server
yarn install
echo "server installed => building Server..."
yarn run production 
echo "Ended : back to home"
cd $HOME
# copy Application zip to download area
cp $DISTRIBUTION_FILE $INSTALL_LOCATION/$SOURCEDIR/distribution


