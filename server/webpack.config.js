//webpack.config.js

//Webpack requires this to work with directories
const path = require('path');
const nodeExternals = require('webpack-node-externals');


module.exports = (env, argv) => ({
  //path to entry paint
  entry: './src/server.ts',
  target: 'node',
  externals: [nodeExternals()],
  // source watching
  watch: argv.mode !== 'production',
  performance: {
    hints: false
  },

  resolve: {
    extensions: ['.ts', '.js'],
    alias: {
      '@': path.resolve(__dirname, 'src'),
      '@common': path.resolve(__dirname, '../common/lib')
    }
  },
  //path and filename of the final output
  output: {
    path: path.resolve(__dirname, 'dist'),
    filename: 'main.js'
  },

  // debugging map
  devtool: 'source-map',

  module: {
    rules: [
      {
        test: /\.(ts)$/,
        exclude: /node_modules/,
        use: [
          // { loader: 'babel-loader' },
          {
            loader: 'ts-loader',
            "options": {
              "projectReferences": true
            }
          }
        ]
      }
    ]
  },
  //default mode is production
  mode: 'development'
})