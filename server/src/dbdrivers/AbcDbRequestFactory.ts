import { ABCDb } from './ABCDb'
import { SqlResult } from '@common/DbProtocol'
import {
  ABCOid,
  File,
  Folder,
  ABCQuery,
  DBType,
  DbOperation,
  ABCDbCandidate,
  StorageOperation,
  DatabaseAction
} from '@common/ABCCommon'

class AbcDbRequestFactory {
  private db: ABCDb

  parent: ABCOid = null
  file: File = null
  folder: Folder = null
  query: ABCQuery = null

  isFolder(): boolean {
    if (this.folder == null) {
      return false
    }
    return true
  }

  getOid(): ABCOid {
    if (this.file != null) {
      return this.file.oid
    }
    if (this.folder != null) {
      return this.folder.oid
    }
    return null
  }

  setDb(dbName: string, dbType: DBType) {
    this.db = new ABCDb(dbName, dbType)
  }

  build(operation: DbOperation) {
    let candidate = new ABCDbCandidate(
      this.isFolder(),
      this.getOid(),
      this.file,
      this.folder
    )
    let oper = new StorageOperation(operation, this.parent, this.query)
    oper.candidate = candidate
    this.db.operation = oper
  }

  emit(): string {
    return JSON.stringify(this.db)
  }

  async exec(): Promise<SqlResult> {
    let jsonRequest = this.emit()
    let request = new ABCDbJsonRequest()
    let returned = await request.doRequest(
      DatabaseAction.StorageOperation,
      jsonRequest
    )
    return returned
  }

  async deletedDb(): Promise<SqlResult> {
    let jsonRequest = this.emit()
    let request = new ABCDbJsonRequest()
    return await request.doRequest(DatabaseAction.DeleteDatabase, jsonRequest)
  }

  async createDb(): Promise<SqlResult> {
    let jsonRequest = this.emit()
    let request = new ABCDbJsonRequest()
    return await request.doRequest(DatabaseAction.CreateDatabase, jsonRequest)
  }

  async dbImport(root: Folder): Promise<SqlResult> {
    this.folder = root
    this.build(DbOperation.ADD)
    let returned = await this.exec()
    return returned
  }

  async dbExport(): Promise<SqlResult> {
    let oid = new ABCOid(0)
    this.query = new ABCQuery(true, oid)
    this.build(DbOperation.LOAD)
    let returned = await this.exec()
    return returned
  }
}

class ABCDbJsonRequest {
  //

  private async storage(db: ABCDb): Promise<SqlResult> {
    let dbResult = await db.mount()
    if (dbResult.isOk()) {
      dbResult = await db.storageOperation()
    }
    if (dbResult.isOk()) {
      await db.unmount() // ignore close result
    }
    return dbResult
  }

  async doRequest(
    fx: DatabaseAction,
    jsonRepo: string = `
    {
      "core" :
      {
        "name" : "defaultRepository" ,
        "type" : "SqlLite"
      }
    }  
   `
  ): Promise<SqlResult> {
    let dbJsoned: any = JSON.parse(jsonRepo)
    let db = ABCDb.decode(dbJsoned)
    let jsonDest = jsonRepo
    switch (fx) {
      case DatabaseAction.CreateDatabase:
        return await db.create()
      case DatabaseAction.DeleteDatabase:
        return await db.delete()
      case DatabaseAction.ListDatabase:
        let dbList = ABCDb.getScoreRepoList()
        jsonDest = JSON.stringify(dbList)
        return SqlResult.setData(jsonDest)
      case DatabaseAction.StorageOperation:
        let returned = await this.storage(db)
        return returned
    }
  }
}

export { AbcDbRequestFactory, ABCDbJsonRequest }
