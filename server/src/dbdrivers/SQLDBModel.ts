import {
  ABCOid,
  DBType,
  Folder,
  File,
  DataContent,
  Leaf,
  log
} from '@common/ABCCommon'

import { DbProtocol, SqlResult } from '@common/DbProtocol'
import { ExportOptions } from '@common/ABCCommon'
import { assert } from 'console'

enum SqlType {
  Text,
  Text64,
  Integer32,
  UInteger64,
  Integer64,
  Real,
  Blob
}

interface SqlStatement {
  prepare(sql: string): Promise<SqlResult>

  exec(stmt: string, bind: any[]): Promise<SqlResult>
  close(): Promise<SqlResult>
  // bind(...args: any[]): any[]
  fetch(): Promise<SqlResult>
}

interface SQLDbProtocol extends DbProtocol {
  createDataModel(): void
  dropDataModel(): void

  newStatement(): SqlStatement

  updateForeignKey(table: string, col: string, pKey: ABCOid, fKey: ABCOid)
  delete(table: String, key: String, id: ABCOid)

  check(dbResult: SqlResult): any
}

interface DatabaseCollection {
  store(dbDriver: SQLDbProtocol): void
  load(dbDriver: SQLDbProtocol, id: ABCOid, recursive: boolean): void
  update(dbDriver: SQLDbProtocol): void
  delete(dbDriver: SQLDbProtocol): void

  // provide DDL SQL
  createSqlDDl(dbType: DBType): string
  dropSqlDDl(dbType: DBType): string
}

function checkResult(result: SqlResult) {
  if (result.isOk()) return
  throw new Error(result.error)
}

class ABCSqlFolder implements DatabaseCollection {
  oid: ABCOid = null
  leaf: ABCOid = null
  folder: Folder = null
  parent?: ABCOid = null

  async rootExists(dbDriver: SQLDbProtocol) {
    let stmtStr = 'SELECT * FROM ABCFolder WHERE oid=0'
    let stmt = dbDriver.newStatement()
    let dbResult = await stmt.prepare(stmtStr)
    dbDriver.check(dbResult)
    dbResult = await stmt.fetch()
    let data = dbDriver.check(dbResult)
    await stmt.close()
    if (data != null) {
      return true
    }
    return false
  }

  constructor(folder?: Folder, parent?: ABCOid) {
    this.folder = folder
    this.parent = parent
  }

  async store(dbDriver: SQLDbProtocol) {
    if (this.folder == null) {
      throw new Error('storing null folder instance')
    }
    let stmtStr = `
      INSERT INTO ABCFolder
      (
        oid, leaf ,  parent
      )
      VALUES (?, ?  ,?)
     `
    this.oid = new ABCOid()
    if (this.parent == null) {
      // Root special key to avoid level indexing
      if (await this.rootExists(dbDriver)) {
        this.parent = new ABCOid(0)
      } else {
        this.oid.oid = 0
      }
    }
    let stmt = dbDriver.newStatement()
    this.folder.oid = this.oid
    let f = this.folder
    let binded = [f.oid.oid, null, this.parent?.oid]
    log.debug('ABCSqlFolder before storing ...')
    let dbResult = await stmt.exec(stmtStr, binded)
    dbDriver.check(dbResult)
    log.debug('ABCSqlFolder stored OK')
    dbResult = await stmt.close()
    dbDriver.check(dbResult)
    log.debug(`stored : ${stmtStr}`)
    // store Leaf first
    let leaf = f.infos
    let dbLeaf = new ABCSqlLeaf(leaf, null, this)
    await dbLeaf.store(dbDriver)
    // fKey update
    await dbDriver.updateForeignKey('ABCFolder', 'leaf', this.oid, leaf.oid)
    // handle children
    if (f.folders != null) {
      for (let curFolder of f.folders) {
        let curDbFolder = new ABCSqlFolder(curFolder, f.oid)
        await curDbFolder.store(dbDriver)
      }
    }
    if (f.files != null) {
      for (let curFile of f.files) {
        let curDbFile = new ABCSqlFile(curFile, f.oid)
        await curDbFile.store(dbDriver)
      }
    }
  }

  private async getChildren(
    dbDriver: SQLDbProtocol,
    stmtStr: string,
    isFolder: boolean
  ): Promise<any[]> {
    let stmt1 = dbDriver.newStatement()
    let dbResult = await stmt1.prepare(stmtStr)
    dbDriver.check(dbResult)
    let childrenF: any[] = null
    let child = (await stmt1.fetch()).check()
    while (child != null) {
      let fOid = new ABCOid(child.oid)
      if (childrenF == null) {
        childrenF = []
      }
      if (isFolder) {
        let f = new ABCSqlFolder()
        await f.load(dbDriver, fOid)
        childrenF.push(f.folder)
      } else {
        let f = new ABCSqlFile()
        await f.load(dbDriver, fOid)
        childrenF.push(f.file)
      }
      child = (await stmt1.fetch()).check()
    }
    await stmt1.close()
    return childrenF
  }

  async load(dbDriver: SQLDbProtocol, id: ABCOid, recursive: boolean = false) {
    let stmtStr = `SELECT * FROM ABCFolder WHERE oid=${id.oid}`
    let stmt = dbDriver.newStatement()
    let dbResult = await stmt.prepare(stmtStr)
    dbDriver.check(dbResult)
    let row = (await stmt.fetch()).check()
    if (row != null) {
      this.leaf = new ABCOid(row.leaf)
      let dbLeaf = new ABCSqlLeaf()
      await dbLeaf.load(dbDriver, this.leaf)
      this.folder = new Folder(dbLeaf.leaf)
      this.folder.oid = id
      // load children folders
      let stmtStr1 = `SELECT * FROM ABCFolder WHERE parent=${id.oid}`
      let childrenFolders = await this.getChildren(dbDriver, stmtStr1, true)
      this.folder.folders = childrenFolders as Folder[]
      // load children files
      stmtStr = `SELECT * FROM ABCFile WHERE parent=${id.oid}`
      let childrenFiles = await this.getChildren(dbDriver, stmtStr, false)
      this.folder.files = childrenFiles as File[]
    }
    await stmt.close()
  }
  async update(dbDriver: SQLDbProtocol) {
    if (this.folder == null) {
      throw new Error('updating  null folder instance')
    }
    let dbLeaf = new ABCSqlLeaf()
    dbLeaf.leaf = this.folder.infos
    await dbLeaf.update(dbDriver)
  }

  async delete(dbDriver: SQLDbProtocol) {
    let delOid: ABCOid = null
    if (this.oid != null) {
      delOid = this.oid
    } else {
      if (this.folder != null) {
        delOid = this.folder.oid
      }
    }
    if (delOid == null) {
      throw new Error('deleting  null folder instance')
    }
    if (delOid.oid == 0) {
      throw new Error('deleting root folder forbidden')
    }
    await dbDriver.delete('ABCFolder', 'oid', delOid)
  }

  createSqlDDl(dbType: DBType): string {
    return `
    CREATE TABLE ABCFolder
    (
    oid INTEGER PRIMARY KEY ,
    leaf INTEGER NULL ,
    parent INTEGER ,
    CONSTRAINT fk_ABCFolder
    FOREIGN KEY (parent)
    REFERENCES ABCFolder(oid)
    ON DELETE CASCADE
    ,
    CONSTRAINT fk_ABCLeaf
    FOREIGN KEY (leaf)
    REFERENCES ABCLeaf(oid)
    )
    `
  }

  dropSqlDDl(dbType: DBType): string {
    return `
    DROP TABLE IF EXISTS ABCFolder
    `
  }
}

class ABCSqlFile implements DatabaseCollection {
  oid: ABCOid = null
  leaf: ABCOid = null
  file: File = null
  parent: ABCOid = null

  constructor(file?: File, parent?: ABCOid) {
    this.file = file
    this.parent = parent
  }

  private checkAbcData(infos: Leaf) {
    if (infos.contents != null) return // just here

    let exports: ExportOptions = {
      exportExtra: false,
      exportSound: false,
      exportIcon: false
    }
    infos.makeExportable(exports)
  }

  async store(dbDriver: SQLDbProtocol) {
    if (this.file == null) {
      throw new Error('null file instance in ABCSqlFile.store')
    }
    this.checkAbcData(this.file.infos)
    let stmtStr = `
      INSERT INTO ABCFile
      (
        oid, leaf , parent
      )
      VALUES (?, ? , ?);
     `
    let stmt = dbDriver.newStatement()
    this.oid = new ABCOid()
    this.file.oid = this.oid
    let binded = [this.oid.oid]
    let dbResult = await stmt.exec(stmtStr, binded)
    dbDriver.check(dbResult)
    stmt.close()
    log.debug(`stored File : ${stmtStr}`)
    // proceed with leaf
    let leaf = this.file.infos
    let dbLeaf = new ABCSqlLeaf(leaf, this, null)
    await dbLeaf.store(dbDriver)
    // Fkey updates
    dbDriver.updateForeignKey('ABCFile', 'leaf', this.file.oid, leaf.oid)
    dbDriver.updateForeignKey('ABCFile', 'parent', this.file.oid, this.parent)
  }

  async load(dbDriver: SQLDbProtocol, id: ABCOid, recursive: boolean = false) {
    let stmtStr = `SELECT * FROM ABCfile WHERE oid=${id.oid}`
    let stmt = dbDriver.newStatement()
    await stmt.prepare(stmtStr)
    let row = (await stmt.fetch()).check()
    if (row != null) {
      this.leaf = new ABCOid(row.leaf)
      let dbLeaf = new ABCSqlLeaf()
      await dbLeaf.load(dbDriver, this.leaf)
      this.file = new File(dbLeaf.leaf)
      this.file.oid = id
    }
    await stmt.close()
  }

  async update(dbDriver: SQLDbProtocol) {
    if (this.file == null) {
      throw new Error('updating  null file instance')
    }
    let dbLeaf = new ABCSqlLeaf()
    dbLeaf.leaf = this.file.infos
    await dbLeaf.update(dbDriver)
  }

  async delete(dbDriver: SQLDbProtocol) {
    await dbDriver.delete('ABCFile', 'oid', this.oid)
  }

  createSqlDDl(dbType: DBType): string {
    return `
    CREATE TABLE ABCFile
    (
    oid INTEGER PRIMARY KEY ,
    leaf INTEGER ,
    parent INTEGER ,
    CONSTRAINT fk_ABCFolder
    FOREIGN KEY (parent)
    REFERENCES ABCFolder(oid)
    ON DELETE CASCADE,
    CONSTRAINT fk_ABCLeaf
    FOREIGN KEY (leaf)
    REFERENCES ABCLeaf(oid)
    )
    `
  }

  dropSqlDDl(dbType: DBType): string {
    return `
    DROP TABLE IF EXISTS ABCFile
    `
  }
}

class ABCSqlDataContent implements DatabaseCollection {
  private oid: ABCOid = null
  private owner: ABCOid = null
  private content: DataContent = null
  private parent: ABCSqlLeaf = null

  constructor(owner?: ABCOid, content?: DataContent, parent?: ABCSqlLeaf) {
    this.owner = owner
    this.content = content
    this.parent = parent
  }

  async store(dbDriver: SQLDbProtocol) {
    let stmtStr = `
      INSERT INTO ABCDataContent
      (
        oid, owner , abc, image , icon , sound
      )
      VALUES (?, ? , ? ,? , ? , ?);
     `
    let stmt = dbDriver.newStatement()
    this.content.oid = new ABCOid()
    this.oid = this.content.oid
    let binded = [
      this.content.oid.oid,
      this.parent?.oid?.oid,
      this.content.abc,
      this.content.imageB64,
      this.content.iconB64,
      this.content.soundB64
    ]
    await stmt.exec(stmtStr, binded)
    await stmt.close()
    // Fkey update
    dbDriver.updateForeignKey('ABCLeaf', 'contents', this.parent?.oid, this.oid)
  }

  async load(dbDriver: SQLDbProtocol, id: ABCOid, recursive: boolean = false) {
    let stmtStr = `SELECT * FROM ABCDataContent WHERE oid=${id.oid}`
    let stmt = dbDriver.newStatement()
    await stmt.prepare(stmtStr)
    let row = (await stmt.fetch()).check()
    if (row != null) {
      let dataContent = new DataContent()
      dataContent.oid = id
      dataContent.abc = row.abc
      dataContent.iconB64 = row.iconB64
      dataContent.imageB64 = row.imageB64
      dataContent.soundB64 = row.soundB64
      this.content = dataContent
    }
    await stmt.close()
  }

  async update(dbDriver: SQLDbProtocol) {
    let stmtStr = `
      UPDATE ABCDataContent
      SET VALUE
        owner = ? ,
        abc = ?,
        image = ?,
        icon = ?,
        sound = ?
      WHERE oid = ${this.content!.oid!.oid}
     `
    let stmt = dbDriver.newStatement()
    let binded = [
      this.content.oid,
      this.content.abc,
      this.content.imageB64,
      this.content.iconB64,
      this.content.soundB64
    ]
    await stmt.exec(stmtStr, binded)
    await stmt.close()
  }

  async delete(dbDriver: SQLDbProtocol) {
    await dbDriver.delete('ABCDataContent', 'oid', this.oid)
  }

  createSqlDDl(dbType: DBType): string {
    return `
    CREATE TABLE ABCDataContent
    (
    oid INTEGER PRIMARY KEY ,
    owner INTEGER ,
    abc BLOB ,
    image  BLOB ,
    icon  BLOB ,
    sound BLOB ,
    CONSTRAINT fk_ABCLeaf
    FOREIGN KEY (owner)
    REFERENCES ABCLeaf(oid)
    ON DELETE CASCADE
    )
    `
  }

  dropSqlDDl(dbType: DBType): string {
    return `
    DROP TABLE IF EXISTS ABCDataContent
    `
  }
}

class ABCSqlLeaf implements DatabaseCollection {
  oid: ABCOid = null
  private container: ABCOid = null
  leaf: Leaf = null
  fileOwner: ABCSqlFile = null
  folderOwner: ABCSqlFolder = null

  constructor(leaf?: Leaf, fileOwner?: ABCSqlFile, folderOwner?: ABCSqlFolder) {
    this.leaf = leaf
    this.fileOwner = fileOwner
    this.folderOwner = folderOwner
  }

  private getBinded(ms = Math.trunc(new Date().getTime())) {
    return [
      this.oid.oid,
      this.fileOwner?.oid?.oid,
      this.folderOwner?.oid?.oid,
      this.leaf.title,
      ms,
      this.leaf.storageURL,
      this.leaf.iconURL,
      this.leaf.imageURL,
      this.leaf.soundURL,
      this.leaf.comments,
      null, // unknown at insert time
      this.leaf.encoding,
      this.leaf.type
    ]
  }

  private hasContent(): boolean {
    if (
      this.leaf.contents != null &&
      this.leaf.contents.abc != null &&
      this.leaf.contents.abc.length != 0
    )
      return true
    return false
  }

  async store(dbDriver: SQLDbProtocol) {
    log.debug('enter ABCSqlLeaf store ...')
    let stmtStr = `
      INSERT INTO ABCLeaf
      (
        oid, fileOwner , folderOwner, title , creation , storageURL ,iconURL ,
        imageURL ,soundURL , comments ,contents , encoding ,type
      )
      VALUES (?, ? , ? ,? ,? ,? ,? ,? ,? ,? ,? ,? ,? );
     `
    let stmt = dbDriver.newStatement()
    this.oid = new ABCOid()
    this.leaf.oid = this.oid
    log.debug(`ABCLEaf oid is : ${this.leaf.oid.oid} ; ${this.leaf.oid}`)
    let binded = this.getBinded()
    let dbResult = await stmt.exec(stmtStr, binded)
    dbDriver.check(dbResult)
    dbResult = await stmt.close()
    dbDriver.check(dbResult)
    log.debug(`stored : ${stmtStr}`)
    if (this.hasContent()) {
      let sqlContent = new ABCSqlDataContent(
        this.leaf.oid,
        this.leaf.contents,
        this
      )
      await sqlContent.store(dbDriver)
    }
    // handle tags
    if (this.leaf.tags != null) {
      let dbTags = new ABCSqlTags(this)
      await dbTags.store(dbDriver)
    }
    // foreign key update
    if (this.folderOwner != null) {
      await dbDriver.updateForeignKey(
        'ABCFolder',
        'leaf',
        this.folderOwner.oid,
        this.leaf.oid
      )
    }
    if (this.fileOwner != null) {
      await dbDriver.updateForeignKey(
        'ABCFile',
        'leaf',
        this.fileOwner.oid,
        this.leaf.oid
      )
    }
  }

  async load(dbDriver: SQLDbProtocol, id: ABCOid, recursive: boolean = false) {
    let stmtStr = `SELECT * FROM ABCLeaf WHERE oid=${id.oid}`
    let stmt = dbDriver.newStatement()
    await stmt.prepare(stmtStr)
    let row = (await stmt.fetch()).check()
    if (row != null) {
      this.leaf = new Leaf(row.title)
      this.leaf.oid = row.oid
      this.leaf.storageURL = row.storageURL
      this.leaf.imageURL = row.imageURL
      this.leaf.soundURL = row.soundURL
      this.leaf.iconURL = row.iconURL
      this.leaf.comments = row.comments
      this.leaf.encoding = row.encoding
      this.leaf.type = row.type
      let contentID = row.contents
      // get stored content next and populate it
      if (contentID != null) {
        let contentOid = new ABCOid(contentID)
        let content = new ABCSqlDataContent()
        await content.load(dbDriver, contentOid)
      }
    }
    await stmt.close()
  }

  async update(dbDriver: SQLDbProtocol) {
    if (this.leaf == null) {
      throw new Error('null lead instance on update request')
    }
    let stmtStr = `
      UPDATE ABCLeaf
      SET
        oid = ?,
        fileOwner = ? ,
        folderOwner = ? ,
        title = ? ,
        creation = ?,
        storageURL = ? ,
        iconURL = ? ,
        imageURL = ? ,
        soundURL = ? ,
        comments = ?,
        contents = ?,
        encoding = ?,
        type = ?
      WHERE oid=${this.leaf.oid!.oid}
      `
    let stmt = dbDriver.newStatement()
    let binded = this.getBinded(this.leaf.creation)
    await stmt.exec(stmtStr, binded)
    await stmt.close()
    if (this.leaf.contents != null) {
      let dbContent = new ABCSqlDataContent(
        this.leaf.oid!,
        this.leaf.contents,
        this
      )
      await dbContent.update(dbDriver)
    }
    // handle tags
    if (this.leaf.tags != null) {
      let dbTags = new ABCSqlTags(this)
      await dbTags.update(dbDriver)
    }
  }

  async delete(dbDriver: SQLDbProtocol) {
    await dbDriver.delete('ABCLeaf', 'oid', this.oid)
  }

  createSqlDDl(dbType: DBType): string {
    return `
    CREATE TABLE ABCLeaf
    (
    oid INTEGER PRIMARY KEY ,
    fileOwner INTEGER ,
    folderOwner INTEGER ,
    title  TEXT ,
    creation  INTEGER ,
    storageURL TEXT ,
    iconURL TEXT ,
    imageURL TEXT ,
    soundURL TEXT ,
    comments TEXT ,
    contents INTEGER ,
    encoding INTEGER ,
    type TEXT ,
    CONSTRAINT fk_ABCFile
    FOREIGN KEY (fileOwner)
    REFERENCES ABCFile(oid)
    ON DELETE CASCADE ,
    CONSTRAINT fk_ABCFolder
    FOREIGN KEY (folderOwner)
    REFERENCES ABCFolder(oid)
    ON DELETE CASCADE ,
    CONSTRAINT fk_ABCContent
    FOREIGN KEY (contents)
    REFERENCES ABCDataContent(oid)
    )
    `
  }

  dropSqlDDl(dbType: DBType): string {
    return `
    DROP TABLE IF EXISTS ABCLeaf
    `
  }
}

class ABCSqlTags implements DatabaseCollection {
  private oid: ABCOid = null // tag ID
  private container: ABCSqlLeaf = null // leaf container ID

  constructor(leaf?: ABCSqlLeaf) {
    this.container = leaf
  }

  private parseTags(tagList: string): string[] {
    if (tagList == null) return null
    if (tagList.length > 0) {
      return tagList.split(',')
    }
    return null
  }

  async store(dbDriver: SQLDbProtocol) {
    let leaf = this.container
    if (leaf == null) {
      throw new Error('tag container is null on store method')
    }
    let stmtStr = `
    INSERT INTO ABCTags
      (
        oid, container , value , sequence
      )
    VALUES (?, ? , ? ,? );
    `
    let tagsStr = leaf.leaf?.tags
    let tagPos = 0
    let tags = this.parseTags(tagsStr)
    if (tags != null) {
      let stmt = dbDriver.newStatement()
      for (let tag of tags) {
        let oid = new ABCOid()
        let binded = [oid.oid, this.container.oid.oid, tag, tagPos]
        stmt.exec(stmtStr, binded)
        tagPos++
      }
      stmt.close()
    }
  }

  //
  // query on given tag value
  //
  async query(dbDriver: SQLDbProtocol, tag: String): Promise<any[]> {
    let stmtStr = `SELECT * FROM ABCTags WHERE value="${tag}"`
    let stmt = dbDriver.newStatement()
    await stmt.prepare(stmtStr)
    let row = (await stmt.fetch()).check()
    let returned: any[] = null
    while (row != null) {
      if (returned == null) {
        returned = []
      }
      let parent = row.container
      let dbLeaf = new ABCSqlLeaf()
      dbLeaf.oid = new ABCOid(parent)
      await dbLeaf.load(dbDriver, dbLeaf.oid)
      if (dbLeaf.fileOwner != null) {
        returned.push(dbLeaf.fileOwner.file)
      }
      if (dbLeaf.folderOwner != null) {
        returned.push(dbLeaf.folderOwner.folder)
      }
    }
    await stmt.close()
    return returned
  }

  async load(dbDriver: SQLDbProtocol, id: ABCOid, recursive: boolean) {
    let stmtStr = `SELECT * FROM ABCTags WHERE container=${id.oid}`
    let stmt = dbDriver.newStatement()
    await stmt.prepare(stmtStr)
    let tags: string = ''
    let row = (await stmt.fetch()).check()
    while (row != null) {
      tags += row.value
      row = (await stmt.fetch()).check()
      if (row != null) tags += ','
    }
    await stmt.close()
    this.container.leaf.tags = tags
  }

  async update(dbDriver: SQLDbProtocol) {
    await this.delete(dbDriver)
    await this.store(dbDriver)
  }

  async delete(dbDriver: SQLDbProtocol) {
    await dbDriver.delete('ABCTags', 'container', this.container.leaf.oid)
  }

  createIndex(dbType: DBType): string {
    return `
      CREATE INDEX TagValueIndex
      ON ABCTags ( value )
    `
  }

  createSqlDDl(dbType: DBType): string {
    return `
    CREATE TABLE ABCTags
    (
    oid  INTEGER PRIMARY KEY ,
    container  INTEGER ,
    sequence  INTEGER ,
    value  TEXT ,
    CONSTRAINT fk_ABCLeaf
    FOREIGN KEY (container)
    REFERENCES ABCLeaf(oid)
    ON DELETE CASCADE
    )
    `
  }

  dropSqlDDl(dbType: DBType): string {
    return `
    DROP TABLE IF EXISTS ABCTags
    `
  }
}

export {
  ABCSqlDataContent,
  SQLDbProtocol,
  SqlStatement,
  SqlType,
  ABCSqlFile,
  ABCSqlFolder,
  ABCSqlLeaf,
  ABCSqlTags
}
