import fs from 'fs'
import path from 'path'

import { DbProtocol, DBCore } from '@common/DbProtocol'
import { DBType, StorageOperation } from '@common/ABCCommon'
import { SqliteDriver } from '../dbdrivers/SqliteDriver'
import { SqlResult } from '@common/DbProtocol'

import { logger } from '../utils/ABCLogger'

const DATABASE_HOME: string = '/data/databases'

class ABCDb {
  core: DBCore
  operation: StorageOperation = null
  private driver: DbProtocol = null

  static decode(source: any): ABCDb {
    let returned: ABCDb = new ABCDb(source.core.name, source.core.type)
    returned.operation = StorageOperation.decode(source.operation)
    return returned
  }

  static getDbDirectory(): string {
    let current = process.cwd()
    // check existence
    const returned = current + DATABASE_HOME
    if (fs.existsSync('./directory-name')) {
      return returned
    }
    // create and return
    try {
      fs.mkdirSync(returned, { recursive: true })
      return returned
    } catch (e) {
      logger.error('SEVEREfails ')
    }
  }

  private static dbFromFile(fName: string): ABCDb {
    const ext = path.extname(fName)
    return new ABCDb(fName, DBType.Sqlite)
  }

  static getScoreRepoList(): ABCDb[] {
    logger.debug('getScoreRepoList entered')
    const dbHomeDir = ABCDb.getDbDirectory()
    let empty: boolean = true
    let returned: ABCDb[] = []
    fs.readdirSync(dbHomeDir).forEach((file) => {
      logger.debug(`database found : ${file}`)
      returned.push(this.dbFromFile(file))
      empty = false
    })
    return returned
  }

  private getDriver(): DbProtocol {
    let driver: DbProtocol = null
    if (this.core.type == DBType.Sqlite) {
      driver = new SqliteDriver(ABCDb.getDbDirectory(), this.core.name)
    } else {
      throw new Error(
        `Invalid(not implemented) database driver : ${this.core.type}`
      )
    }
    return driver
  }

  async create(): Promise<SqlResult> {
    logger.debug(`Creating repository ${this.core.name} of ${this.core.type}`)
    return await this.driver.createDatabase()
  }

  async delete(): Promise<SqlResult> {
    logger.debug(`deleting repository ${this.core.name} of ${this.core.type}`)
    let deleted = await this.driver.deleteDatabase()
    return deleted
  }

  exists(): boolean {
    return this.driver.existsDatabase()
  }

  async mount(): Promise<SqlResult> {
    let result: SqlResult
    if (!this.driver.existsDatabase()) {
      result = await this.create()
    } else {
      result = await this.driver.openDatabase()
    }
    return result
  }

  async storageOperation(): Promise<SqlResult> {
    if (this.operation != null) {
      if (this.driver != null) {
        this.operation.test()
        let returned = await this.operation.proceed(this.driver)
        return returned
      } else {
        return SqlResult.setError('Db Storage operation : driver missing')
      }
    } else {
      return SqlResult.setError('db storage operation is null ')
    }
  }

  async unmount(): Promise<SqlResult> {
    logger.debug('closing database ...')
    let returned = await this.driver.closeDatabase()
    logger.debug('database closed')
    return returned
  }

  constructor(name: string, type: DBType | string) {
    let tp: DBType
    if (typeof type === 'string') {
      tp = DBCore.driverTypeFromString(type as string)
    } else {
      tp = type as DBType
    }
    this.core = new DBCore(name, tp)
    this.driver = this.getDriver()
  }
}

export { ABCDb }
