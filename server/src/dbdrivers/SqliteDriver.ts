//
// SqlLite Db driver
//

import sqlite3 from 'sqlite3'
import { Database, Statement, open } from 'sqlite'

import fs from 'fs'
import { promises as fsPromises } from 'fs'
// import { Fiberit } from 'fiberit'
import { logger } from '../utils/ABCLogger'
import { SQLDbProtocol, SqlStatement } from './SQLDBModel'
import { SqlResult } from '@common/DbProtocol'
import {
  ABCDbCandidate,
  ABCOid,
  ABCQuery,
  DBType,
  log
} from '@common/ABCCommon'
import {
  ABCSqlFolder,
  ABCSqlFile,
  ABCSqlLeaf,
  ABCSqlDataContent,
  ABCSqlTags
} from './SQLDBModel'
import { reject } from 'lodash'

const _DBTRACING_ = true

function emitOK(): SqlResult {
  return SqlResult.setData(null)
}

function emitError(msg: string): SqlResult {
  return SqlResult.setError(msg)
}

function emitWarning(msg: string, data?: any): SqlResult {
  return SqlResult.setWarning(msg, data)
}

function emitData(data: any): SqlResult {
  return SqlResult.setData(data)
}

//
// Async sqlite primitives
//
class SqliteStatement implements SqlStatement {
  private db: Database
  private stmt: Statement

  constructor(db: Database) {
    this.db = db
    this.stmt = null
  }

  async exec(stmt: string, bind: any[] = []): Promise<SqlResult> {
    try {
      if (this.db == null) return emitError('null db on Statement.exec')
      await this.db.run(stmt, bind)
      return emitOK()
    } catch (error) {
      return emitError(error.message)
    }
  }

  async prepare(sql: string): Promise<SqlResult> {
    try {
      this.stmt = await this.db.prepare(sql)
      if (this.stmt == null) {
        return emitError(`failed to prepare SQL statement: ${sql}`)
      }
      return emitOK()
    } catch (error) {
      return emitError(error.message)
    }
  }

  async fetch(): Promise<SqlResult> {
    try {
      if (this.stmt == null) {
        return emitError('fetch on null statement')
      }
      let data = await this.stmt.get()
      return emitData(data)
    } catch (error) {
      return emitError(error.message)
    }
  }

  async close(): Promise<SqlResult> {
    if (this.stmt == null) return emitOK()
    try {
      await this.stmt.finalize()
    } catch (error) {
      return emitError(error.message)
    }
  }
}

class SqliteDriver implements SQLDbProtocol {
  private static _DB_SUFFIX_ = '.sqlite3.abcmusicstudio'

  private path: string
  private name: string
  private db: Database = null
  // private p: SqliteFront = new SqliteFront()

  constructor(dbPath: string, name: string) {
    this.path = dbPath
    // create dir if not there
    if (!fs.existsSync(this.path)) {
      fs.mkdirSync(this.path, { recursive: true })
    }
    this.name = name
  }

  async delete(table: String, key: String, id: ABCOid) {
    let stmtStr = `DELETE FROM ${table} WHERE ${key}=${id.oid}`
    let stmt = this.newStatement()
    await stmt.exec(stmtStr, null)
    await stmt.close()
  }

  static dbSuffix(name: string): string {
    if (name.endsWith(this._DB_SUFFIX_)) return name
    return name + '.sqlite3.abcmusicstudio'
  }

  private dbPath(): string {
    return this.path + '/' + SqliteDriver.dbSuffix(this.name)
  }

  private reportError(msg: string, code: number): Error {
    return new Error(`ERROR code=${code} : ${msg}`)
  }

  private async DDLExec(sql: string) {
    if (this.db != null) {
      let stmt = new SqliteStatement(this.db)
      let returned = await stmt.exec(sql)
      let closed = await stmt.close()
      return returned
    }
  }

  check(dbResult: SqlResult): any {
    if (dbResult.isOk()) {
      return dbResult.data
    } else {
      throw new Error(dbResult.error)
    }
  }

  isOpened(): boolean {
    if (this.db == null) {
      return false
    }
    return true
  }

  async createDatabase(): Promise<SqlResult> {
    logger.debug('create database started ...')
    let returned = { name: this.name }
    if (!this.isOpened()) {
      await this.openDatabase()
    }
    let catalogOK = await this.checkCatalog()

    if (catalogOK) {
      await this.closeDatabase()
      return emitWarning(
        `database ${this.name} already exists`,
        JSON.stringify(returned)
      )
    }
    let created = await this.createDataModel()
    logger.debug('create database ended')
    logger.debug('checking catalog again ...')
    catalogOK = await this.checkCatalog()
    if (catalogOK) {
      logger.debug('DB Catalog is OK')
      return emitData(JSON.stringify(returned))
    } else {
      return emitError(`database ${this.name} Bad Db catalog`)
    }
  }

  async checkFileExists(file: string) {
    return fs.promises
      .access(file, fs.constants.F_OK)
      .then(() => true)
      .catch(() => false)
  }

  async deleteDatabase(): Promise<SqlResult> {
    let returned = { name: this.name }
    if (this.db != null) {
      this.closeDatabase()
    }
    let candidate: string = this.dbPath()
    const exists = await this.checkFileExists(candidate)
    if (exists) {
      await fs.promises.unlink(candidate)
      return emitData(JSON.stringify(returned))
    }
    return emitWarning(
      `database ${this.name} does not exists`,
      JSON.stringify(returned)
    )
  }

  async openDatabase(): Promise<SqlResult> {
    log.debug(`opening database : ${this.dbPath()}`)
    this.db = await open({
      filename: this.dbPath(),
      driver: sqlite3.Database,
      mode: sqlite3.OPEN_READWRITE | sqlite3.OPEN_CREATE
    })
    if (_DBTRACING_) {
      this.db.on('trace', (data) => {
        log.debug(`DBTRACING = ${data}`)
      })
    }
    if (this.db == null) {
      return emitError(`Failed to open database : ${this.name}`)
    }
    return emitOK()
  }

  existsDatabase(): boolean {
    if (fs.existsSync(this.dbPath())) {
      return true
    }
    return false
  }

  async closeDatabase(): Promise<SqlResult> {
    if (this.db != null) {
      await this.db.close()
    }
    this.db = null
    return emitOK()
  }

  // nothing special before driver deallocation
  terminate(): void {}

  async checkCatalog() {
    logger.debug('entering checkCatalog')
    if (this.db != null) {
      let stmtStr = `
      SELECT
          name
      FROM
          sqlite_master
      WHERE
          type ='table' AND
          name NOT LIKE 'sqlite_%'
      `
      let stmt = this.newStatement()
      // let stmt = await this.select(stmtStr)
      let result = await stmt.prepare(stmtStr)
      this.check(result)
      logger.debug('checkCatalog after select')
      //let row = (await this.fetch(stmt)).data
      result = await stmt.fetch()
      let row = this.check(result)

      logger.debug('checkCatalog after fetch')
      await stmt.close()
      logger.debug('checkCatalog after closestatement')
      if (row == null) {
        logger.debug('checkCatalog exit : NOT FOUND')
        return false
      }
      if (row.name == 'ABCFolder') {
        logger.debug('ABC db schema has been detected')
        return true
      }
      logger.debug('checkCatalog exit : No ABCFolder found')
      return false
    } else {
      reject('cannot checkCatalog : Db is closed')
    }
  }

  async createDataModel(): Promise<SqlResult> {
    try {
      logger.debug('creating ABC Datamodel ...')
      await this.DDLExec(new ABCSqlFolder().createSqlDDl(DBType.Sqlite))
      await this.DDLExec(new ABCSqlFile().createSqlDDl(DBType.Sqlite))
      await this.DDLExec(new ABCSqlLeaf().createSqlDDl(DBType.Sqlite))
      await this.DDLExec(new ABCSqlDataContent().createSqlDDl(DBType.Sqlite))
      await this.DDLExec(new ABCSqlTags().createSqlDDl(DBType.Sqlite))
      await this.DDLExec(new ABCSqlTags().createIndex(DBType.Sqlite))
      logger.debug('ABC Datamodel created')
      return emitOK()
    } catch (err) {
      logger.error(`datamodel creation failed : ${err.message}`)
      return emitError(`datamodel creation failed : ${err.message}`)
    }
  }

  async dropDataModel() {
    logger.debug('dropping ABC Datamodel ...')
    await this.DDLExec(new ABCSqlFolder().dropSqlDDl(DBType.Sqlite))
    await this.DDLExec(new ABCSqlFile().dropSqlDDl(DBType.Sqlite))
    await this.DDLExec(new ABCSqlLeaf().dropSqlDDl(DBType.Sqlite))
    await this.DDLExec(new ABCSqlDataContent().dropSqlDDl(DBType.Sqlite))
    await this.DDLExec(new ABCSqlTags().dropSqlDDl(DBType.Sqlite))
    await this.DDLExec(new ABCSqlTags().dropSqlDDl(DBType.Sqlite))
    logger.debug('ABC Datamodel dropped')
    return emitOK()
  }

  async updateForeignKey(
    table: string,
    col: string,
    pKey: ABCOid,
    fKey: ABCOid
  ) {
    let stmt = this.newStatement()
    let stmtStr = 'UPDATE ' + table + ' set ' + col + '=? WHERE oid=?'
    let binded = [fKey.oid, pKey.oid]
    let dbResult = await stmt.exec(stmtStr, binded)
    this.check(dbResult)
    dbResult = await stmt.close()
    this.check(dbResult)
  }

  newStatement(): SqlStatement {
    return new SqliteStatement(this.db)
  }

  private async fileChange(
    candidate: ABCDbCandidate,
    parent: ABCOid,
    newFile: boolean = false
  ): Promise<SqlResult> {
    let sqlFile = new ABCSqlFile(candidate.file, parent)
    try {
      if (newFile) {
        await sqlFile.store(this)
      } else {
        await sqlFile.update(this)
      }
      return new SqlResult()
    } catch (error) {
      log.error(error)
      return new SqlResult(null, null, error.message)
    }
  }

  private async folderChange(
    candidate: ABCDbCandidate,
    parent: ABCOid,
    newFolder: boolean = false
  ): Promise<SqlResult> {
    let sqlFolder = new ABCSqlFolder(candidate.folder, parent)
    try {
      if (newFolder) {
        await sqlFolder.store(this)
      } else {
        await sqlFolder.update(this)
      }
      return emitOK()
    } catch (error) {
      log.error(error)
      return emitError(error.message)
    }
  }

  private async getFile(oid: ABCOid): Promise<SqlResult> {
    let sqlFile = new ABCSqlFile()
    try {
      await sqlFile.load(this, oid)
      return emitData(sqlFile.file)
    } catch (error) {
      log.error(error)
      return emitError(error.message)
    }
  }

  private async getFolder(oid: ABCOid): Promise<SqlResult> {
    let sqlFolder = new ABCSqlFolder()
    try {
      await sqlFolder.load(this, oid)
      return emitData(sqlFolder.folder)
    } catch (error) {
      log.error(error)
      return emitError(error.message)
    }
  }

  //
  // add candidate to database (file or folder)
  //
  async add(candidate: ABCDbCandidate, parent: ABCOid): Promise<SqlResult> {
    if (candidate.file != null) {
      return this.fileChange(candidate, parent, true)
    }
    if (candidate.folder != null) {
      return this.folderChange(candidate, parent, true)
    }
    return emitError('Add method expect either file or folder not being null')
  }

  async deleteRows(candidate: ABCDbCandidate): Promise<SqlResult> {
    try {
      if (candidate.file != null) {
        let sqlFile = new ABCSqlFile(candidate.file)
        await sqlFile.delete(this)
      }
      if (candidate.folder != null) {
        let sqlFolder = new ABCSqlFile(candidate.folder)
        await sqlFolder.delete(this)
      }
      return emitError('Add method expect either file or folder not being null')
    } catch (error) {
      log.error(error)
      return emitError(error.message)
    }
  }

  async update(candidate: ABCDbCandidate, parent: ABCOid): Promise<SqlResult> {
    if (candidate.file != null) {
      return this.fileChange(candidate, parent)
    }
    if (candidate.folder != null) {
      return this.folderChange(candidate, parent)
    }
    return emitError('Add method expect either file or folder not being null')
  }

  async load(query: ABCQuery): Promise<SqlResult> {
    log.debug('load operation started ...')
    let result: SqlResult
    if (query.isFolder) {
      result = await this.getFolder(query.oid)
    } else {
      result = await this.getFile(query.oid)
    }
    log.debug('load operation completed')
    return result
  }
}

export { SqliteDriver }
