import { hello } from '../hello_sample'
import { ABCLogger, logger } from '../utils/ABCLogger'

let l: ABCLogger

beforeAll(() => {
  l = new ABCLogger('info')
})

describe('Hello function', () => {
  it('should return hello world', () => {
    logger.debug('testing debug')
    const result = hello()
    expect(result).toEqual('Hello world!')
  })
})
