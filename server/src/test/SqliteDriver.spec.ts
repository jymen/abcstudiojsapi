import { SqliteDriver } from '../dbdrivers/SqliteDriver'
import { ABCLogger, logger } from '../utils/ABCLogger'
import {
  ScoreDataStore,
  setLogger,
  log,
  DBType,
  Folder
} from '@common/ABCCommon'
import { SqlResult } from '@common/DbProtocol'
import { AbcDbRequestFactory } from '../dbdrivers/AbcDbRequestFactory'
import { sampleFolder, readTestCatalog } from './SampleData'

const TESTHOME = process.cwd() + '/testData'
const DBHOME = TESTHOME + '/databases'
const BIG = 'testDatabaseBIG'
const TINY = 'testDatabaseTINY'

// init a global loader
let l = new ABCLogger('info')
setLogger(ABCLogger.logger)
const TIMEOUT = 240000000
//
jest.setTimeout(TIMEOUT)

describe('Sqlite driver test', () => {
  //
  it('test create database', async () => {
    log.info('test create database started ...')
    jest.setTimeout(TIMEOUT)
    const driver = new SqliteDriver(DBHOME, TINY)
    let created = await driver.createDatabase()
    logger.debug(`database creation status : ${created}`)
    expect(created.isOk()).toBe(true)
    let dropped = await driver.dropDataModel()
    expect(dropped.isOk()).toBe(true)
    log.info('test create database ended')
  })

  it('test open database', () => {
    log.info('test open database started ...')
    jest.setTimeout(TIMEOUT)
    const driver = new SqliteDriver(DBHOME, TINY)
    let db = driver.openDatabase()
    expect(db).not.toEqual(null)
    driver.closeDatabase()
    log.info('test open database ended ')
  })

  it('test delete database', async () => {
    log.info('test delete database started ...')
    jest.setTimeout(TIMEOUT)
    const driver = new SqliteDriver(DBHOME, TINY)
    const deleted = await driver.deleteDatabase()
    expect(deleted.isOk()).toBe(true)
    expect(driver.existsDatabase()).toBe(false)
    log.info('test delete database ended')
  })

  async function importFrom(json: string, db: string): Promise<SqlResult> {
    const driver = new SqliteDriver(DBHOME, db)
    const created = await driver.createDatabase()
    expect(created.isOk()).toBe(true)
    expect(json).not.toBe(null)
    let dataStore = new ScoreDataStore()
    dataStore.fromJson(json)
    // populate to data base
    let dbFactory = new AbcDbRequestFactory()
    dbFactory.setDb(db, DBType.Sqlite)
    dataStore.unlinkParents() // remove parentage
    let imported = await dbFactory.dbImport(dataStore.root)
    if (imported.isError()) {
      console.error(`tiny import error: ${imported.error}`)
    }
    expect(imported.isOk()).toBe(true)
    return imported
  }

  async function exportFrom(db: string): Promise<Folder> {
    const driver = new SqliteDriver(DBHOME, db)
    await driver.openDatabase()
    let dbFactory = new AbcDbRequestFactory()
    dbFactory.setDb(db, DBType.Sqlite)
    let dbResult = await dbFactory.dbExport()
    expect(dbResult.isOk()).toEqual(true)
    return dbResult.data as Folder
  }

  it('test TINY import From JSON', async () => {
    log.info('test TINY import From JSON started ...')
    jest.setTimeout(TIMEOUT)
    let dbResult = await importFrom(sampleFolder, TINY)
    expect(dbResult.isOk()).toBe(true)
    log.info('test TINY import From JSON ended')
  })

  it('test BIG import From JSON', async () => {
    log.info('test BIG import From JSON started ...')
    jest.setTimeout(TIMEOUT)
    let catalogStr = readTestCatalog()
    let dbResult = await importFrom(catalogStr, BIG)
    expect(dbResult.isOk()).toBe(true)
    log.info('test BIG import From JSON ended')
  })

  it('test full export from database to JSON', async () => {
    log.info('test full export from database to JSON started ...')
    jest.setTimeout(TIMEOUT)
    let folder = await exportFrom(BIG)
    expect(folder).not.toEqual(null)
    log.info('test full export from database to JSON ended')
  })
})
