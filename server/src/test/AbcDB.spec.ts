import { hello } from '../hello_sample'
import { ABCDb } from '../dbdrivers/ABCDb'
import { DBType, setLogger, log } from '@common/ABCCommon'
import { ABCLogger } from '../utils/ABCLogger'
import { SqlResult } from '@common/DbProtocol'

beforeAll(() => {
  let l = new ABCLogger('info')
  setLogger(ABCLogger.logger)
})

function checkResult(result: SqlResult) {
  if (result.isOk()) return
  throw new Error(result.error)
}

describe('AbcDB test', () => {
  it('should return hello world', () => {
    log.info('checking logger ...')
    const result = hello()
    expect(result).toEqual('Hello world!')
    log.info('checking logger ended')
  })

  it('test Create Mount unmount Delete database API', async () => {
    log.info('test Create Mount unmount Delete database API started ...')
    const db = new ABCDb('temporaryForTest', DBType.Sqlite)
    try {
      let dbResult = await db.create()
      checkResult(dbResult)
      dbResult = await db.unmount()
      checkResult(dbResult)
      dbResult = await db.mount()
      checkResult(dbResult)
      db.delete()
      log.debug(`database deleted`)
      log.debug('test Create Mount unmount Delete database API ended')
    } catch (error) {
      log.error(`create mount delete failure : ${error}`)
      expect(true).toBe(false)
    }
  })

  it('test list database API', () => {
    log.info('test list database API started ...')
    let databaseList: ABCDb[] = ABCDb.getScoreRepoList()
    let nbDb = databaseList.length
    log.debug(`database count : ${nbDb}`) // 1, "string", false
    expect(nbDb).not.toEqual(0)
    for (let db of databaseList) {
      log.debug(`database : ${db.core.name}`) // 1, "string", false
    }
    log.info('test list database API ended')
  })
})
