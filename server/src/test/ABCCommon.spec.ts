import { ABCOid, ScoreDataStore } from '@common/ABCCommon'
import { ABCLogger, logger } from '../utils/ABCLogger'
import { sampleFolder } from './SampleData'

const TIMEOUT = 200000
let container: number[] = []
let l: ABCLogger
beforeAll(() => {
  l = new ABCLogger('info')
})

function doesNotContain(oid: ABCOid) {
  for (let i = 0; i < container.length; i++) {
    if (oid.oid == container[i]) return false
  }
  container.push(oid.oid)
  return true
}

describe('Test ABCCommon classes', () => {
  //
  it('Test load from DataStore', () => {
    jest.setTimeout(TIMEOUT)
    logger.info('Test load from DataStore started...')
    let dataStore = new ScoreDataStore()
    dataStore.fromJson(sampleFolder)
    expect(dataStore.root).not.toEqual(null)
    expect(dataStore.root.folders.length).toEqual(1)
    expect(dataStore.root.files.length).toEqual(3)
    logger.info('Test load from DataStore ended')
  })

  it('Test ABCOid', () => {
    logger.info('Test ABCOid started ...')
    let oid = new ABCOid()
    expect(oid.oid).toBeGreaterThan(0)
    logger.debug(`oid=${oid.toString()}`)
    for (let ii = 1; ii < 100; ii++) {
      let newOid = new ABCOid()
      logger.debug(`newOid = ${newOid.oid}`)
      expect(newOid.oid).not.toEqual(oid.oid)
      expect(doesNotContain(newOid)).toEqual(true)
      oid = newOid
    }
    logger.info('Test ABCOid ended')
  })
})
