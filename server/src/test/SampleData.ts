/*

sample data structures used by tests

*/
import { log } from '@common/ABCCommon'
import fs from 'fs'

const TESTHOME = process.cwd() + '/testData'
// const TESTCATALOG = TESTHOME + '/testcatalogue.abcmusicstudio'
const TESTCATALOG = TESTHOME + '/exportForWeb.json'

let sampleFolder: string = `
 {
  	"root": {
  		"infos": {
  			"title": "root Folder",
  			"creation": 573495800.37878299,
  			"storageURL": "file:///Users/jymen",
  			"comments": "An optional comment area",
  			"encoding": 1
  		},
  		"folders": [{
  			"infos": {
  				"title": "Jigs and Slip Jigs",
  				"creation": 573495800.37878299,
  				"storageURL": "file:///Users/jymen",
  				"encoding": 1
  			}
  		}],
  		"files": [{
  				"infos": {
  					"title": "Jigs and Slip Jigs",
  					"creation": 573495800.37878299,
  					"storageURL": "file:///Users/jymen",
  					"encoding": 1
  				}
				},
				{ "infos": {
						"storageURL":"file:\/\/\/Users\/jymen\/Library\/Application%20Support\/ABCMusicStudio\/abcdata\/Mulqueen's.abc",
						"comments":"",
						"soundURL":"file:\/\/\/Users\/jymen\/Music\/irish%20tunes\/fiddle\/Sessions\/atelier%20skv\/MullQueen.mp3",
						"title":"Mulqueen's",
						"creation":597361502.72669005,
						"contents":{
							"encoded":true,
							"abc":"dnZ2ZyUldm9jYWxmb250IEJvb2ttYW4tTGlnaHQgMTIKJSVoaXN0b3J5Zm9udCBCb29rbWFuLUxpZ2h0IDEwCiUld29yZHNmb250IEJvb2ttYW4tTGlnaHQgMTQKJSVjb21wb3NlcmZvbnQgQm9va21hbi1MaWdodCAxMgolJXRpdGxlZm9udCBMdW1pbmFyaSAyNgpYOiAxClQ6IE11bHF1ZWVuJ3MKUjogcmVlbApNOiA0LzQKTDogMS84Cks6IERtYWoKfEYyIEVHIEZEREd8RkRBRyBGRERFfEYyIEVHIEZEREd8RkFCYyBkQkFHfApGMiBFRyBGRERHfEZEQUcgRkRERXxGMiBFRyBGRERHfEZBQmMgZDIgY2R8CnxlNCBlZmVkfGNkZWYgZzIgZmd8YTJlMiBlZmVkfGNBQmMgZDIgY2R8CmUyIGUyIGVmZWR8Y2RlZiBnMiBmZ3xhMmYyIGcyIGUyfGRmZWMgZEJBR3w="},"type":"ABCScore",
							"encoding":1
						},
						"state":"FolderCLosed"
				},
  			{
  				"infos": {
  					"title": "locate me",
  					"creation": 573495800.37878299,
  					"storageURL": "file:///Users/jymen",
  					"encoding": 1
  				}
  			}
  		]
  	}
  }
	`

function readTestCatalog(): string {
  try {
    const data = fs.readFileSync(TESTCATALOG, 'utf8')
    return data
  } catch (e) {
    log.error('Error:', e.stack)
    expect(e).toEqual(null) // force error
  }
}

export { sampleFolder, readTestCatalog }
