import { ABCLogger, logger } from '../utils/ABCLogger'
import {
  setLogger,
  log,
  Folder,
  Leaf,
  ABCOid,
  ScoreDataStore
} from '@common/ABCCommon'
import { SqliteDriver } from '../dbdrivers/SqliteDriver'
import { ABCSqlFolder } from '../dbdrivers/SQLDBModel'
import { SqlResult } from '../../../common/src/DbProtocol'
import { sampleFolder, readTestCatalog } from './SampleData'
import sqlite3 from 'sqlite3'

const TESTHOME = process.cwd() + '/testData'
const DBHOME = TESTHOME + '/databases'
const TIMEOUT = 20000

// init a global loader
let l = new ABCLogger('info')
setLogger(ABCLogger.logger)

beforeAll(async () => {
  sqlite3.verbose()
})

function checkResult(result: SqlResult) {
  if (result.isOk()) return
  throw new Error(result.error)
}

async function dbCleanup() {
  // cleanup database
  log.debug('DB cleanup started ...')
  const driver = new SqliteDriver(DBHOME, 'testDatabase')
  await driver.deleteDatabase()
  await driver.createDatabase()
  await driver.closeDatabase()
  log.debug('DB cleanup done')
}

function checkFolder(folder: Folder) {
  expect(folder.oid).not.toBe(null)
  let leaf = folder.infos
  expect(leaf).not.toBe(null)
  expect(leaf.title).not.toBe(null)
  log.debug(`folder title is : ${leaf.title}`)
}

async function afterStore(driver: SqliteDriver, sqlFolder: ABCSqlFolder) {
  await driver.closeDatabase()
  await driver.openDatabase()
  let exist = await sqlFolder.rootExists(driver)
  expect(exist).toBe(true)
  log.debug('root existing')
  await driver.closeDatabase()
  log.debug('rDb closed')
}

async function storeCat(tiny: boolean) {
  log.debug('initial cleanup ...')
  await dbCleanup()
  log.debug('cleanup completed')
  const driver = new SqliteDriver(DBHOME, 'testDatabase')
  await driver.openDatabase()
  let dataStore = new ScoreDataStore()
  if (tiny) {
    dataStore.fromJson(sampleFolder)
  } else {
    dataStore.fromJson(readTestCatalog())
  }
  let sqlFolder = new ABCSqlFolder(dataStore.root)
  await sqlFolder.store(driver)
  let oid = new ABCOid(0)
  log.debug('starting loading ....')
  await sqlFolder.load(driver, oid, false)
  log.debug('load completed')
  let folder = sqlFolder.folder
  checkFolder(folder)
  expect(folder.folders.length).toBeGreaterThan(0)
}

describe('Test ABCFolder', () => {
  //
  it('validate exception capture', async () => {
    jest.setTimeout(TIMEOUT)
    const driver = new SqliteDriver(DBHOME, 'testDatabase')
    let stmt = null
    try {
      log.info('validate exception catpture ...')
      await driver.openDatabase()
      stmt = driver.newStatement()
      let sqlStmt = 'This is an invalid sql statement'
      log.debug('before exec')
      checkResult(await stmt.exec(sqlStmt, null))
      log.debug('after exec')
      fail('exception thow expected here ...')
    } catch (error) {
      expect(error).not.toBe(null)
      log.debug(`expected returned error is : ${error.message}`)
      stmt.close()
      log.info('validate exception catpture completed with success')
    }
  })

  it('ABCFolder insert and check', async () => {
    jest.setTimeout(TIMEOUT)
    try {
      log.info('test check root not exist started ...')
      const driver = new SqliteDriver(DBHOME, 'testDatabase')
      await driver.openDatabase()
      let sqlFolder = new ABCSqlFolder()
      let exist = await sqlFolder.rootExists(driver)
      if (exist) {
        log.debug('Root is there when starting => clean up ...')
        await dbCleanup()
        log.debug('cleanup Done and OK')
      }
      log.debug('OK Root is not there on start')
      await driver.openDatabase()
      let leaf = new Leaf('root folder')
      let folder = new Folder(leaf, null)
      sqlFolder = new ABCSqlFolder(folder)
      log.debug('storing root ...')
      await sqlFolder.store(driver)
      log.debug('root stored')
      await driver.closeDatabase()
      await driver.openDatabase()
      exist = await sqlFolder.rootExists(driver)
      expect(exist).toBe(true)
      log.debug('root existing')
      await driver.closeDatabase()
      log.debug('rDb closed')
    } catch (error) {
      log.error(`severe error : ${error}`)
      fail('Exception occured on execution')
    }
    log.info('test check root not exist ended')
  })

  it('ABCFolder load root non recursive', async () => {
    jest.setTimeout(TIMEOUT)
    try {
      log.info('ABCFolder load sample root started ...')
      const driver = new SqliteDriver(DBHOME, 'testDatabase')
      await driver.openDatabase()
      let sqlFolder = new ABCSqlFolder()
      let oid = new ABCOid(0)
      await sqlFolder.load(driver, oid, false)
      let folder = sqlFolder.folder
      checkFolder(folder)
    } catch (error) {
      log.error(`severe error : ${error}`)
      fail('Exception occured on execution')
    }
    log.info('ABCFolder load root ended')
  })

  it('ABCFolder store sample folder recursive', async () => {
    jest.setTimeout(TIMEOUT)
    try {
      log.info('ABCFolder TINY store sample folder recursive started ...')
      await storeCat(false)
    } catch (error) {
      log.error(`severe error : ${error}`)
      fail('Exception occured on execution')
    }
    log.info('ABCFolder TINY store sample folder recursiveended')
  })
})
