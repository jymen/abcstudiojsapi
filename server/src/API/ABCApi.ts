//
// Available Server API FrontEnd
//
import express from 'express'
// import { AbcDbRequestFactory } from '../dbdrivers/AbcDbRequestFactory'
import {
  DatabaseAction,
  DatabaseActionUtil,
  EntryPointArgs,
  FxRet
} from '@common/ABCCommon'
import { SqlResult } from '@common/DbProtocol'

import { ABCDbJsonRequest } from '@/dbdrivers/AbcDbRequestFactory'
/**
 * external Communication API args
 */

class ABCApi {
  private res: express.Response = null
  private apiModel: EntryPointArgs = null

  constructor(body: EntryPointArgs, res: express.Response) {
    this.res = res
    this.apiModel = body
  }

  private checkParams(): boolean {
    if (this.apiModel.className == null) {
      return false
    }
    if (this.apiModel.fx == null) {
      return false
    }
    return true
  }

  private async repository(
    fx: DatabaseAction,
    jsonRepo = `
     {
       "core" : {
         "name" : "defaultRepository" ,
         "type" : "SqlLite"
        }
      }  
    `
  ) {
    let request = new ABCDbJsonRequest()
    let dbResult = await request.doRequest(fx, jsonRepo)
    let fxRet: FxRet
    if (dbResult.isOk()) {
      let data: string[] = []
      data.push(dbResult.data)
      fxRet = new FxRet(data, dbResult.error, dbResult.errorCode)
    } else {
      fxRet = new FxRet(null, dbResult.error, dbResult.errorCode)
      throw new Error(dbResult.error)
    }
    this.apiModel.fx = DatabaseActionUtil.toString(fx)
    this.apiModel.fxRet = fxRet
    this.res.send(JSON.stringify(this.apiModel))
  }

  private getArgs0(): string {
    if (this.apiModel.fxArgs == null) return null
    return this.apiModel.fxArgs[0]
  }

  handleRequest() {
    let errMsg = null
    if (this.checkParams()) {
      let className = this.apiModel.className
      let fxName = this.apiModel.fx
      let arg0 = this.getArgs0()
      switch (className) {
        case 'AbcRepositoryAPI':
          switch (fxName) {
            case 'storageOperation':
              this.repository(DatabaseAction.StorageOperation, arg0).catch(
                (err) => {
                  this.res.status(500).send({
                    error: `SERVER StorageOperation ERROR : ${err.message}`
                  })
                }
              )
              break
            case 'getScoreRepoList':
              this.repository(DatabaseAction.ListDatabase).catch((err) => {
                this.res.status(500).send({
                  error: `SERVER getScoreReposlist ERROR : ${err.message}`
                })
              })
              break
            case 'createRepository':
              this.repository(DatabaseAction.CreateDatabase, arg0).catch(
                (err) => {
                  this.res.status(500).send({
                    error: `SERVER CreateDatabase ERROR : ${err.message}`
                  })
                }
              )
              break
            case 'deleteRepository':
              this.repository(DatabaseAction.DeleteDatabase, arg0).catch(
                (err) => {
                  this.res.status(500).send({
                    error: `SERVER DeleteDatabase ERROR : ${err.message}`
                  })
                }
              )
              break
            default:
              errMsg = `Undefined API : ${this.apiModel.className}.${fxName}`
          }
          break
        default:
          errMsg = `Invalid API className : ${this.apiModel.className}`
          break
      }
    } else {
      errMsg = `Invalid API message : ${JSON.stringify(this.apiModel)}`
    }
    if (errMsg != null) {
      this.res.status(500).send({ error: `SERVER SIDE ERROR : ${errMsg}` })
    }
  }
}

export { ABCApi }
