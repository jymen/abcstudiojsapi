/**
 * Pino Logger class
 */
import express from 'express'

import pino from 'pino'
import expressPino from 'express-pino-logger'

class ABCLogger {
  private expressLogger: expressPino.HttpLogger
  public static logger: pino.Logger = null

  constructor(logLevel?: string | null, app?: express.Application | null) {
    if (logger == null) {
      if (logLevel == null) {
        logLevel = 'debug'
      }
      logger = pino({ level: logLevel })
    }
    if (app != null) {
      this.expressLogger = expressPino({ logger })
      app.use(this.expressLogger)
    }
    ABCLogger.logger = logger
  }
}

let logger: pino.Logger = null

export { ABCLogger, logger }
