//
// Main Express server entry point
//
import express from 'express'
import cors from 'cors'
import bodyparser from 'body-parser'
import morgan from 'morgan'
import fs from 'fs'
import yargs from 'yargs'
//
import { ABCApi } from '@/API/ABCApi'
import { ABCLogger, logger } from '@/utils/ABCLogger'
import { EntryPointArgs, log, setLogger } from '@common/ABCCommon'
import { testValue, SampleTest } from '@common/samplehello'

// just verify that references are working
new SampleTest().hello(testValue.toString())
//
//
class AbcStudioServer {
  private logLevel: string
  public routePrv: Routes = new Routes()

  public app: express.Application
  public logger: ABCLogger
  public homeDir: string
  public homePublic: string
  public port: number

  constructor() {
    this.logLevel = process.env.LOG_LEVEL || 'info'
    console.log(`ABCStudio Express http loglevel is ${this.logLevel}`)
    // just verify that references are working
    this.app = express()
    this.logger = new ABCLogger(this.logLevel, this.app)
    setLogger(ABCLogger.logger)
    this.homeDir = process.cwd()
    this.homePublic = this.homeDir + '/../client/dist'
  }

  private processArgv(): void {
    const argv = yargs.option('port', {
      alias: 'p',
      type: 'number',
      default: 4000,
      describe: 'listening port number'
    }).argv
    if (argv.port) {
      this.port = argv.port
    }
    log.info(`ABcStudioServer will listen on port : ${this.port}`)
  }

  //
  // basic configurator
  //
  private config(): void {
    log.info(`pino app logger is active in mode : ${this.logLevel}`)
    log.info(`home dir is : ${this.homeDir}`)
    log.info(`public home directory is : ${this.homePublic}`)
    // check homePublic
    if (!fs.existsSync(this.homePublic)) {
      log.error(
        `Invalid public home directory => ABORTING : ${this.homePublic}`
      )
      process.exit(12)
    }
    this.app.use(morgan('tiny'))
    this.app.use(cors())
    this.app.use(
      bodyparser.urlencoded({
        extended: true
      })
    )
    this.app.use(bodyparser.json())
    //
    // set static path to home (vue js stuff)
    //
    this.app.use(
      '/',
      express.static(this.homePublic, { index: 'indexStudio.html' })
    )
  }

  public init(): void {
    this.config()
    this.processArgv()
    // config routes
    this.routePrv.routes(this.app)
  }

  public listen(): void {
    this.app.listen(this.port, () => {
      log.info(
        `listening for incoming sollicitors on ${abcStudioServer.port} ...`
      )
    })
  }
}

class NodesController {
  private getArgs(req: express.Request): EntryPointArgs {
    if (req.method == 'GET') {
      return (req.query as unknown) as EntryPointArgs
    } else {
      return (req.body as unknown) as EntryPointArgs
    }
  }

  public api = (req: express.Request, res: express.Response) => {
    const args = this.getArgs(req)
    let api = new ABCApi(args, res)
    api.handleRequest()
  }

  public badRoute = (req: express.Request, res: express.Response) => {
    res.statusCode = 404
    res.end('404 Invalid ABCMusicStudio server request')
  }
}

class Routes {
  private API_ROUTE = '/API/:which'

  public nodesController: NodesController = new NodesController()

  public routes(app: express.Application): void {
    app.get(this.API_ROUTE, this.nodesController.api)
    app.post(this.API_ROUTE, this.nodesController.api)
    app.use(this.nodesController.badRoute)
  }
}

//
// main entry point
//
const abcStudioServer = new AbcStudioServer()
abcStudioServer.init()
abcStudioServer.listen()
