import express from 'express';
import { EntryPointArgs } from '@common/ABCCommon';
/**
 * external Communication API args
 */
declare class ABCApi {
    private res;
    private apiModel;
    constructor(body: EntryPointArgs, res: express.Response);
    private checkParams;
    private repository;
    private getArgs0;
    handleRequest(): void;
}
export { ABCApi };
