/**
 * Pino Logger class
 */
import express from 'express';
import pino from 'pino';
declare class ABCLogger {
    private expressLogger;
    static logger: pino.Logger;
    constructor(logLevel?: string | null, app?: express.Application | null);
}
declare let logger: pino.Logger;
export { ABCLogger, logger };
