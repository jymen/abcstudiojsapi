import { SqlResult } from '@common/DbProtocol';
import { ABCOid, File, Folder, ABCQuery, DBType, DbOperation, DatabaseAction } from '@common/ABCCommon';
declare class AbcDbRequestFactory {
    private db;
    parent: ABCOid;
    file: File;
    folder: Folder;
    query: ABCQuery;
    isFolder(): boolean;
    getOid(): ABCOid;
    setDb(dbName: string, dbType: DBType): void;
    build(operation: DbOperation): void;
    emit(): string;
    exec(): Promise<SqlResult>;
    deletedDb(): Promise<SqlResult>;
    createDb(): Promise<SqlResult>;
    dbImport(root: Folder): Promise<SqlResult>;
    dbExport(): Promise<SqlResult>;
}
declare class ABCDbJsonRequest {
    private storage;
    doRequest(fx: DatabaseAction, jsonRepo?: string): Promise<SqlResult>;
}
export { AbcDbRequestFactory, ABCDbJsonRequest };
