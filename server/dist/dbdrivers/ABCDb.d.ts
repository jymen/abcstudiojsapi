import { DBCore } from '@common/DbProtocol';
import { DBType, StorageOperation } from '@common/ABCCommon';
import { SqlResult } from '@common/DbProtocol';
declare class ABCDb {
    core: DBCore;
    operation: StorageOperation;
    private driver;
    static decode(source: any): ABCDb;
    static getDbDirectory(): string;
    private static dbFromFile;
    static getScoreRepoList(): ABCDb[];
    private getDriver;
    create(): Promise<SqlResult>;
    delete(): Promise<SqlResult>;
    exists(): boolean;
    mount(): Promise<SqlResult>;
    storageOperation(): Promise<SqlResult>;
    unmount(): Promise<SqlResult>;
    constructor(name: string, type: DBType | string);
}
export { ABCDb };
