import { ABCOid, DBType, Folder, File, DataContent, Leaf } from '@common/ABCCommon';
import { DbProtocol, SqlResult } from '@common/DbProtocol';
declare enum SqlType {
    Text = 0,
    Text64 = 1,
    Integer32 = 2,
    UInteger64 = 3,
    Integer64 = 4,
    Real = 5,
    Blob = 6
}
interface SqlStatement {
    prepare(sql: string): Promise<SqlResult>;
    exec(stmt: string, bind: any[]): Promise<SqlResult>;
    close(): Promise<SqlResult>;
    fetch(): Promise<SqlResult>;
}
interface SQLDbProtocol extends DbProtocol {
    createDataModel(): void;
    dropDataModel(): void;
    newStatement(): SqlStatement;
    updateForeignKey(table: string, col: string, pKey: ABCOid, fKey: ABCOid): any;
    delete(table: String, key: String, id: ABCOid): any;
    check(dbResult: SqlResult): any;
}
interface DatabaseCollection {
    store(dbDriver: SQLDbProtocol): void;
    load(dbDriver: SQLDbProtocol, id: ABCOid, recursive: boolean): void;
    update(dbDriver: SQLDbProtocol): void;
    delete(dbDriver: SQLDbProtocol): void;
    createSqlDDl(dbType: DBType): string;
    dropSqlDDl(dbType: DBType): string;
}
declare class ABCSqlFolder implements DatabaseCollection {
    oid: ABCOid;
    leaf: ABCOid;
    folder: Folder;
    parent?: ABCOid;
    rootExists(dbDriver: SQLDbProtocol): Promise<boolean>;
    constructor(folder?: Folder, parent?: ABCOid);
    store(dbDriver: SQLDbProtocol): Promise<void>;
    private getChildren;
    load(dbDriver: SQLDbProtocol, id: ABCOid, recursive?: boolean): Promise<void>;
    update(dbDriver: SQLDbProtocol): Promise<void>;
    delete(dbDriver: SQLDbProtocol): Promise<void>;
    createSqlDDl(dbType: DBType): string;
    dropSqlDDl(dbType: DBType): string;
}
declare class ABCSqlFile implements DatabaseCollection {
    oid: ABCOid;
    leaf: ABCOid;
    file: File;
    parent: ABCOid;
    constructor(file?: File, parent?: ABCOid);
    private checkAbcData;
    store(dbDriver: SQLDbProtocol): Promise<void>;
    load(dbDriver: SQLDbProtocol, id: ABCOid, recursive?: boolean): Promise<void>;
    update(dbDriver: SQLDbProtocol): Promise<void>;
    delete(dbDriver: SQLDbProtocol): Promise<void>;
    createSqlDDl(dbType: DBType): string;
    dropSqlDDl(dbType: DBType): string;
}
declare class ABCSqlDataContent implements DatabaseCollection {
    private oid;
    private owner;
    private content;
    private parent;
    constructor(owner?: ABCOid, content?: DataContent, parent?: ABCSqlLeaf);
    store(dbDriver: SQLDbProtocol): Promise<void>;
    load(dbDriver: SQLDbProtocol, id: ABCOid, recursive?: boolean): Promise<void>;
    update(dbDriver: SQLDbProtocol): Promise<void>;
    delete(dbDriver: SQLDbProtocol): Promise<void>;
    createSqlDDl(dbType: DBType): string;
    dropSqlDDl(dbType: DBType): string;
}
declare class ABCSqlLeaf implements DatabaseCollection {
    oid: ABCOid;
    private container;
    leaf: Leaf;
    fileOwner: ABCSqlFile;
    folderOwner: ABCSqlFolder;
    constructor(leaf?: Leaf, fileOwner?: ABCSqlFile, folderOwner?: ABCSqlFolder);
    private getBinded;
    private hasContent;
    store(dbDriver: SQLDbProtocol): Promise<void>;
    load(dbDriver: SQLDbProtocol, id: ABCOid, recursive?: boolean): Promise<void>;
    update(dbDriver: SQLDbProtocol): Promise<void>;
    delete(dbDriver: SQLDbProtocol): Promise<void>;
    createSqlDDl(dbType: DBType): string;
    dropSqlDDl(dbType: DBType): string;
}
declare class ABCSqlTags implements DatabaseCollection {
    private oid;
    private container;
    constructor(leaf?: ABCSqlLeaf);
    private parseTags;
    store(dbDriver: SQLDbProtocol): Promise<void>;
    query(dbDriver: SQLDbProtocol, tag: String): Promise<any[]>;
    load(dbDriver: SQLDbProtocol, id: ABCOid, recursive: boolean): Promise<void>;
    update(dbDriver: SQLDbProtocol): Promise<void>;
    delete(dbDriver: SQLDbProtocol): Promise<void>;
    createIndex(dbType: DBType): string;
    createSqlDDl(dbType: DBType): string;
    dropSqlDDl(dbType: DBType): string;
}
export { ABCSqlDataContent, SQLDbProtocol, SqlStatement, SqlType, ABCSqlFile, ABCSqlFolder, ABCSqlLeaf, ABCSqlTags };
