/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/server.ts");
/******/ })
/************************************************************************/
/******/ ({

/***/ "../common/lib/ABCCommon.js":
/*!**********************************!*\
  !*** ../common/lib/ABCCommon.js ***!
  \**********************************/
/*! exports provided: ABCOid, StorageOperation, ABCQuery, DbOperation, ABCDbCandidate, DBType, File, Folder, ScoreDataStore, DataContent, Leaf, ABCResType, DatabaseAction, DatabaseActionUtil, DBAction, setLogger, log, EntryPointArgs, FxRet */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ABCOid", function() { return ABCOid; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "StorageOperation", function() { return StorageOperation; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ABCQuery", function() { return ABCQuery; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DbOperation", function() { return DbOperation; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ABCDbCandidate", function() { return ABCDbCandidate; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DBType", function() { return DBType; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "File", function() { return File; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Folder", function() { return Folder; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ScoreDataStore", function() { return ScoreDataStore; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DataContent", function() { return DataContent; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Leaf", function() { return Leaf; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ABCResType", function() { return ABCResType; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DatabaseAction", function() { return DatabaseAction; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DatabaseActionUtil", function() { return DatabaseActionUtil; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DBAction", function() { return DBAction; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "setLogger", function() { return setLogger; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "log", function() { return log; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "EntryPointArgs", function() { return EntryPointArgs; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FxRet", function() { return FxRet; });
/**
 * COMMON client server structures
 * WARNING
 * THIS File is shared through a symbolic link and must
 * only contain client / server compatible code
 */
// import _ from 'lodash'
//
// Data Semantics
//
//
var ABCResType;
(function (ABCResType) {
    ABCResType[ABCResType["ABCScore"] = 0] = "ABCScore";
    ABCResType[ABCResType["Group"] = 1] = "Group";
    ABCResType[ABCResType["GroupWithFolder"] = 2] = "GroupWithFolder";
    ABCResType[ABCResType["XmlMusicScore"] = 3] = "XmlMusicScore";
})(ABCResType || (ABCResType = {}));
var DatabaseAction;
(function (DatabaseAction) {
    DatabaseAction[DatabaseAction["CreateDatabase"] = 0] = "CreateDatabase";
    DatabaseAction[DatabaseAction["DeleteDatabase"] = 1] = "DeleteDatabase";
    DatabaseAction[DatabaseAction["ListDatabase"] = 2] = "ListDatabase";
    DatabaseAction[DatabaseAction["StorageOperation"] = 3] = "StorageOperation";
    DatabaseAction[DatabaseAction["Undefined"] = 4] = "Undefined";
})(DatabaseAction || (DatabaseAction = {}));
class DatabaseActionUtil {
    static toString(action) {
        switch (action) {
            case DatabaseAction.CreateDatabase:
                return 'CreateDatabase';
            case DatabaseAction.DeleteDatabase:
                return 'DeleteDatabase';
            case DatabaseAction.ListDatabase:
                return 'ListDatabase';
            case DatabaseAction.StorageOperation:
                return 'StorageOperation';
            default:
                return 'UndefinedAction';
        }
    }
}
class DataContent {
    constructor() {
        this.oid = null;
    }
    // missing stuff to be completed later
    static decode(source) {
        if (source == null)
            return null;
        let returned = new DataContent();
        returned.encoded = source.encoded;
        returned.abc = source.abc;
        returned.oid = source.oid;
        returned.imageB64 = source.imageB64;
        returned.iconB64 = source.iconB64;
        returned.soundB64 = source.soundB64;
        return returned;
    }
}
class Leaf {
    constructor(title) {
        this.oid = null;
        this.title = title;
    }
    makeExportable(exportType) {
        // Browser NodeJS portability to be pondered HERE
        // for this method which may remain optional for the moment
    }
    static decode(source) {
        if (source == null)
            return null;
        let returned = new Leaf(source.title);
        returned.oid = ABCOid.decode(source.oid);
        returned.tags = source.tags;
        returned.contents = DataContent.decode(source.contents);
        returned.encoding = source.encoding;
        returned.type = source.type;
        returned.storageURL = source.storageURL;
        returned.iconURL = source.iconURL;
        returned.imageURL = source.imageURL;
        returned.imageZoom = source.imageZoom;
        returned.imageBgColor = source.imageBgColor;
        returned.soundURL = source.soundURL;
        returned.comments = source.comments;
        return returned;
    }
}
class File {
    constructor(infos, parent = null) {
        this.oid = null;
        this.infos = infos;
        this.parent = parent;
    }
    static decode(source) {
        if (source == null)
            return null;
        let leaf = Leaf.decode(source.infos);
        // parent is set to null to avoid circular refs
        let returned = new File(leaf, null);
        return returned;
    }
}
class Folder {
    constructor(infos, parent = null) {
        this.oid = null;
        this.infos = infos;
        this.files = [];
        this.folders = [];
        this.parent = parent;
    }
    static decode(source) {
        if (source == null)
            return null;
        let returned = Folder.buildClasses(source);
        return returned;
    }
    addFile(infos) {
        log.debug(`adding file ${infos.title}`);
        this.files.push(new File(infos, this));
    }
    addFolder(infos) {
        log.debug(`adding folder ${infos}`);
        this.folders.push(new Folder(infos, this));
    }
    add(candidate) {
        if ('files' in candidate) {
            this.folders.push(candidate);
        }
        else {
            this.files.push(candidate);
        }
    }
    remove(candidate) {
        if ('files' in candidate) {
            const folder = candidate;
            for (let ii = 0; ii < this.folders.length; ii++) {
                if (this.folders[ii].infos.title == folder.infos.title) {
                    this.folders.splice(ii, 1);
                }
            }
        }
        if ('content' in candidate) {
            const file = candidate;
            for (let ii = 0; ii < this.files.length; ii++) {
                if (this.files[ii].infos.title == file.infos.title) {
                    this.files.splice(ii, 1);
                }
            }
        }
    }
    /**
     * For debugging purposes
     */
    dump() {
        log.debug(`title = ${this.infos.title}`);
        log.debug('CHILD FOLDERS : ');
        for (const childFolder of this.folders) {
            childFolder.dump();
        }
        for (const childF of this.files) {
            log.debug(`child file title = ${childF.infos.title}`);
        }
    }
    static buildClasses(from) {
        const leaf = Leaf.decode(from.infos);
        const folder = new Folder(leaf, null);
        if (from.folders != null) {
            from.folders.forEach((element) => folder.add(Folder.buildClasses(element)));
        }
        if (from.files != null) {
            from.files.forEach((element) => folder.add(File.decode(element)));
        }
        return folder;
    }
    dealWithParents(add) {
        for (const childFolder of this.folders) {
            if (add) {
                childFolder.parent = this;
            }
            else {
                childFolder.parent = null;
            }
            childFolder.dealWithParents(add);
        }
        for (const childF of this.files) {
            if (add) {
                childF.parent = this;
            }
            else {
                childF.parent = null;
            }
        }
    }
}
class ScoreDataStore {
    constructor() {
        this.root = null;
    }
    setRoot(data) {
        if (data == null) {
            // build from scratch
            const leaf = new Leaf('This is the root tune');
            this.root = new Folder(leaf, null);
        }
        else if (typeof data === 'string') {
            // build from JSON
            this.fromJson(data);
        }
        else {
            // build from object
            this.root = data;
        }
        this.root.dump();
    }
    canBuildRoot(jsoned) {
        if (jsoned.folders != null) {
            let returned = {
                root: {
                    files: jsoned.files,
                    folders: jsoned.folders,
                    infos: jsoned.infos,
                },
            };
            return returned;
        }
        return null;
    }
    fromJson(jsonStr) {
        let jsoned = JSON.parse(jsonStr);
        if (jsoned.root == null) {
            jsoned = this.canBuildRoot(jsoned);
        }
        if (jsoned.root != null) {
            this.root = Folder.decode(jsoned.root);
            // this.root = jsoned.root.buildClasses(null)
        }
        else {
            this.root = null;
        }
    }
    unlinkParents() {
        // cleanup parents and stringify
        if (this.root != null) {
            this.root.dealWithParents(false);
        }
    }
    linkParents() {
        // cleanup parents and stringify
        if (this.root != null) {
            this.root.dealWithParents(true);
        }
    }
    fromLocalRepo(loader) {
        loader.loadALL();
    }
}
//
// Database Semantics
//
var DBType;
(function (DBType) {
    DBType[DBType["Sqlite"] = 0] = "Sqlite";
    DBType[DBType["SqliteMemory"] = 1] = "SqliteMemory";
    DBType[DBType["MySql"] = 2] = "MySql";
    DBType[DBType["PostGreSql"] = 3] = "PostGreSql";
    DBType[DBType["Undefined"] = 4] = "Undefined";
})(DBType || (DBType = {}));
var DBAction;
(function (DBAction) {
    DBAction[DBAction["CREATE"] = 'CreateDatabase'] = "CREATE";
    DBAction[DBAction["DELETE"] = 'DeleteDatabase'] = "DELETE";
    DBAction[DBAction["LIST"] = 'ListDatabase'] = "LIST";
    DBAction[DBAction["STORAGEOP"] = 'StorageOperation'] = "STORAGEOP";
})(DBAction || (DBAction = {}));
var DbOperation;
(function (DbOperation) {
    DbOperation[DbOperation["ADD"] = 0] = "ADD";
    DbOperation[DbOperation["UPDATE"] = 1] = "UPDATE";
    DbOperation[DbOperation["DELETE"] = 2] = "DELETE";
    DbOperation[DbOperation["LOAD"] = 3] = "LOAD";
})(DbOperation || (DbOperation = {}));
class ABCOid {
    constructor(oid = null) {
        this.oid = 0;
        if (oid == null) {
            this.oid = this.emit();
        }
        else {
            this.oid = oid;
        }
    }
    emit() {
        let min = Math.ceil(0);
        let max = Math.floor(5120000000);
        let random = Math.floor(Math.random() * (max - min)) + min;
        return new Date().getUTCMilliseconds() + random;
    }
    FormatNumber(length) {
        var r = '' + this.oid;
        while (r.length < length) {
            r = '0' + r;
        }
        return r;
    }
    toString() {
        let timestamp = this.oid.toString(16);
        return (timestamp +
            'xxxxxxxxxxxxxxxx'
                .replace(/[x]/g, function () {
                return ((Math.random() * 16) | 0).toString(16);
            })
                .toLowerCase());
    }
    static decode(source) {
        if (source == null)
            return null;
        let returned = new ABCOid(source.oid);
        return returned;
    }
}
class ABCQuery {
    constructor(isFolder, oid) {
        this.isFolder = isFolder;
        this.oid = oid;
    }
    static decode(source) {
        if (source == null)
            return null;
        return new ABCQuery(source.isFolder, ABCOid.decode(source.oid));
    }
}
class ABCDbCandidate {
    constructor(isFolder, oid, file, folder) {
        this.folder = null;
        this.file = null;
        this.isFolder = false;
        this.oid = null;
        this.isFolder = isFolder;
        this.oid = oid;
        this.file = file;
        this.folder = folder;
    }
    static decode(source) {
        if (source == null)
            return null;
        let folder = Folder.decode(source.folder);
        let file = File.decode(source.file);
        let oid = ABCOid.decode(source.oid);
        let returned = new ABCDbCandidate(source.isFolder, oid, file, folder);
        return returned;
    }
}
class StorageOperation {
    constructor(op, parent, query) {
        this.parent = null;
        this.candidate = null;
        this.query = null;
        this.completion = null;
        this.driver = null;
        this.op = op;
        this.parent = parent;
        this.candidate = null;
        this.query = query;
    }
    static decode(source) {
        if (source == null)
            return;
        let parent = ABCOid.decode(source.parent);
        let query = ABCQuery.decode(source.query);
        let returned = new StorageOperation(source.op, parent, query);
        returned.candidate = ABCDbCandidate.decode(source.candidate);
        return returned;
    }
    checkCandidate() {
        if (this.candidate == null) {
            throw new Error('Storage operation candidate is null');
        }
    }
    checkQuery() {
        if (this.query == null) {
            log.error('StorageOperation query is null');
            throw new Error('Storage operation query is null');
        }
    }
    test() {
        log.debug('StorageOperation test ENTERED');
    }
    async proceed(driver) {
        log.debug('PROCEEDING with storage operation');
        this.driver = driver;
        this.checkCandidate();
        switch (this.op) {
            case DbOperation.ADD:
                return await this.driver.add(this.candidate, this.parent);
            case DbOperation.DELETE:
                return await this.driver.deleteRows(this.candidate);
            case DbOperation.UPDATE:
                return await this.driver.update(this.candidate, this.parent);
            case DbOperation.LOAD:
                this.checkQuery();
                return await this.driver.load(this.query);
        }
    }
}
class FxRet {
    constructor(data, error, errorCode) {
        this.errorCode = errorCode;
        this.error = error;
        this.data = data;
    }
    hasError() {
        if (this.errorCode < 0)
            return true;
        return false;
    }
    hasWarning() {
        if (this.errorCode > 0)
            return true;
        return false;
    }
    static decode(source) {
        return new FxRet(source.data, source.error, source.errorCode);
    }
}
class EntryPointArgs {
    constructor(className, fx, fxArgs, fxRet) {
        this.className = className;
        this.fx = fx;
        this.fxArgs = fxArgs;
        this.fxRet = fxRet;
    }
    encode() {
        return JSON.stringify(this);
    }
    static decode(source) {
        if (source == null)
            return;
        let fxRet = FxRet.decode(source.fxRet);
        return new EntryPointArgs(source.className, source.fx, source.fxArgs, fxRet);
    }
}
//
// static global shared pino logger
//
var log = null;
function setLogger(logger) {
    log = logger;
}

//# sourceMappingURL=ABCCommon.js.map

/***/ }),

/***/ "../common/lib/DbProtocol.js":
/*!***********************************!*\
  !*** ../common/lib/DbProtocol.js ***!
  \***********************************/
/*! exports provided: DBCore, SqlResult */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DBCore", function() { return DBCore; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SqlResult", function() { return SqlResult; });
/* harmony import */ var _ABCCommon__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./ABCCommon */ "../common/lib/ABCCommon.js");
//
// Common Database protocol
//

var SqlType;
(function (SqlType) {
    SqlType[SqlType["Text"] = 0] = "Text";
    SqlType[SqlType["Text64"] = 1] = "Text64";
    SqlType[SqlType["Integer32"] = 2] = "Integer32";
    SqlType[SqlType["UInteger64"] = 3] = "UInteger64";
    SqlType[SqlType["Integer64"] = 4] = "Integer64";
    SqlType[SqlType["Real"] = 5] = "Real";
    SqlType[SqlType["Blob"] = 6] = "Blob";
})(SqlType || (SqlType = {}));
class DBCore {
    constructor(name, type) {
        this.name = name;
        this.type = type;
    }
    static driverTypeFromString(name) {
        switch (name) {
            case 'SqlLite':
                return _ABCCommon__WEBPACK_IMPORTED_MODULE_0__["DBType"].Sqlite;
            default:
                return _ABCCommon__WEBPACK_IMPORTED_MODULE_0__["DBType"].Undefined;
        }
    }
    getDbType() {
        switch (this.type) {
            case _ABCCommon__WEBPACK_IMPORTED_MODULE_0__["DBType"].Sqlite:
                return 'Sqlite';
            default:
                return 'Not Implemented';
        }
    }
}
/**
 Database Access common protocol
 */
class SqlResult {
    constructor(data, errorCode, error) {
        this.error = error;
        this.data = data;
        this.errorCode = errorCode;
    }
    isOk() {
        if (this.errorCode >= 0)
            return true;
        return false;
    }
    isWarning() {
        if (this.errorCode == null)
            return false;
        if (this.errorCode > 0)
            return true;
        return false;
    }
    isError() {
        if (this.errorCode == null)
            return false;
        if (this.errorCode < 0)
            return true;
        return false;
    }
    check() {
        if (this.error != null) {
            throw new Error(this.error);
        }
        else {
            return this.data;
        }
    }
    static setError(msg) {
        return new SqlResult(null, -1, msg);
    }
    static setWarning(msg, data) {
        return new SqlResult(data, +1, msg);
    }
    static setData(data) {
        return new SqlResult(data, 0, null);
    }
}

//# sourceMappingURL=DbProtocol.js.map

/***/ }),

/***/ "../common/lib/samplehello.js":
/*!************************************!*\
  !*** ../common/lib/samplehello.js ***!
  \************************************/
/*! exports provided: SampleTest, testValue */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SampleTest", function() { return SampleTest; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "testValue", function() { return testValue; });
const testValue = 42;
class SampleTest {
    hello(msg) {
        console.log(`Hello class has received this: ${msg}`);
    }
}

//# sourceMappingURL=samplehello.js.map

/***/ }),

/***/ "./src/API/ABCApi.ts":
/*!***************************!*\
  !*** ./src/API/ABCApi.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;!(__WEBPACK_AMD_DEFINE_ARRAY__ = [__webpack_require__, exports, __webpack_require__(/*! @common/ABCCommon */ "../common/lib/ABCCommon.js"), __webpack_require__(/*! @/dbdrivers/AbcDbRequestFactory */ "./src/dbdrivers/AbcDbRequestFactory.ts")], __WEBPACK_AMD_DEFINE_RESULT__ = (function (require, exports, ABCCommon_1, AbcDbRequestFactory_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    exports.ABCApi = void 0;
    /**
     * external Communication API args
     */
    class ABCApi {
        constructor(body, res) {
            this.res = null;
            this.apiModel = null;
            this.res = res;
            this.apiModel = body;
        }
        checkParams() {
            if (this.apiModel.className == null) {
                return false;
            }
            if (this.apiModel.fx == null) {
                return false;
            }
            return true;
        }
        async repository(fx, jsonRepo = `
     {
       "core" : {
         "name" : "defaultRepository" ,
         "type" : "SqlLite"
        }
      }  
    `) {
            let request = new AbcDbRequestFactory_1.ABCDbJsonRequest();
            let dbResult = await request.doRequest(fx, jsonRepo);
            let fxRet;
            if (dbResult.isOk()) {
                let data = [];
                data.push(dbResult.data);
                fxRet = new ABCCommon_1.FxRet(data, dbResult.error, dbResult.errorCode);
            }
            else {
                fxRet = new ABCCommon_1.FxRet(null, dbResult.error, dbResult.errorCode);
                throw new Error(dbResult.error);
            }
            this.apiModel.fx = ABCCommon_1.DatabaseActionUtil.toString(fx);
            this.apiModel.fxRet = fxRet;
            this.res.send(JSON.stringify(this.apiModel));
        }
        getArgs0() {
            if (this.apiModel.fxArgs == null)
                return null;
            return this.apiModel.fxArgs[0];
        }
        handleRequest() {
            let errMsg = null;
            if (this.checkParams()) {
                let className = this.apiModel.className;
                let fxName = this.apiModel.fx;
                let arg0 = this.getArgs0();
                switch (className) {
                    case 'AbcRepositoryAPI':
                        switch (fxName) {
                            case 'storageOperation':
                                this.repository(ABCCommon_1.DatabaseAction.StorageOperation, arg0).catch((err) => {
                                    this.res.status(500).send({
                                        error: `SERVER StorageOperation ERROR : ${err.message}`
                                    });
                                });
                                break;
                            case 'getScoreRepoList':
                                this.repository(ABCCommon_1.DatabaseAction.ListDatabase).catch((err) => {
                                    this.res.status(500).send({
                                        error: `SERVER getScoreReposlist ERROR : ${err.message}`
                                    });
                                });
                                break;
                            case 'createRepository':
                                this.repository(ABCCommon_1.DatabaseAction.CreateDatabase, arg0).catch((err) => {
                                    this.res.status(500).send({
                                        error: `SERVER CreateDatabase ERROR : ${err.message}`
                                    });
                                });
                                break;
                            case 'deleteRepository':
                                this.repository(ABCCommon_1.DatabaseAction.DeleteDatabase, arg0).catch((err) => {
                                    this.res.status(500).send({
                                        error: `SERVER DeleteDatabase ERROR : ${err.message}`
                                    });
                                });
                                break;
                            default:
                                errMsg = `Undefined API : ${this.apiModel.className}.${fxName}`;
                        }
                        break;
                    default:
                        errMsg = `Invalid API className : ${this.apiModel.className}`;
                        break;
                }
            }
            else {
                errMsg = `Invalid API message : ${JSON.stringify(this.apiModel)}`;
            }
            if (errMsg != null) {
                this.res.status(500).send({ error: `SERVER SIDE ERROR : ${errMsg}` });
            }
        }
    }
    exports.ABCApi = ABCApi;
}).apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__),
				__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));


/***/ }),

/***/ "./src/dbdrivers/ABCDb.ts":
/*!********************************!*\
  !*** ./src/dbdrivers/ABCDb.ts ***!
  \********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
!(__WEBPACK_AMD_DEFINE_ARRAY__ = [__webpack_require__, exports, __webpack_require__(/*! fs */ "fs"), __webpack_require__(/*! path */ "path"), __webpack_require__(/*! @common/DbProtocol */ "../common/lib/DbProtocol.js"), __webpack_require__(/*! @common/ABCCommon */ "../common/lib/ABCCommon.js"), __webpack_require__(/*! ../dbdrivers/SqliteDriver */ "./src/dbdrivers/SqliteDriver.ts"), __webpack_require__(/*! @common/DbProtocol */ "../common/lib/DbProtocol.js"), __webpack_require__(/*! ../utils/ABCLogger */ "./src/utils/ABCLogger.ts")], __WEBPACK_AMD_DEFINE_RESULT__ = (function (require, exports, fs_1, path_1, DbProtocol_1, ABCCommon_1, SqliteDriver_1, DbProtocol_2, ABCLogger_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    exports.ABCDb = void 0;
    fs_1 = __importDefault(fs_1);
    path_1 = __importDefault(path_1);
    const DATABASE_HOME = '/data/databases';
    class ABCDb {
        constructor(name, type) {
            this.operation = null;
            this.driver = null;
            let tp;
            if (typeof type === 'string') {
                tp = DbProtocol_1.DBCore.driverTypeFromString(type);
            }
            else {
                tp = type;
            }
            this.core = new DbProtocol_1.DBCore(name, tp);
            this.driver = this.getDriver();
        }
        static decode(source) {
            let returned = new ABCDb(source.core.name, source.core.type);
            returned.operation = ABCCommon_1.StorageOperation.decode(source.operation);
            return returned;
        }
        static getDbDirectory() {
            let current = process.cwd();
            // check existence
            const returned = current + DATABASE_HOME;
            if (fs_1.default.existsSync('./directory-name')) {
                return returned;
            }
            // create and return
            try {
                fs_1.default.mkdirSync(returned, { recursive: true });
                return returned;
            }
            catch (e) {
                ABCLogger_1.logger.error('SEVEREfails ');
            }
        }
        static dbFromFile(fName) {
            const ext = path_1.default.extname(fName);
            return new ABCDb(fName, ABCCommon_1.DBType.Sqlite);
        }
        static getScoreRepoList() {
            ABCLogger_1.logger.debug('getScoreRepoList entered');
            const dbHomeDir = ABCDb.getDbDirectory();
            let empty = true;
            let returned = [];
            fs_1.default.readdirSync(dbHomeDir).forEach((file) => {
                ABCLogger_1.logger.debug(`database found : ${file}`);
                returned.push(this.dbFromFile(file));
                empty = false;
            });
            return returned;
        }
        getDriver() {
            let driver = null;
            if (this.core.type == ABCCommon_1.DBType.Sqlite) {
                driver = new SqliteDriver_1.SqliteDriver(ABCDb.getDbDirectory(), this.core.name);
            }
            else {
                throw new Error(`Invalid(not implemented) database driver : ${this.core.type}`);
            }
            return driver;
        }
        async create() {
            ABCLogger_1.logger.debug(`Creating repository ${this.core.name} of ${this.core.type}`);
            return await this.driver.createDatabase();
        }
        async delete() {
            ABCLogger_1.logger.debug(`deleting repository ${this.core.name} of ${this.core.type}`);
            let deleted = await this.driver.deleteDatabase();
            return deleted;
        }
        exists() {
            return this.driver.existsDatabase();
        }
        async mount() {
            let result;
            if (!this.driver.existsDatabase()) {
                result = await this.create();
            }
            else {
                result = await this.driver.openDatabase();
            }
            return result;
        }
        async storageOperation() {
            if (this.operation != null) {
                if (this.driver != null) {
                    this.operation.test();
                    let returned = await this.operation.proceed(this.driver);
                    return returned;
                }
                else {
                    return DbProtocol_2.SqlResult.setError('Db Storage operation : driver missing');
                }
            }
            else {
                return DbProtocol_2.SqlResult.setError('db storage operation is null ');
            }
        }
        async unmount() {
            ABCLogger_1.logger.debug('closing database ...');
            let returned = await this.driver.closeDatabase();
            ABCLogger_1.logger.debug('database closed');
            return returned;
        }
    }
    exports.ABCDb = ABCDb;
}).apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__),
				__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));


/***/ }),

/***/ "./src/dbdrivers/AbcDbRequestFactory.ts":
/*!**********************************************!*\
  !*** ./src/dbdrivers/AbcDbRequestFactory.ts ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;!(__WEBPACK_AMD_DEFINE_ARRAY__ = [__webpack_require__, exports, __webpack_require__(/*! ./ABCDb */ "./src/dbdrivers/ABCDb.ts"), __webpack_require__(/*! @common/DbProtocol */ "../common/lib/DbProtocol.js"), __webpack_require__(/*! @common/ABCCommon */ "../common/lib/ABCCommon.js")], __WEBPACK_AMD_DEFINE_RESULT__ = (function (require, exports, ABCDb_1, DbProtocol_1, ABCCommon_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    exports.ABCDbJsonRequest = exports.AbcDbRequestFactory = void 0;
    class AbcDbRequestFactory {
        constructor() {
            this.parent = null;
            this.file = null;
            this.folder = null;
            this.query = null;
        }
        isFolder() {
            if (this.folder == null) {
                return false;
            }
            return true;
        }
        getOid() {
            if (this.file != null) {
                return this.file.oid;
            }
            if (this.folder != null) {
                return this.folder.oid;
            }
            return null;
        }
        setDb(dbName, dbType) {
            this.db = new ABCDb_1.ABCDb(dbName, dbType);
        }
        build(operation) {
            let candidate = new ABCCommon_1.ABCDbCandidate(this.isFolder(), this.getOid(), this.file, this.folder);
            let oper = new ABCCommon_1.StorageOperation(operation, this.parent, this.query);
            oper.candidate = candidate;
            this.db.operation = oper;
        }
        emit() {
            return JSON.stringify(this.db);
        }
        async exec() {
            let jsonRequest = this.emit();
            let request = new ABCDbJsonRequest();
            let returned = await request.doRequest(ABCCommon_1.DatabaseAction.StorageOperation, jsonRequest);
            return returned;
        }
        async deletedDb() {
            let jsonRequest = this.emit();
            let request = new ABCDbJsonRequest();
            return await request.doRequest(ABCCommon_1.DatabaseAction.DeleteDatabase, jsonRequest);
        }
        async createDb() {
            let jsonRequest = this.emit();
            let request = new ABCDbJsonRequest();
            return await request.doRequest(ABCCommon_1.DatabaseAction.CreateDatabase, jsonRequest);
        }
        async dbImport(root) {
            this.folder = root;
            this.build(ABCCommon_1.DbOperation.ADD);
            let returned = await this.exec();
            return returned;
        }
        async dbExport() {
            let oid = new ABCCommon_1.ABCOid(0);
            this.query = new ABCCommon_1.ABCQuery(true, oid);
            this.build(ABCCommon_1.DbOperation.LOAD);
            let returned = await this.exec();
            return returned;
        }
    }
    exports.AbcDbRequestFactory = AbcDbRequestFactory;
    class ABCDbJsonRequest {
        //
        async storage(db) {
            let dbResult = await db.mount();
            if (dbResult.isOk()) {
                dbResult = await db.storageOperation();
            }
            if (dbResult.isOk()) {
                await db.unmount(); // ignore close result
            }
            return dbResult;
        }
        async doRequest(fx, jsonRepo = `
    {
      "core" :
      {
        "name" : "defaultRepository" ,
        "type" : "SqlLite"
      }
    }  
   `) {
            let dbJsoned = JSON.parse(jsonRepo);
            let db = ABCDb_1.ABCDb.decode(dbJsoned);
            let jsonDest = jsonRepo;
            switch (fx) {
                case ABCCommon_1.DatabaseAction.CreateDatabase:
                    return await db.create();
                case ABCCommon_1.DatabaseAction.DeleteDatabase:
                    return await db.delete();
                case ABCCommon_1.DatabaseAction.ListDatabase:
                    let dbList = ABCDb_1.ABCDb.getScoreRepoList();
                    jsonDest = JSON.stringify(dbList);
                    return DbProtocol_1.SqlResult.setData(jsonDest);
                case ABCCommon_1.DatabaseAction.StorageOperation:
                    let returned = await this.storage(db);
                    return returned;
            }
        }
    }
    exports.ABCDbJsonRequest = ABCDbJsonRequest;
}).apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__),
				__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));


/***/ }),

/***/ "./src/dbdrivers/SQLDBModel.ts":
/*!*************************************!*\
  !*** ./src/dbdrivers/SQLDBModel.ts ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;!(__WEBPACK_AMD_DEFINE_ARRAY__ = [__webpack_require__, exports, __webpack_require__(/*! @common/ABCCommon */ "../common/lib/ABCCommon.js")], __WEBPACK_AMD_DEFINE_RESULT__ = (function (require, exports, ABCCommon_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    exports.ABCSqlTags = exports.ABCSqlLeaf = exports.ABCSqlFolder = exports.ABCSqlFile = exports.SqlType = exports.ABCSqlDataContent = void 0;
    var SqlType;
    (function (SqlType) {
        SqlType[SqlType["Text"] = 0] = "Text";
        SqlType[SqlType["Text64"] = 1] = "Text64";
        SqlType[SqlType["Integer32"] = 2] = "Integer32";
        SqlType[SqlType["UInteger64"] = 3] = "UInteger64";
        SqlType[SqlType["Integer64"] = 4] = "Integer64";
        SqlType[SqlType["Real"] = 5] = "Real";
        SqlType[SqlType["Blob"] = 6] = "Blob";
    })(SqlType || (SqlType = {}));
    exports.SqlType = SqlType;
    function checkResult(result) {
        if (result.isOk())
            return;
        throw new Error(result.error);
    }
    class ABCSqlFolder {
        constructor(folder, parent) {
            this.oid = null;
            this.leaf = null;
            this.folder = null;
            this.parent = null;
            this.folder = folder;
            this.parent = parent;
        }
        async rootExists(dbDriver) {
            let stmtStr = 'SELECT * FROM ABCFolder WHERE oid=0';
            let stmt = dbDriver.newStatement();
            let dbResult = await stmt.prepare(stmtStr);
            dbDriver.check(dbResult);
            dbResult = await stmt.fetch();
            let data = dbDriver.check(dbResult);
            await stmt.close();
            if (data != null) {
                return true;
            }
            return false;
        }
        async store(dbDriver) {
            var _a;
            if (this.folder == null) {
                throw new Error('storing null folder instance');
            }
            let stmtStr = `
      INSERT INTO ABCFolder
      (
        oid, leaf ,  parent
      )
      VALUES (?, ?  ,?)
     `;
            this.oid = new ABCCommon_1.ABCOid();
            if (this.parent == null) {
                // Root special key to avoid level indexing
                if (await this.rootExists(dbDriver)) {
                    this.parent = new ABCCommon_1.ABCOid(0);
                }
                else {
                    this.oid.oid = 0;
                }
            }
            let stmt = dbDriver.newStatement();
            this.folder.oid = this.oid;
            let f = this.folder;
            let binded = [f.oid.oid, null, (_a = this.parent) === null || _a === void 0 ? void 0 : _a.oid];
            ABCCommon_1.log.debug('ABCSqlFolder before storing ...');
            let dbResult = await stmt.exec(stmtStr, binded);
            dbDriver.check(dbResult);
            ABCCommon_1.log.debug('ABCSqlFolder stored OK');
            dbResult = await stmt.close();
            dbDriver.check(dbResult);
            ABCCommon_1.log.debug(`stored : ${stmtStr}`);
            // store Leaf first
            let leaf = f.infos;
            let dbLeaf = new ABCSqlLeaf(leaf, null, this);
            await dbLeaf.store(dbDriver);
            // fKey update
            await dbDriver.updateForeignKey('ABCFolder', 'leaf', this.oid, leaf.oid);
            // handle children
            if (f.folders != null) {
                for (let curFolder of f.folders) {
                    let curDbFolder = new ABCSqlFolder(curFolder, f.oid);
                    await curDbFolder.store(dbDriver);
                }
            }
            if (f.files != null) {
                for (let curFile of f.files) {
                    let curDbFile = new ABCSqlFile(curFile, f.oid);
                    await curDbFile.store(dbDriver);
                }
            }
        }
        async getChildren(dbDriver, stmtStr, isFolder) {
            let stmt1 = dbDriver.newStatement();
            let dbResult = await stmt1.prepare(stmtStr);
            dbDriver.check(dbResult);
            let childrenF = null;
            let child = (await stmt1.fetch()).check();
            while (child != null) {
                let fOid = new ABCCommon_1.ABCOid(child.oid);
                if (childrenF == null) {
                    childrenF = [];
                }
                if (isFolder) {
                    let f = new ABCSqlFolder();
                    await f.load(dbDriver, fOid);
                    childrenF.push(f.folder);
                }
                else {
                    let f = new ABCSqlFile();
                    await f.load(dbDriver, fOid);
                    childrenF.push(f.file);
                }
                child = (await stmt1.fetch()).check();
            }
            await stmt1.close();
            return childrenF;
        }
        async load(dbDriver, id, recursive = false) {
            let stmtStr = `SELECT * FROM ABCFolder WHERE oid=${id.oid}`;
            let stmt = dbDriver.newStatement();
            let dbResult = await stmt.prepare(stmtStr);
            dbDriver.check(dbResult);
            let row = (await stmt.fetch()).check();
            if (row != null) {
                this.leaf = new ABCCommon_1.ABCOid(row.leaf);
                let dbLeaf = new ABCSqlLeaf();
                await dbLeaf.load(dbDriver, this.leaf);
                this.folder = new ABCCommon_1.Folder(dbLeaf.leaf);
                this.folder.oid = id;
                // load children folders
                let stmtStr1 = `SELECT * FROM ABCFolder WHERE parent=${id.oid}`;
                let childrenFolders = await this.getChildren(dbDriver, stmtStr1, true);
                this.folder.folders = childrenFolders;
                // load children files
                stmtStr = `SELECT * FROM ABCFile WHERE parent=${id.oid}`;
                let childrenFiles = await this.getChildren(dbDriver, stmtStr, false);
                this.folder.files = childrenFiles;
            }
            await stmt.close();
        }
        async update(dbDriver) {
            if (this.folder == null) {
                throw new Error('updating  null folder instance');
            }
            let dbLeaf = new ABCSqlLeaf();
            dbLeaf.leaf = this.folder.infos;
            await dbLeaf.update(dbDriver);
        }
        async delete(dbDriver) {
            let delOid = null;
            if (this.oid != null) {
                delOid = this.oid;
            }
            else {
                if (this.folder != null) {
                    delOid = this.folder.oid;
                }
            }
            if (delOid == null) {
                throw new Error('deleting  null folder instance');
            }
            if (delOid.oid == 0) {
                throw new Error('deleting root folder forbidden');
            }
            await dbDriver.delete('ABCFolder', 'oid', delOid);
        }
        createSqlDDl(dbType) {
            return `
    CREATE TABLE ABCFolder
    (
    oid INTEGER PRIMARY KEY ,
    leaf INTEGER NULL ,
    parent INTEGER ,
    CONSTRAINT fk_ABCFolder
    FOREIGN KEY (parent)
    REFERENCES ABCFolder(oid)
    ON DELETE CASCADE
    ,
    CONSTRAINT fk_ABCLeaf
    FOREIGN KEY (leaf)
    REFERENCES ABCLeaf(oid)
    )
    `;
        }
        dropSqlDDl(dbType) {
            return `
    DROP TABLE IF EXISTS ABCFolder
    `;
        }
    }
    exports.ABCSqlFolder = ABCSqlFolder;
    class ABCSqlFile {
        constructor(file, parent) {
            this.oid = null;
            this.leaf = null;
            this.file = null;
            this.parent = null;
            this.file = file;
            this.parent = parent;
        }
        checkAbcData(infos) {
            if (infos.contents != null)
                return; // just here
            let exports = {
                exportExtra: false,
                exportSound: false,
                exportIcon: false
            };
            infos.makeExportable(exports);
        }
        async store(dbDriver) {
            if (this.file == null) {
                throw new Error('null file instance in ABCSqlFile.store');
            }
            this.checkAbcData(this.file.infos);
            let stmtStr = `
      INSERT INTO ABCFile
      (
        oid, leaf , parent
      )
      VALUES (?, ? , ?);
     `;
            let stmt = dbDriver.newStatement();
            this.oid = new ABCCommon_1.ABCOid();
            this.file.oid = this.oid;
            let binded = [this.oid.oid];
            let dbResult = await stmt.exec(stmtStr, binded);
            dbDriver.check(dbResult);
            stmt.close();
            ABCCommon_1.log.debug(`stored File : ${stmtStr}`);
            // proceed with leaf
            let leaf = this.file.infos;
            let dbLeaf = new ABCSqlLeaf(leaf, this, null);
            await dbLeaf.store(dbDriver);
            // Fkey updates
            dbDriver.updateForeignKey('ABCFile', 'leaf', this.file.oid, leaf.oid);
            dbDriver.updateForeignKey('ABCFile', 'parent', this.file.oid, this.parent);
        }
        async load(dbDriver, id, recursive = false) {
            let stmtStr = `SELECT * FROM ABCfile WHERE oid=${id.oid}`;
            let stmt = dbDriver.newStatement();
            await stmt.prepare(stmtStr);
            let row = (await stmt.fetch()).check();
            if (row != null) {
                this.leaf = new ABCCommon_1.ABCOid(row.leaf);
                let dbLeaf = new ABCSqlLeaf();
                await dbLeaf.load(dbDriver, this.leaf);
                this.file = new ABCCommon_1.File(dbLeaf.leaf);
                this.file.oid = id;
            }
            await stmt.close();
        }
        async update(dbDriver) {
            if (this.file == null) {
                throw new Error('updating  null file instance');
            }
            let dbLeaf = new ABCSqlLeaf();
            dbLeaf.leaf = this.file.infos;
            await dbLeaf.update(dbDriver);
        }
        async delete(dbDriver) {
            await dbDriver.delete('ABCFile', 'oid', this.oid);
        }
        createSqlDDl(dbType) {
            return `
    CREATE TABLE ABCFile
    (
    oid INTEGER PRIMARY KEY ,
    leaf INTEGER ,
    parent INTEGER ,
    CONSTRAINT fk_ABCFolder
    FOREIGN KEY (parent)
    REFERENCES ABCFolder(oid)
    ON DELETE CASCADE,
    CONSTRAINT fk_ABCLeaf
    FOREIGN KEY (leaf)
    REFERENCES ABCLeaf(oid)
    )
    `;
        }
        dropSqlDDl(dbType) {
            return `
    DROP TABLE IF EXISTS ABCFile
    `;
        }
    }
    exports.ABCSqlFile = ABCSqlFile;
    class ABCSqlDataContent {
        constructor(owner, content, parent) {
            this.oid = null;
            this.owner = null;
            this.content = null;
            this.parent = null;
            this.owner = owner;
            this.content = content;
            this.parent = parent;
        }
        async store(dbDriver) {
            var _a, _b, _c;
            let stmtStr = `
      INSERT INTO ABCDataContent
      (
        oid, owner , abc, image , icon , sound
      )
      VALUES (?, ? , ? ,? , ? , ?);
     `;
            let stmt = dbDriver.newStatement();
            this.content.oid = new ABCCommon_1.ABCOid();
            this.oid = this.content.oid;
            let binded = [
                this.content.oid.oid,
                (_b = (_a = this.parent) === null || _a === void 0 ? void 0 : _a.oid) === null || _b === void 0 ? void 0 : _b.oid,
                this.content.abc,
                this.content.imageB64,
                this.content.iconB64,
                this.content.soundB64
            ];
            await stmt.exec(stmtStr, binded);
            await stmt.close();
            // Fkey update
            dbDriver.updateForeignKey('ABCLeaf', 'contents', (_c = this.parent) === null || _c === void 0 ? void 0 : _c.oid, this.oid);
        }
        async load(dbDriver, id, recursive = false) {
            let stmtStr = `SELECT * FROM ABCDataContent WHERE oid=${id.oid}`;
            let stmt = dbDriver.newStatement();
            await stmt.prepare(stmtStr);
            let row = (await stmt.fetch()).check();
            if (row != null) {
                let dataContent = new ABCCommon_1.DataContent();
                dataContent.oid = id;
                dataContent.abc = row.abc;
                dataContent.iconB64 = row.iconB64;
                dataContent.imageB64 = row.imageB64;
                dataContent.soundB64 = row.soundB64;
                this.content = dataContent;
            }
            await stmt.close();
        }
        async update(dbDriver) {
            let stmtStr = `
      UPDATE ABCDataContent
      SET VALUE
        owner = ? ,
        abc = ?,
        image = ?,
        icon = ?,
        sound = ?
      WHERE oid = ${this.content.oid.oid}
     `;
            let stmt = dbDriver.newStatement();
            let binded = [
                this.content.oid,
                this.content.abc,
                this.content.imageB64,
                this.content.iconB64,
                this.content.soundB64
            ];
            await stmt.exec(stmtStr, binded);
            await stmt.close();
        }
        async delete(dbDriver) {
            await dbDriver.delete('ABCDataContent', 'oid', this.oid);
        }
        createSqlDDl(dbType) {
            return `
    CREATE TABLE ABCDataContent
    (
    oid INTEGER PRIMARY KEY ,
    owner INTEGER ,
    abc BLOB ,
    image  BLOB ,
    icon  BLOB ,
    sound BLOB ,
    CONSTRAINT fk_ABCLeaf
    FOREIGN KEY (owner)
    REFERENCES ABCLeaf(oid)
    ON DELETE CASCADE
    )
    `;
        }
        dropSqlDDl(dbType) {
            return `
    DROP TABLE IF EXISTS ABCDataContent
    `;
        }
    }
    exports.ABCSqlDataContent = ABCSqlDataContent;
    class ABCSqlLeaf {
        constructor(leaf, fileOwner, folderOwner) {
            this.oid = null;
            this.container = null;
            this.leaf = null;
            this.fileOwner = null;
            this.folderOwner = null;
            this.leaf = leaf;
            this.fileOwner = fileOwner;
            this.folderOwner = folderOwner;
        }
        getBinded(ms = Math.trunc(new Date().getTime())) {
            var _a, _b, _c, _d;
            return [
                this.oid.oid,
                (_b = (_a = this.fileOwner) === null || _a === void 0 ? void 0 : _a.oid) === null || _b === void 0 ? void 0 : _b.oid,
                (_d = (_c = this.folderOwner) === null || _c === void 0 ? void 0 : _c.oid) === null || _d === void 0 ? void 0 : _d.oid,
                this.leaf.title,
                ms,
                this.leaf.storageURL,
                this.leaf.iconURL,
                this.leaf.imageURL,
                this.leaf.soundURL,
                this.leaf.comments,
                null,
                this.leaf.encoding,
                this.leaf.type
            ];
        }
        hasContent() {
            if (this.leaf.contents != null &&
                this.leaf.contents.abc != null &&
                this.leaf.contents.abc.length != 0)
                return true;
            return false;
        }
        async store(dbDriver) {
            ABCCommon_1.log.debug('enter ABCSqlLeaf store ...');
            let stmtStr = `
      INSERT INTO ABCLeaf
      (
        oid, fileOwner , folderOwner, title , creation , storageURL ,iconURL ,
        imageURL ,soundURL , comments ,contents , encoding ,type
      )
      VALUES (?, ? , ? ,? ,? ,? ,? ,? ,? ,? ,? ,? ,? );
     `;
            let stmt = dbDriver.newStatement();
            this.oid = new ABCCommon_1.ABCOid();
            this.leaf.oid = this.oid;
            ABCCommon_1.log.debug(`ABCLEaf oid is : ${this.leaf.oid.oid} ; ${this.leaf.oid}`);
            let binded = this.getBinded();
            let dbResult = await stmt.exec(stmtStr, binded);
            dbDriver.check(dbResult);
            dbResult = await stmt.close();
            dbDriver.check(dbResult);
            ABCCommon_1.log.debug(`stored : ${stmtStr}`);
            if (this.hasContent()) {
                let sqlContent = new ABCSqlDataContent(this.leaf.oid, this.leaf.contents, this);
                await sqlContent.store(dbDriver);
            }
            // handle tags
            if (this.leaf.tags != null) {
                let dbTags = new ABCSqlTags(this);
                await dbTags.store(dbDriver);
            }
            // foreign key update
            if (this.folderOwner != null) {
                await dbDriver.updateForeignKey('ABCFolder', 'leaf', this.folderOwner.oid, this.leaf.oid);
            }
            if (this.fileOwner != null) {
                await dbDriver.updateForeignKey('ABCFile', 'leaf', this.fileOwner.oid, this.leaf.oid);
            }
        }
        async load(dbDriver, id, recursive = false) {
            let stmtStr = `SELECT * FROM ABCLeaf WHERE oid=${id.oid}`;
            let stmt = dbDriver.newStatement();
            await stmt.prepare(stmtStr);
            let row = (await stmt.fetch()).check();
            if (row != null) {
                this.leaf = new ABCCommon_1.Leaf(row.title);
                this.leaf.oid = row.oid;
                this.leaf.storageURL = row.storageURL;
                this.leaf.imageURL = row.imageURL;
                this.leaf.soundURL = row.soundURL;
                this.leaf.iconURL = row.iconURL;
                this.leaf.comments = row.comments;
                this.leaf.encoding = row.encoding;
                this.leaf.type = row.type;
                let contentID = row.contents;
                // get stored content next and populate it
                if (contentID != null) {
                    let contentOid = new ABCCommon_1.ABCOid(contentID);
                    let content = new ABCSqlDataContent();
                    await content.load(dbDriver, contentOid);
                }
            }
            await stmt.close();
        }
        async update(dbDriver) {
            if (this.leaf == null) {
                throw new Error('null lead instance on update request');
            }
            let stmtStr = `
      UPDATE ABCLeaf
      SET
        oid = ?,
        fileOwner = ? ,
        folderOwner = ? ,
        title = ? ,
        creation = ?,
        storageURL = ? ,
        iconURL = ? ,
        imageURL = ? ,
        soundURL = ? ,
        comments = ?,
        contents = ?,
        encoding = ?,
        type = ?
      WHERE oid=${this.leaf.oid.oid}
      `;
            let stmt = dbDriver.newStatement();
            let binded = this.getBinded(this.leaf.creation);
            await stmt.exec(stmtStr, binded);
            await stmt.close();
            if (this.leaf.contents != null) {
                let dbContent = new ABCSqlDataContent(this.leaf.oid, this.leaf.contents, this);
                await dbContent.update(dbDriver);
            }
            // handle tags
            if (this.leaf.tags != null) {
                let dbTags = new ABCSqlTags(this);
                await dbTags.update(dbDriver);
            }
        }
        async delete(dbDriver) {
            await dbDriver.delete('ABCLeaf', 'oid', this.oid);
        }
        createSqlDDl(dbType) {
            return `
    CREATE TABLE ABCLeaf
    (
    oid INTEGER PRIMARY KEY ,
    fileOwner INTEGER ,
    folderOwner INTEGER ,
    title  TEXT ,
    creation  INTEGER ,
    storageURL TEXT ,
    iconURL TEXT ,
    imageURL TEXT ,
    soundURL TEXT ,
    comments TEXT ,
    contents INTEGER ,
    encoding INTEGER ,
    type TEXT ,
    CONSTRAINT fk_ABCFile
    FOREIGN KEY (fileOwner)
    REFERENCES ABCFile(oid)
    ON DELETE CASCADE ,
    CONSTRAINT fk_ABCFolder
    FOREIGN KEY (folderOwner)
    REFERENCES ABCFolder(oid)
    ON DELETE CASCADE ,
    CONSTRAINT fk_ABCContent
    FOREIGN KEY (contents)
    REFERENCES ABCDataContent(oid)
    )
    `;
        }
        dropSqlDDl(dbType) {
            return `
    DROP TABLE IF EXISTS ABCLeaf
    `;
        }
    }
    exports.ABCSqlLeaf = ABCSqlLeaf;
    class ABCSqlTags {
        constructor(leaf) {
            this.oid = null; // tag ID
            this.container = null; // leaf container ID
            this.container = leaf;
        }
        parseTags(tagList) {
            if (tagList == null)
                return null;
            if (tagList.length > 0) {
                return tagList.split(',');
            }
            return null;
        }
        async store(dbDriver) {
            var _a;
            let leaf = this.container;
            if (leaf == null) {
                throw new Error('tag container is null on store method');
            }
            let stmtStr = `
    INSERT INTO ABCTags
      (
        oid, container , value , sequence
      )
    VALUES (?, ? , ? ,? );
    `;
            let tagsStr = (_a = leaf.leaf) === null || _a === void 0 ? void 0 : _a.tags;
            let tagPos = 0;
            let tags = this.parseTags(tagsStr);
            if (tags != null) {
                let stmt = dbDriver.newStatement();
                for (let tag of tags) {
                    let oid = new ABCCommon_1.ABCOid();
                    let binded = [oid.oid, this.container.oid.oid, tag, tagPos];
                    stmt.exec(stmtStr, binded);
                    tagPos++;
                }
                stmt.close();
            }
        }
        //
        // query on given tag value
        //
        async query(dbDriver, tag) {
            let stmtStr = `SELECT * FROM ABCTags WHERE value="${tag}"`;
            let stmt = dbDriver.newStatement();
            await stmt.prepare(stmtStr);
            let row = (await stmt.fetch()).check();
            let returned = null;
            while (row != null) {
                if (returned == null) {
                    returned = [];
                }
                let parent = row.container;
                let dbLeaf = new ABCSqlLeaf();
                dbLeaf.oid = new ABCCommon_1.ABCOid(parent);
                await dbLeaf.load(dbDriver, dbLeaf.oid);
                if (dbLeaf.fileOwner != null) {
                    returned.push(dbLeaf.fileOwner.file);
                }
                if (dbLeaf.folderOwner != null) {
                    returned.push(dbLeaf.folderOwner.folder);
                }
            }
            await stmt.close();
            return returned;
        }
        async load(dbDriver, id, recursive) {
            let stmtStr = `SELECT * FROM ABCTags WHERE container=${id.oid}`;
            let stmt = dbDriver.newStatement();
            await stmt.prepare(stmtStr);
            let tags = '';
            let row = (await stmt.fetch()).check();
            while (row != null) {
                tags += row.value;
                row = (await stmt.fetch()).check();
                if (row != null)
                    tags += ',';
            }
            await stmt.close();
            this.container.leaf.tags = tags;
        }
        async update(dbDriver) {
            await this.delete(dbDriver);
            await this.store(dbDriver);
        }
        async delete(dbDriver) {
            await dbDriver.delete('ABCTags', 'container', this.container.leaf.oid);
        }
        createIndex(dbType) {
            return `
      CREATE INDEX TagValueIndex
      ON ABCTags ( value )
    `;
        }
        createSqlDDl(dbType) {
            return `
    CREATE TABLE ABCTags
    (
    oid  INTEGER PRIMARY KEY ,
    container  INTEGER ,
    sequence  INTEGER ,
    value  TEXT ,
    CONSTRAINT fk_ABCLeaf
    FOREIGN KEY (container)
    REFERENCES ABCLeaf(oid)
    ON DELETE CASCADE
    )
    `;
        }
        dropSqlDDl(dbType) {
            return `
    DROP TABLE IF EXISTS ABCTags
    `;
        }
    }
    exports.ABCSqlTags = ABCSqlTags;
}).apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__),
				__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));


/***/ }),

/***/ "./src/dbdrivers/SqliteDriver.ts":
/*!***************************************!*\
  !*** ./src/dbdrivers/SqliteDriver.ts ***!
  \***************************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;//
// SqlLite Db driver
//
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
!(__WEBPACK_AMD_DEFINE_ARRAY__ = [__webpack_require__, exports, __webpack_require__(/*! sqlite3 */ "sqlite3"), __webpack_require__(/*! sqlite */ "sqlite"), __webpack_require__(/*! fs */ "fs"), __webpack_require__(/*! ../utils/ABCLogger */ "./src/utils/ABCLogger.ts"), __webpack_require__(/*! @common/DbProtocol */ "../common/lib/DbProtocol.js"), __webpack_require__(/*! @common/ABCCommon */ "../common/lib/ABCCommon.js"), __webpack_require__(/*! ./SQLDBModel */ "./src/dbdrivers/SQLDBModel.ts"), __webpack_require__(/*! lodash */ "lodash")], __WEBPACK_AMD_DEFINE_RESULT__ = (function (require, exports, sqlite3_1, sqlite_1, fs_1, ABCLogger_1, DbProtocol_1, ABCCommon_1, SQLDBModel_1, lodash_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    exports.SqliteDriver = void 0;
    sqlite3_1 = __importDefault(sqlite3_1);
    fs_1 = __importDefault(fs_1);
    const _DBTRACING_ = true;
    function emitOK() {
        return DbProtocol_1.SqlResult.setData(null);
    }
    function emitError(msg) {
        return DbProtocol_1.SqlResult.setError(msg);
    }
    function emitWarning(msg, data) {
        return DbProtocol_1.SqlResult.setWarning(msg, data);
    }
    function emitData(data) {
        return DbProtocol_1.SqlResult.setData(data);
    }
    //
    // Async sqlite primitives
    //
    class SqliteStatement {
        constructor(db) {
            this.db = db;
            this.stmt = null;
        }
        async exec(stmt, bind = []) {
            try {
                if (this.db == null)
                    return emitError('null db on Statement.exec');
                await this.db.run(stmt, bind);
                return emitOK();
            }
            catch (error) {
                return emitError(error.message);
            }
        }
        async prepare(sql) {
            try {
                this.stmt = await this.db.prepare(sql);
                if (this.stmt == null) {
                    return emitError(`failed to prepare SQL statement: ${sql}`);
                }
                return emitOK();
            }
            catch (error) {
                return emitError(error.message);
            }
        }
        async fetch() {
            try {
                if (this.stmt == null) {
                    return emitError('fetch on null statement');
                }
                let data = await this.stmt.get();
                return emitData(data);
            }
            catch (error) {
                return emitError(error.message);
            }
        }
        async close() {
            if (this.stmt == null)
                return emitOK();
            try {
                await this.stmt.finalize();
            }
            catch (error) {
                return emitError(error.message);
            }
        }
    }
    class SqliteDriver {
        // private p: SqliteFront = new SqliteFront()
        constructor(dbPath, name) {
            this.db = null;
            this.path = dbPath;
            // create dir if not there
            if (!fs_1.default.existsSync(this.path)) {
                fs_1.default.mkdirSync(this.path, { recursive: true });
            }
            this.name = name;
        }
        async delete(table, key, id) {
            let stmtStr = `DELETE FROM ${table} WHERE ${key}=${id.oid}`;
            let stmt = this.newStatement();
            await stmt.exec(stmtStr, null);
            await stmt.close();
        }
        static dbSuffix(name) {
            if (name.endsWith(this._DB_SUFFIX_))
                return name;
            return name + '.sqlite3.abcmusicstudio';
        }
        dbPath() {
            return this.path + '/' + SqliteDriver.dbSuffix(this.name);
        }
        reportError(msg, code) {
            return new Error(`ERROR code=${code} : ${msg}`);
        }
        async DDLExec(sql) {
            if (this.db != null) {
                let stmt = new SqliteStatement(this.db);
                let returned = await stmt.exec(sql);
                let closed = await stmt.close();
                return returned;
            }
        }
        check(dbResult) {
            if (dbResult.isOk()) {
                return dbResult.data;
            }
            else {
                throw new Error(dbResult.error);
            }
        }
        isOpened() {
            if (this.db == null) {
                return false;
            }
            return true;
        }
        async createDatabase() {
            ABCLogger_1.logger.debug('create database started ...');
            let returned = { name: this.name };
            if (!this.isOpened()) {
                await this.openDatabase();
            }
            let catalogOK = await this.checkCatalog();
            if (catalogOK) {
                await this.closeDatabase();
                return emitWarning(`database ${this.name} already exists`, JSON.stringify(returned));
            }
            let created = await this.createDataModel();
            ABCLogger_1.logger.debug('create database ended');
            ABCLogger_1.logger.debug('checking catalog again ...');
            catalogOK = await this.checkCatalog();
            if (catalogOK) {
                ABCLogger_1.logger.debug('DB Catalog is OK');
                return emitData(JSON.stringify(returned));
            }
            else {
                return emitError(`database ${this.name} Bad Db catalog`);
            }
        }
        async checkFileExists(file) {
            return fs_1.default.promises
                .access(file, fs_1.default.constants.F_OK)
                .then(() => true)
                .catch(() => false);
        }
        async deleteDatabase() {
            let returned = { name: this.name };
            if (this.db != null) {
                this.closeDatabase();
            }
            let candidate = this.dbPath();
            const exists = await this.checkFileExists(candidate);
            if (exists) {
                await fs_1.default.promises.unlink(candidate);
                return emitData(JSON.stringify(returned));
            }
            return emitWarning(`database ${this.name} does not exists`, JSON.stringify(returned));
        }
        async openDatabase() {
            ABCCommon_1.log.debug(`opening database : ${this.dbPath()}`);
            this.db = await sqlite_1.open({
                filename: this.dbPath(),
                driver: sqlite3_1.default.Database,
                mode: sqlite3_1.default.OPEN_READWRITE | sqlite3_1.default.OPEN_CREATE
            });
            if (_DBTRACING_) {
                this.db.on('trace', (data) => {
                    ABCCommon_1.log.debug(`DBTRACING = ${data}`);
                });
            }
            if (this.db == null) {
                return emitError(`Failed to open database : ${this.name}`);
            }
            return emitOK();
        }
        existsDatabase() {
            if (fs_1.default.existsSync(this.dbPath())) {
                return true;
            }
            return false;
        }
        async closeDatabase() {
            if (this.db != null) {
                await this.db.close();
            }
            this.db = null;
            return emitOK();
        }
        // nothing special before driver deallocation
        terminate() { }
        async checkCatalog() {
            ABCLogger_1.logger.debug('entering checkCatalog');
            if (this.db != null) {
                let stmtStr = `
      SELECT
          name
      FROM
          sqlite_master
      WHERE
          type ='table' AND
          name NOT LIKE 'sqlite_%'
      `;
                let stmt = this.newStatement();
                // let stmt = await this.select(stmtStr)
                let result = await stmt.prepare(stmtStr);
                this.check(result);
                ABCLogger_1.logger.debug('checkCatalog after select');
                //let row = (await this.fetch(stmt)).data
                result = await stmt.fetch();
                let row = this.check(result);
                ABCLogger_1.logger.debug('checkCatalog after fetch');
                await stmt.close();
                ABCLogger_1.logger.debug('checkCatalog after closestatement');
                if (row == null) {
                    ABCLogger_1.logger.debug('checkCatalog exit : NOT FOUND');
                    return false;
                }
                if (row.name == 'ABCFolder') {
                    ABCLogger_1.logger.debug('ABC db schema has been detected');
                    return true;
                }
                ABCLogger_1.logger.debug('checkCatalog exit : No ABCFolder found');
                return false;
            }
            else {
                lodash_1.reject('cannot checkCatalog : Db is closed');
            }
        }
        async createDataModel() {
            try {
                ABCLogger_1.logger.debug('creating ABC Datamodel ...');
                await this.DDLExec(new SQLDBModel_1.ABCSqlFolder().createSqlDDl(ABCCommon_1.DBType.Sqlite));
                await this.DDLExec(new SQLDBModel_1.ABCSqlFile().createSqlDDl(ABCCommon_1.DBType.Sqlite));
                await this.DDLExec(new SQLDBModel_1.ABCSqlLeaf().createSqlDDl(ABCCommon_1.DBType.Sqlite));
                await this.DDLExec(new SQLDBModel_1.ABCSqlDataContent().createSqlDDl(ABCCommon_1.DBType.Sqlite));
                await this.DDLExec(new SQLDBModel_1.ABCSqlTags().createSqlDDl(ABCCommon_1.DBType.Sqlite));
                await this.DDLExec(new SQLDBModel_1.ABCSqlTags().createIndex(ABCCommon_1.DBType.Sqlite));
                ABCLogger_1.logger.debug('ABC Datamodel created');
                return emitOK();
            }
            catch (err) {
                ABCLogger_1.logger.error(`datamodel creation failed : ${err.message}`);
                return emitError(`datamodel creation failed : ${err.message}`);
            }
        }
        async dropDataModel() {
            ABCLogger_1.logger.debug('dropping ABC Datamodel ...');
            await this.DDLExec(new SQLDBModel_1.ABCSqlFolder().dropSqlDDl(ABCCommon_1.DBType.Sqlite));
            await this.DDLExec(new SQLDBModel_1.ABCSqlFile().dropSqlDDl(ABCCommon_1.DBType.Sqlite));
            await this.DDLExec(new SQLDBModel_1.ABCSqlLeaf().dropSqlDDl(ABCCommon_1.DBType.Sqlite));
            await this.DDLExec(new SQLDBModel_1.ABCSqlDataContent().dropSqlDDl(ABCCommon_1.DBType.Sqlite));
            await this.DDLExec(new SQLDBModel_1.ABCSqlTags().dropSqlDDl(ABCCommon_1.DBType.Sqlite));
            await this.DDLExec(new SQLDBModel_1.ABCSqlTags().dropSqlDDl(ABCCommon_1.DBType.Sqlite));
            ABCLogger_1.logger.debug('ABC Datamodel dropped');
            return emitOK();
        }
        async updateForeignKey(table, col, pKey, fKey) {
            let stmt = this.newStatement();
            let stmtStr = 'UPDATE ' + table + ' set ' + col + '=? WHERE oid=?';
            let binded = [fKey.oid, pKey.oid];
            let dbResult = await stmt.exec(stmtStr, binded);
            this.check(dbResult);
            dbResult = await stmt.close();
            this.check(dbResult);
        }
        newStatement() {
            return new SqliteStatement(this.db);
        }
        async fileChange(candidate, parent, newFile = false) {
            let sqlFile = new SQLDBModel_1.ABCSqlFile(candidate.file, parent);
            try {
                if (newFile) {
                    await sqlFile.store(this);
                }
                else {
                    await sqlFile.update(this);
                }
                return new DbProtocol_1.SqlResult();
            }
            catch (error) {
                ABCCommon_1.log.error(error);
                return new DbProtocol_1.SqlResult(null, null, error.message);
            }
        }
        async folderChange(candidate, parent, newFolder = false) {
            let sqlFolder = new SQLDBModel_1.ABCSqlFolder(candidate.folder, parent);
            try {
                if (newFolder) {
                    await sqlFolder.store(this);
                }
                else {
                    await sqlFolder.update(this);
                }
                return emitOK();
            }
            catch (error) {
                ABCCommon_1.log.error(error);
                return emitError(error.message);
            }
        }
        async getFile(oid) {
            let sqlFile = new SQLDBModel_1.ABCSqlFile();
            try {
                await sqlFile.load(this, oid);
                return emitData(sqlFile.file);
            }
            catch (error) {
                ABCCommon_1.log.error(error);
                return emitError(error.message);
            }
        }
        async getFolder(oid) {
            let sqlFolder = new SQLDBModel_1.ABCSqlFolder();
            try {
                await sqlFolder.load(this, oid);
                return emitData(sqlFolder.folder);
            }
            catch (error) {
                ABCCommon_1.log.error(error);
                return emitError(error.message);
            }
        }
        //
        // add candidate to database (file or folder)
        //
        async add(candidate, parent) {
            if (candidate.file != null) {
                return this.fileChange(candidate, parent, true);
            }
            if (candidate.folder != null) {
                return this.folderChange(candidate, parent, true);
            }
            return emitError('Add method expect either file or folder not being null');
        }
        async deleteRows(candidate) {
            try {
                if (candidate.file != null) {
                    let sqlFile = new SQLDBModel_1.ABCSqlFile(candidate.file);
                    await sqlFile.delete(this);
                }
                if (candidate.folder != null) {
                    let sqlFolder = new SQLDBModel_1.ABCSqlFile(candidate.folder);
                    await sqlFolder.delete(this);
                }
                return emitError('Add method expect either file or folder not being null');
            }
            catch (error) {
                ABCCommon_1.log.error(error);
                return emitError(error.message);
            }
        }
        async update(candidate, parent) {
            if (candidate.file != null) {
                return this.fileChange(candidate, parent);
            }
            if (candidate.folder != null) {
                return this.folderChange(candidate, parent);
            }
            return emitError('Add method expect either file or folder not being null');
        }
        async load(query) {
            ABCCommon_1.log.debug('load operation started ...');
            let result;
            if (query.isFolder) {
                result = await this.getFolder(query.oid);
            }
            else {
                result = await this.getFile(query.oid);
            }
            ABCCommon_1.log.debug('load operation completed');
            return result;
        }
    }
    exports.SqliteDriver = SqliteDriver;
    SqliteDriver._DB_SUFFIX_ = '.sqlite3.abcmusicstudio';
}).apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__),
				__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));


/***/ }),

/***/ "./src/server.ts":
/*!***********************!*\
  !*** ./src/server.ts ***!
  \***********************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
!(__WEBPACK_AMD_DEFINE_ARRAY__ = [__webpack_require__, exports, __webpack_require__(/*! express */ "express"), __webpack_require__(/*! cors */ "cors"), __webpack_require__(/*! body-parser */ "body-parser"), __webpack_require__(/*! morgan */ "morgan"), __webpack_require__(/*! fs */ "fs"), __webpack_require__(/*! yargs */ "yargs"), __webpack_require__(/*! @/API/ABCApi */ "./src/API/ABCApi.ts"), __webpack_require__(/*! @/utils/ABCLogger */ "./src/utils/ABCLogger.ts"), __webpack_require__(/*! @common/ABCCommon */ "../common/lib/ABCCommon.js"), __webpack_require__(/*! @common/samplehello */ "../common/lib/samplehello.js")], __WEBPACK_AMD_DEFINE_RESULT__ = (function (require, exports, express_1, cors_1, body_parser_1, morgan_1, fs_1, yargs_1, ABCApi_1, ABCLogger_1, ABCCommon_1, samplehello_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    express_1 = __importDefault(express_1);
    cors_1 = __importDefault(cors_1);
    body_parser_1 = __importDefault(body_parser_1);
    morgan_1 = __importDefault(morgan_1);
    fs_1 = __importDefault(fs_1);
    yargs_1 = __importDefault(yargs_1);
    // just verify that references are working
    new samplehello_1.SampleTest().hello(samplehello_1.testValue.toString());
    //
    //
    class AbcStudioServer {
        constructor() {
            this.routePrv = new Routes();
            this.logLevel = process.env.LOG_LEVEL || 'info';
            console.log(`ABCStudio Express http loglevel is ${this.logLevel}`);
            // just verify that references are working
            this.app = express_1.default();
            this.logger = new ABCLogger_1.ABCLogger(this.logLevel, this.app);
            ABCCommon_1.setLogger(ABCLogger_1.ABCLogger.logger);
            this.homeDir = process.cwd();
            this.homePublic = this.homeDir + '/../client/dist';
        }
        processArgv() {
            const argv = yargs_1.default.option('port', {
                alias: 'p',
                type: 'number',
                default: 4000,
                describe: 'listening port number'
            }).argv;
            if (argv.port) {
                this.port = argv.port;
            }
            ABCCommon_1.log.info(`ABcStudioServer will listen on port : ${this.port}`);
        }
        //
        // basic configurator
        //
        config() {
            ABCCommon_1.log.info(`pino app logger is active in mode : ${this.logLevel}`);
            ABCCommon_1.log.info(`home dir is : ${this.homeDir}`);
            ABCCommon_1.log.info(`public home directory is : ${this.homePublic}`);
            // check homePublic
            if (!fs_1.default.existsSync(this.homePublic)) {
                ABCCommon_1.log.error(`Invalid public home directory => ABORTING : ${this.homePublic}`);
                process.exit(12);
            }
            this.app.use(morgan_1.default('tiny'));
            this.app.use(cors_1.default());
            this.app.use(body_parser_1.default.urlencoded({
                extended: true
            }));
            this.app.use(body_parser_1.default.json());
            //
            // set static path to home (vue js stuff)
            //
            this.app.use('/', express_1.default.static(this.homePublic, { index: 'indexStudio.html' }));
        }
        init() {
            this.config();
            this.processArgv();
            // config routes
            this.routePrv.routes(this.app);
        }
        listen() {
            this.app.listen(this.port, () => {
                ABCCommon_1.log.info(`listening for incoming sollicitors on ${abcStudioServer.port} ...`);
            });
        }
    }
    class NodesController {
        constructor() {
            this.api = (req, res) => {
                const args = this.getArgs(req);
                let api = new ABCApi_1.ABCApi(args, res);
                api.handleRequest();
            };
            this.badRoute = (req, res) => {
                res.statusCode = 404;
                res.end('404 Invalid ABCMusicStudio server request');
            };
        }
        getArgs(req) {
            if (req.method == 'GET') {
                return req.query;
            }
            else {
                return req.body;
            }
        }
    }
    class Routes {
        constructor() {
            this.API_ROUTE = '/API/:which';
            this.nodesController = new NodesController();
        }
        routes(app) {
            app.get(this.API_ROUTE, this.nodesController.api);
            app.post(this.API_ROUTE, this.nodesController.api);
            app.use(this.nodesController.badRoute);
        }
    }
    //
    // main entry point
    //
    const abcStudioServer = new AbcStudioServer();
    abcStudioServer.init();
    abcStudioServer.listen();
}).apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__),
				__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));


/***/ }),

/***/ "./src/utils/ABCLogger.ts":
/*!********************************!*\
  !*** ./src/utils/ABCLogger.ts ***!
  \********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var __WEBPACK_AMD_DEFINE_ARRAY__, __WEBPACK_AMD_DEFINE_RESULT__;var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
!(__WEBPACK_AMD_DEFINE_ARRAY__ = [__webpack_require__, exports, __webpack_require__(/*! pino */ "pino"), __webpack_require__(/*! express-pino-logger */ "express-pino-logger")], __WEBPACK_AMD_DEFINE_RESULT__ = (function (require, exports, pino_1, express_pino_logger_1) {
    "use strict";
    Object.defineProperty(exports, "__esModule", { value: true });
    exports.logger = exports.ABCLogger = void 0;
    pino_1 = __importDefault(pino_1);
    express_pino_logger_1 = __importDefault(express_pino_logger_1);
    class ABCLogger {
        constructor(logLevel, app) {
            if (logger == null) {
                if (logLevel == null) {
                    logLevel = 'debug';
                }
                exports.logger = logger = pino_1.default({ level: logLevel });
            }
            if (app != null) {
                this.expressLogger = express_pino_logger_1.default({ logger });
                app.use(this.expressLogger);
            }
            ABCLogger.logger = logger;
        }
    }
    exports.ABCLogger = ABCLogger;
    ABCLogger.logger = null;
    let logger = null;
    exports.logger = logger;
}).apply(exports, __WEBPACK_AMD_DEFINE_ARRAY__),
				__WEBPACK_AMD_DEFINE_RESULT__ !== undefined && (module.exports = __WEBPACK_AMD_DEFINE_RESULT__));


/***/ }),

/***/ "body-parser":
/*!******************************!*\
  !*** external "body-parser" ***!
  \******************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("body-parser");

/***/ }),

/***/ "cors":
/*!***********************!*\
  !*** external "cors" ***!
  \***********************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("cors");

/***/ }),

/***/ "express":
/*!**************************!*\
  !*** external "express" ***!
  \**************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("express");

/***/ }),

/***/ "express-pino-logger":
/*!**************************************!*\
  !*** external "express-pino-logger" ***!
  \**************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("express-pino-logger");

/***/ }),

/***/ "fs":
/*!*********************!*\
  !*** external "fs" ***!
  \*********************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("fs");

/***/ }),

/***/ "lodash":
/*!*************************!*\
  !*** external "lodash" ***!
  \*************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("lodash");

/***/ }),

/***/ "morgan":
/*!*************************!*\
  !*** external "morgan" ***!
  \*************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("morgan");

/***/ }),

/***/ "path":
/*!***********************!*\
  !*** external "path" ***!
  \***********************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("path");

/***/ }),

/***/ "pino":
/*!***********************!*\
  !*** external "pino" ***!
  \***********************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("pino");

/***/ }),

/***/ "sqlite":
/*!*************************!*\
  !*** external "sqlite" ***!
  \*************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("sqlite");

/***/ }),

/***/ "sqlite3":
/*!**************************!*\
  !*** external "sqlite3" ***!
  \**************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("sqlite3");

/***/ }),

/***/ "yargs":
/*!************************!*\
  !*** external "yargs" ***!
  \************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("yargs");

/***/ })

/******/ });
//# sourceMappingURL=main.js.map