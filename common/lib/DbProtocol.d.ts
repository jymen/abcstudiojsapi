import { ABCDbCandidate, ABCOid, ABCQuery, DBType } from './ABCCommon';
declare class DBCore {
    name: string | null;
    type: DBType | null;
    constructor(name: string, type: DBType);
    static driverTypeFromString(name: string): DBType;
    getDbType(): string;
}
/**
 Database Access common protocol
 */
declare class SqlResult {
    error: string;
    errorCode: number;
    data: any;
    constructor(data?: any, errorCode?: number, error?: string);
    isOk(): boolean;
    isWarning(): boolean;
    isError(): boolean;
    check(): any;
    static setError(msg: string): SqlResult;
    static setWarning(msg: string, data?: any): SqlResult;
    static setData(data: any): SqlResult;
}
interface DbProtocol {
    createDatabase(): Promise<SqlResult>;
    deleteDatabase(): Promise<SqlResult>;
    openDatabase(): Promise<SqlResult>;
    existsDatabase(): boolean;
    closeDatabase(): Promise<SqlResult>;
    terminate(): void;
    checkCatalog(): Promise<boolean>;
    add(candidate: ABCDbCandidate, parent: ABCOid): Promise<SqlResult>;
    deleteRows(candidate: ABCDbCandidate): Promise<SqlResult>;
    update(candidate: ABCDbCandidate, parent: ABCOid): Promise<SqlResult>;
    load(query: ABCQuery): Promise<SqlResult>;
}
export { DbProtocol, DBCore, SqlResult };
