//
// Common Database protocol
//
import { DBType } from './ABCCommon';
var SqlType;
(function (SqlType) {
    SqlType[SqlType["Text"] = 0] = "Text";
    SqlType[SqlType["Text64"] = 1] = "Text64";
    SqlType[SqlType["Integer32"] = 2] = "Integer32";
    SqlType[SqlType["UInteger64"] = 3] = "UInteger64";
    SqlType[SqlType["Integer64"] = 4] = "Integer64";
    SqlType[SqlType["Real"] = 5] = "Real";
    SqlType[SqlType["Blob"] = 6] = "Blob";
})(SqlType || (SqlType = {}));
class DBCore {
    constructor(name, type) {
        this.name = name;
        this.type = type;
    }
    static driverTypeFromString(name) {
        switch (name) {
            case 'SqlLite':
                return DBType.Sqlite;
            default:
                return DBType.Undefined;
        }
    }
    getDbType() {
        switch (this.type) {
            case DBType.Sqlite:
                return 'Sqlite';
            default:
                return 'Not Implemented';
        }
    }
}
/**
 Database Access common protocol
 */
class SqlResult {
    constructor(data, errorCode, error) {
        this.error = error;
        this.data = data;
        this.errorCode = errorCode;
    }
    isOk() {
        if (this.errorCode >= 0)
            return true;
        return false;
    }
    isWarning() {
        if (this.errorCode == null)
            return false;
        if (this.errorCode > 0)
            return true;
        return false;
    }
    isError() {
        if (this.errorCode == null)
            return false;
        if (this.errorCode < 0)
            return true;
        return false;
    }
    check() {
        if (this.error != null) {
            throw new Error(this.error);
        }
        else {
            return this.data;
        }
    }
    static setError(msg) {
        return new SqlResult(null, -1, msg);
    }
    static setWarning(msg, data) {
        return new SqlResult(data, +1, msg);
    }
    static setData(data) {
        return new SqlResult(data, 0, null);
    }
}
export { DBCore, SqlResult };
//# sourceMappingURL=DbProtocol.js.map