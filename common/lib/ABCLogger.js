/**
 * Pino Logger client class
 */
/* eslint-disable no-console */
import pino from 'pino';
class ABCLogger {
    constructor(logLevel) {
        if (!ABCLogger.initted) {
            ABCLogger.initted = true;
            if (logLevel == null) {
                logLevel = 'debug';
            }
            logger = pino({
                level: logLevel,
                browser: {
                    asObject: true,
                    write: {
                        debug: function (msg) {
                            const s = new Date(msg.time).toISOString();
                            console.debug(`${s} : DEBUG : ${msg.msg}`);
                        },
                        info: function (msg) {
                            const s = new Date(msg.time).toISOString();
                            console.info(`${s} : INFO : ${msg.msg}`);
                        },
                        warning: function (msg) {
                            const s = new Date(msg.time).toISOString();
                            console.warn(`${s} : WARNING : ${msg.msg}`);
                        },
                        error: function (msg) {
                            const s = new Date(msg.time).toISOString();
                            console.error(`${s} : ERROR : ${msg.msg}`);
                        }
                    }
                }
            });
        }
        ABCLogger.logger = logger;
    }
}
ABCLogger.initted = false;
let logger;
export { ABCLogger, logger };
//# sourceMappingURL=ABCLogger.js.map