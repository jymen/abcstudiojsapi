declare const testValue = 42;
declare class SampleTest {
    hello(msg: string): void;
}
export { SampleTest, testValue };
