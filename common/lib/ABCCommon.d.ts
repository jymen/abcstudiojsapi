/**
 * COMMON client server structures
 * WARNING
 * THIS File is shared through a symbolic link and must
 * only contain client / server compatible code
 */
import pino from 'pino';
import { DbProtocol, SqlResult } from './DbProtocol';
interface ABCTreeElement {
}
declare enum ABCResType {
    ABCScore = 0,
    Group = 1,
    GroupWithFolder = 2,
    XmlMusicScore = 3
}
declare enum DatabaseAction {
    CreateDatabase = 0,
    DeleteDatabase = 1,
    ListDatabase = 2,
    StorageOperation = 3,
    Undefined = 4
}
declare class DatabaseActionUtil {
    static toString(action: DatabaseAction): "CreateDatabase" | "DeleteDatabase" | "ListDatabase" | "StorageOperation" | "UndefinedAction";
}
interface ExportOptions {
    exportExtra: boolean;
    exportSound: boolean;
    exportIcon: boolean;
}
declare class DataContent {
    oid: ABCOid;
    encoded: boolean;
    abc: string;
    imageB64: string;
    iconB64: string;
    soundB64: string;
    static decode(source: any): DataContent;
}
declare class Leaf {
    oid: ABCOid;
    title: string;
    creation: number;
    tags: string;
    storageURL: string;
    iconURL: string;
    imageURL: string;
    imageZoom: number;
    imageBgColor: string;
    soundURL: string;
    comments: string;
    contents: DataContent;
    encoding: number;
    type: ABCResType;
    constructor(title: string);
    makeExportable(exportType: ExportOptions): void;
    static decode(source: any): Leaf;
}
declare class File implements ABCTreeElement {
    oid: ABCOid;
    infos: Leaf;
    parent: Folder;
    constructor(infos: Leaf, parent?: Folder);
    static decode(source: any): File;
}
declare class Folder implements ABCTreeElement {
    oid: ABCOid;
    infos: Leaf;
    files: File[];
    folders: Folder[];
    parent: Folder;
    constructor(infos: Leaf, parent?: Folder);
    static decode(source: any): Folder;
    addFile(infos: Leaf): void;
    addFolder(infos: Leaf): void;
    add(candidate: ABCTreeElement): void;
    remove(candidate: ABCTreeElement): void;
    /**
     * For debugging purposes
     */
    dump(): void;
    static buildClasses(from: any): Folder;
    dealWithParents(add: boolean): void;
}
interface DatabaseDataLoader {
    loadALL(): void | Error;
}
declare class ScoreDataStore {
    root: Folder;
    constructor();
    setRoot(data: any): void;
    private canBuildRoot;
    g: any;
    fromJson(jsonStr: string): void;
    unlinkParents(): void;
    linkParents(): void;
    fromLocalRepo(loader: DatabaseDataLoader): void;
}
declare enum DBType {
    Sqlite = 0,
    SqliteMemory = 1,
    MySql = 2,
    PostGreSql = 3,
    Undefined = 4
}
declare enum DBAction {
    CREATE,
    DELETE,
    LIST,
    STORAGEOP
}
declare enum DbOperation {
    ADD = 0,
    UPDATE = 1,
    DELETE = 2,
    LOAD = 3
}
declare class ABCOid {
    oid: number;
    emit(): number;
    FormatNumber(length: number): string;
    toString(): string;
    constructor(oid?: number);
    static decode(source: any): ABCOid;
}
declare class ABCQuery {
    isFolder: boolean;
    oid: ABCOid;
    constructor(isFolder: boolean, oid: ABCOid);
    static decode(source: any): ABCQuery;
}
declare class ABCDbCandidate {
    folder: Folder;
    file: File;
    private isFolder;
    private oid;
    constructor(isFolder: boolean, oid: ABCOid, file: File, folder: Folder);
    static decode(source: any): ABCDbCandidate;
}
declare class StorageOperation {
    op: DbOperation;
    parent: ABCOid;
    candidate: ABCDbCandidate;
    query: ABCQuery;
    completion: string;
    driver: DbProtocol;
    constructor(op: DbOperation, parent: ABCOid, query: ABCQuery);
    static decode(source: any): StorageOperation;
    private checkCandidate;
    private checkQuery;
    test(): void;
    proceed(driver: DbProtocol): Promise<SqlResult>;
}
declare class FxRet {
    errorCode: number;
    error: string;
    data: string[];
    constructor(data: string[], error: string, errorCode: number);
    hasError(): boolean;
    hasWarning(): boolean;
    static decode(source: any): FxRet;
}
declare class EntryPointArgs {
    className: string;
    fx: string;
    fxArgs: [any];
    fxRet: FxRet;
    constructor(className: string, fx: string, fxArgs?: [any], fxRet?: FxRet);
    encode(): string;
    static decode(source: any): EntryPointArgs;
}
declare var log: pino.Logger;
declare function setLogger(logger: pino.Logger): void;
export { ABCOid, ABCTreeElement, StorageOperation, ABCQuery, DbOperation, ExportOptions, ABCDbCandidate, DBType, File, Folder, ScoreDataStore, DataContent, Leaf, ABCResType, DatabaseAction, DatabaseActionUtil, DatabaseDataLoader, DBAction, setLogger, log, EntryPointArgs, FxRet, };
