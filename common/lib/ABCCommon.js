/**
 * COMMON client server structures
 * WARNING
 * THIS File is shared through a symbolic link and must
 * only contain client / server compatible code
 */
// import _ from 'lodash'
//
// Data Semantics
//
//
var ABCResType;
(function (ABCResType) {
    ABCResType[ABCResType["ABCScore"] = 0] = "ABCScore";
    ABCResType[ABCResType["Group"] = 1] = "Group";
    ABCResType[ABCResType["GroupWithFolder"] = 2] = "GroupWithFolder";
    ABCResType[ABCResType["XmlMusicScore"] = 3] = "XmlMusicScore";
})(ABCResType || (ABCResType = {}));
var DatabaseAction;
(function (DatabaseAction) {
    DatabaseAction[DatabaseAction["CreateDatabase"] = 0] = "CreateDatabase";
    DatabaseAction[DatabaseAction["DeleteDatabase"] = 1] = "DeleteDatabase";
    DatabaseAction[DatabaseAction["ListDatabase"] = 2] = "ListDatabase";
    DatabaseAction[DatabaseAction["StorageOperation"] = 3] = "StorageOperation";
    DatabaseAction[DatabaseAction["Undefined"] = 4] = "Undefined";
})(DatabaseAction || (DatabaseAction = {}));
class DatabaseActionUtil {
    static toString(action) {
        switch (action) {
            case DatabaseAction.CreateDatabase:
                return 'CreateDatabase';
            case DatabaseAction.DeleteDatabase:
                return 'DeleteDatabase';
            case DatabaseAction.ListDatabase:
                return 'ListDatabase';
            case DatabaseAction.StorageOperation:
                return 'StorageOperation';
            default:
                return 'UndefinedAction';
        }
    }
}
class DataContent {
    constructor() {
        this.oid = null;
    }
    // missing stuff to be completed later
    static decode(source) {
        if (source == null)
            return null;
        let returned = new DataContent();
        returned.encoded = source.encoded;
        returned.abc = source.abc;
        returned.oid = source.oid;
        returned.imageB64 = source.imageB64;
        returned.iconB64 = source.iconB64;
        returned.soundB64 = source.soundB64;
        return returned;
    }
}
class Leaf {
    constructor(title) {
        this.oid = null;
        this.title = title;
    }
    makeExportable(exportType) {
        // Browser NodeJS portability to be pondered HERE
        // for this method which may remain optional for the moment
    }
    static decode(source) {
        if (source == null)
            return null;
        let returned = new Leaf(source.title);
        returned.oid = ABCOid.decode(source.oid);
        returned.tags = source.tags;
        returned.contents = DataContent.decode(source.contents);
        returned.encoding = source.encoding;
        returned.type = source.type;
        returned.storageURL = source.storageURL;
        returned.iconURL = source.iconURL;
        returned.imageURL = source.imageURL;
        returned.imageZoom = source.imageZoom;
        returned.imageBgColor = source.imageBgColor;
        returned.soundURL = source.soundURL;
        returned.comments = source.comments;
        return returned;
    }
}
class File {
    constructor(infos, parent = null) {
        this.oid = null;
        this.infos = infos;
        this.parent = parent;
    }
    static decode(source) {
        if (source == null)
            return null;
        let leaf = Leaf.decode(source.infos);
        // parent is set to null to avoid circular refs
        let returned = new File(leaf, null);
        return returned;
    }
}
class Folder {
    constructor(infos, parent = null) {
        this.oid = null;
        this.infos = infos;
        this.files = [];
        this.folders = [];
        this.parent = parent;
    }
    static decode(source) {
        if (source == null)
            return null;
        let returned = Folder.buildClasses(source);
        return returned;
    }
    addFile(infos) {
        log.debug(`adding file ${infos.title}`);
        this.files.push(new File(infos, this));
    }
    addFolder(infos) {
        log.debug(`adding folder ${infos}`);
        this.folders.push(new Folder(infos, this));
    }
    add(candidate) {
        if ('files' in candidate) {
            this.folders.push(candidate);
        }
        else {
            this.files.push(candidate);
        }
    }
    remove(candidate) {
        if ('files' in candidate) {
            const folder = candidate;
            for (let ii = 0; ii < this.folders.length; ii++) {
                if (this.folders[ii].infos.title == folder.infos.title) {
                    this.folders.splice(ii, 1);
                }
            }
        }
        if ('content' in candidate) {
            const file = candidate;
            for (let ii = 0; ii < this.files.length; ii++) {
                if (this.files[ii].infos.title == file.infos.title) {
                    this.files.splice(ii, 1);
                }
            }
        }
    }
    /**
     * For debugging purposes
     */
    dump() {
        log.debug(`title = ${this.infos.title}`);
        log.debug('CHILD FOLDERS : ');
        for (const childFolder of this.folders) {
            childFolder.dump();
        }
        for (const childF of this.files) {
            log.debug(`child file title = ${childF.infos.title}`);
        }
    }
    static buildClasses(from) {
        const leaf = Leaf.decode(from.infos);
        const folder = new Folder(leaf, null);
        if (from.folders != null) {
            from.folders.forEach((element) => folder.add(Folder.buildClasses(element)));
        }
        if (from.files != null) {
            from.files.forEach((element) => folder.add(File.decode(element)));
        }
        return folder;
    }
    dealWithParents(add) {
        for (const childFolder of this.folders) {
            if (add) {
                childFolder.parent = this;
            }
            else {
                childFolder.parent = null;
            }
            childFolder.dealWithParents(add);
        }
        for (const childF of this.files) {
            if (add) {
                childF.parent = this;
            }
            else {
                childF.parent = null;
            }
        }
    }
}
class ScoreDataStore {
    constructor() {
        this.root = null;
    }
    setRoot(data) {
        if (data == null) {
            // build from scratch
            const leaf = new Leaf('This is the root tune');
            this.root = new Folder(leaf, null);
        }
        else if (typeof data === 'string') {
            // build from JSON
            this.fromJson(data);
        }
        else {
            // build from object
            this.root = data;
        }
        this.root.dump();
    }
    canBuildRoot(jsoned) {
        if (jsoned.folders != null) {
            let returned = {
                root: {
                    files: jsoned.files,
                    folders: jsoned.folders,
                    infos: jsoned.infos,
                },
            };
            return returned;
        }
        return null;
    }
    fromJson(jsonStr) {
        let jsoned = JSON.parse(jsonStr);
        if (jsoned.root == null) {
            jsoned = this.canBuildRoot(jsoned);
        }
        if (jsoned.root != null) {
            this.root = Folder.decode(jsoned.root);
            // this.root = jsoned.root.buildClasses(null)
        }
        else {
            this.root = null;
        }
    }
    unlinkParents() {
        // cleanup parents and stringify
        if (this.root != null) {
            this.root.dealWithParents(false);
        }
    }
    linkParents() {
        // cleanup parents and stringify
        if (this.root != null) {
            this.root.dealWithParents(true);
        }
    }
    fromLocalRepo(loader) {
        loader.loadALL();
    }
}
//
// Database Semantics
//
var DBType;
(function (DBType) {
    DBType[DBType["Sqlite"] = 0] = "Sqlite";
    DBType[DBType["SqliteMemory"] = 1] = "SqliteMemory";
    DBType[DBType["MySql"] = 2] = "MySql";
    DBType[DBType["PostGreSql"] = 3] = "PostGreSql";
    DBType[DBType["Undefined"] = 4] = "Undefined";
})(DBType || (DBType = {}));
var DBAction;
(function (DBAction) {
    DBAction[DBAction["CREATE"] = 'CreateDatabase'] = "CREATE";
    DBAction[DBAction["DELETE"] = 'DeleteDatabase'] = "DELETE";
    DBAction[DBAction["LIST"] = 'ListDatabase'] = "LIST";
    DBAction[DBAction["STORAGEOP"] = 'StorageOperation'] = "STORAGEOP";
})(DBAction || (DBAction = {}));
var DbOperation;
(function (DbOperation) {
    DbOperation[DbOperation["ADD"] = 0] = "ADD";
    DbOperation[DbOperation["UPDATE"] = 1] = "UPDATE";
    DbOperation[DbOperation["DELETE"] = 2] = "DELETE";
    DbOperation[DbOperation["LOAD"] = 3] = "LOAD";
})(DbOperation || (DbOperation = {}));
class ABCOid {
    constructor(oid = null) {
        this.oid = 0;
        if (oid == null) {
            this.oid = this.emit();
        }
        else {
            this.oid = oid;
        }
    }
    emit() {
        let min = Math.ceil(0);
        let max = Math.floor(5120000000);
        let random = Math.floor(Math.random() * (max - min)) + min;
        return new Date().getUTCMilliseconds() + random;
    }
    FormatNumber(length) {
        var r = '' + this.oid;
        while (r.length < length) {
            r = '0' + r;
        }
        return r;
    }
    toString() {
        let timestamp = this.oid.toString(16);
        return (timestamp +
            'xxxxxxxxxxxxxxxx'
                .replace(/[x]/g, function () {
                return ((Math.random() * 16) | 0).toString(16);
            })
                .toLowerCase());
    }
    static decode(source) {
        if (source == null)
            return null;
        let returned = new ABCOid(source.oid);
        return returned;
    }
}
class ABCQuery {
    constructor(isFolder, oid) {
        this.isFolder = isFolder;
        this.oid = oid;
    }
    static decode(source) {
        if (source == null)
            return null;
        return new ABCQuery(source.isFolder, ABCOid.decode(source.oid));
    }
}
class ABCDbCandidate {
    constructor(isFolder, oid, file, folder) {
        this.folder = null;
        this.file = null;
        this.isFolder = false;
        this.oid = null;
        this.isFolder = isFolder;
        this.oid = oid;
        this.file = file;
        this.folder = folder;
    }
    static decode(source) {
        if (source == null)
            return null;
        let folder = Folder.decode(source.folder);
        let file = File.decode(source.file);
        let oid = ABCOid.decode(source.oid);
        let returned = new ABCDbCandidate(source.isFolder, oid, file, folder);
        return returned;
    }
}
class StorageOperation {
    constructor(op, parent, query) {
        this.parent = null;
        this.candidate = null;
        this.query = null;
        this.completion = null;
        this.driver = null;
        this.op = op;
        this.parent = parent;
        this.candidate = null;
        this.query = query;
    }
    static decode(source) {
        if (source == null)
            return;
        let parent = ABCOid.decode(source.parent);
        let query = ABCQuery.decode(source.query);
        let returned = new StorageOperation(source.op, parent, query);
        returned.candidate = ABCDbCandidate.decode(source.candidate);
        return returned;
    }
    checkCandidate() {
        if (this.candidate == null) {
            throw new Error('Storage operation candidate is null');
        }
    }
    checkQuery() {
        if (this.query == null) {
            log.error('StorageOperation query is null');
            throw new Error('Storage operation query is null');
        }
    }
    test() {
        log.debug('StorageOperation test ENTERED');
    }
    async proceed(driver) {
        log.debug('PROCEEDING with storage operation');
        this.driver = driver;
        this.checkCandidate();
        switch (this.op) {
            case DbOperation.ADD:
                return await this.driver.add(this.candidate, this.parent);
            case DbOperation.DELETE:
                return await this.driver.deleteRows(this.candidate);
            case DbOperation.UPDATE:
                return await this.driver.update(this.candidate, this.parent);
            case DbOperation.LOAD:
                this.checkQuery();
                return await this.driver.load(this.query);
        }
    }
}
class FxRet {
    constructor(data, error, errorCode) {
        this.errorCode = errorCode;
        this.error = error;
        this.data = data;
    }
    hasError() {
        if (this.errorCode < 0)
            return true;
        return false;
    }
    hasWarning() {
        if (this.errorCode > 0)
            return true;
        return false;
    }
    static decode(source) {
        return new FxRet(source.data, source.error, source.errorCode);
    }
}
class EntryPointArgs {
    constructor(className, fx, fxArgs, fxRet) {
        this.className = className;
        this.fx = fx;
        this.fxArgs = fxArgs;
        this.fxRet = fxRet;
    }
    encode() {
        return JSON.stringify(this);
    }
    static decode(source) {
        if (source == null)
            return;
        let fxRet = FxRet.decode(source.fxRet);
        return new EntryPointArgs(source.className, source.fx, source.fxArgs, fxRet);
    }
}
//
// static global shared pino logger
//
var log = null;
function setLogger(logger) {
    log = logger;
}
export { ABCOid, StorageOperation, ABCQuery, DbOperation, ABCDbCandidate, DBType, File, Folder, ScoreDataStore, DataContent, Leaf, ABCResType, DatabaseAction, DatabaseActionUtil, DBAction, setLogger, log, EntryPointArgs, FxRet, };
//# sourceMappingURL=ABCCommon.js.map