/**
 * Pino Logger client class
 */
import pino from 'pino';
declare class ABCLogger {
    private static initted;
    static logger: pino.Logger;
    constructor(logLevel?: string | null);
}
declare let logger: pino.Logger;
export { ABCLogger, logger };
