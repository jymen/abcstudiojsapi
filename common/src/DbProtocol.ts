//
// Common Database protocol
//
import { ABCDbCandidate, ABCOid, ABCQuery, DBType } from './ABCCommon'
enum SqlType {
	Text,
	Text64,
	Integer32,
	UInteger64,
	Integer64,
	Real,
	Blob,
}

class DBCore {
	name: string | null
	type: DBType | null

	constructor(name: string, type: DBType) {
		this.name = name
		this.type = type
	}

	static driverTypeFromString(name: string): DBType {
		switch (name) {
			case 'SqlLite':
				return DBType.Sqlite
			default:
				return DBType.Undefined
		}
	}

	getDbType(): string {
		switch (this.type) {
			case DBType.Sqlite:
				return 'Sqlite'
			default:
				return 'Not Implemented'
		}
	}
}

/**
 Database Access common protocol
 */

class SqlResult {
	error: string
	errorCode: number
	data: any

	constructor(data?: any, errorCode?: number, error?: string) {
		this.error = error
		this.data = data
		this.errorCode = errorCode
	}

	isOk(): boolean {
		if (this.errorCode >= 0) return true
		return false
	}

	isWarning(): boolean {
		if (this.errorCode == null) return false
		if (this.errorCode > 0) return true
		return false
	}

	isError(): boolean {
		if (this.errorCode == null) return false
		if (this.errorCode < 0) return true
		return false
	}

	check(): any {
		if (this.error != null) {
			throw new Error(this.error)
		} else {
			return this.data
		}
	}

	static setError(msg: string): SqlResult {
		return new SqlResult(null, -1, msg)
	}

	static setWarning(msg: string, data?: any): SqlResult {
		return new SqlResult(data, +1, msg)
	}

	static setData(data: any): SqlResult {
		return new SqlResult(data, 0, null)
	}
}

interface DbProtocol {
	createDatabase(): Promise<SqlResult>
	deleteDatabase(): Promise<SqlResult>

	openDatabase(): Promise<SqlResult>
	existsDatabase(): boolean

	closeDatabase(): Promise<SqlResult>

	// do any cleanup requested
	terminate(): void

	checkCatalog(): Promise<boolean>

	add(candidate: ABCDbCandidate, parent: ABCOid): Promise<SqlResult>
	deleteRows(candidate: ABCDbCandidate): Promise<SqlResult>
	update(candidate: ABCDbCandidate, parent: ABCOid): Promise<SqlResult>
	load(query: ABCQuery): Promise<SqlResult>
}

export { DbProtocol, DBCore, SqlResult }
