const testValue = 42

class SampleTest {
	hello(msg: string) {
		console.log(`Hello class has received this: ${msg}`)
	}
}

export { SampleTest, testValue }
