/**
 * COMMON client server structures
 * WARNING
 * THIS File is shared through a symbolic link and must
 * only contain client / server compatible code
 */
// import _ from 'lodash'
//
// Data Semantics
//
//

import pino from 'pino'
import { DbProtocol, SqlResult } from './DbProtocol'

interface ABCTreeElement {}

enum ABCResType {
	ABCScore = 0,
	Group,
	GroupWithFolder,
	XmlMusicScore,
}

enum DatabaseAction {
	CreateDatabase,
	DeleteDatabase,
	ListDatabase,
	StorageOperation,
	Undefined,
}

class DatabaseActionUtil {
	public static toString(action: DatabaseAction) {
		switch (action) {
			case DatabaseAction.CreateDatabase:
				return 'CreateDatabase'
			case DatabaseAction.DeleteDatabase:
				return 'DeleteDatabase'
			case DatabaseAction.ListDatabase:
				return 'ListDatabase'
			case DatabaseAction.StorageOperation:
				return 'StorageOperation'
			default:
				return 'UndefinedAction'
		}
	}
}

interface ExportOptions {
	exportExtra: boolean
	exportSound: boolean
	exportIcon: boolean
}

class DataContent {
	oid: ABCOid = null
	encoded: boolean
	abc: string
	imageB64: string
	iconB64: string
	soundB64: string
	// missing stuff to be completed later

	static decode(source: any): DataContent {
		if (source == null) return null
		let returned = new DataContent()
		returned.encoded = source.encoded
		returned.abc = source.abc
		returned.oid = source.oid
		returned.imageB64 = source.imageB64
		returned.iconB64 = source.iconB64
		returned.soundB64 = source.soundB64
		return returned
	}
}

class Leaf {
	oid: ABCOid = null
	title: string
	creation: number
	tags: string
	storageURL: string
	iconURL: string
	imageURL: string
	imageZoom: number
	imageBgColor: string
	soundURL: string
	comments: string
	contents: DataContent
	encoding: number
	type: ABCResType

	constructor(title: string) {
		this.title = title
	}

	makeExportable(exportType: ExportOptions) {
		// Browser NodeJS portability to be pondered HERE
		// for this method which may remain optional for the moment
	}

	static decode(source: any): Leaf {
		if (source == null) return null
		let returned = new Leaf(source.title)
		returned.oid = ABCOid.decode(source.oid)
		returned.tags = source.tags
		returned.contents = DataContent.decode(source.contents)
		returned.encoding = source.encoding
		returned.type = source.type
		returned.storageURL = source.storageURL
		returned.iconURL = source.iconURL
		returned.imageURL = source.imageURL
		returned.imageZoom = source.imageZoom
		returned.imageBgColor = source.imageBgColor
		returned.soundURL = source.soundURL
		returned.comments = source.comments
		return returned
	}
}

class File implements ABCTreeElement {
	public oid: ABCOid = null
	public infos: Leaf
	public parent: Folder

	constructor(infos: Leaf, parent: Folder = null) {
		this.infos = infos
		this.parent = parent
	}

	static decode(source: any): File {
		if (source == null) return null
		let leaf = Leaf.decode(source.infos)
		// parent is set to null to avoid circular refs
		let returned = new File(leaf, null)
		return returned
	}
}

interface JsonedRepository {
	root: Folder
}

class Folder implements ABCTreeElement {
	public oid: ABCOid = null
	public infos: Leaf
	public files: File[]
	public folders: Folder[]
	public parent: Folder

	constructor(infos: Leaf, parent: Folder = null) {
		this.infos = infos
		this.files = []
		this.folders = []
		this.parent = parent
	}

	static decode(source: any): Folder {
		if (source == null) return null
		let returned = Folder.buildClasses(source)
		return returned
	}

	addFile(infos: Leaf) {
		log.debug(`adding file ${infos.title}`)
		this.files.push(new File(infos, this))
	}

	addFolder(infos: Leaf) {
		log.debug(`adding folder ${infos}`)
		this.folders.push(new Folder(infos, this))
	}

	add(candidate: ABCTreeElement) {
		if ('files' in candidate) {
			this.folders.push(candidate as Folder)
		} else {
			this.files.push(candidate as File)
		}
	}

	remove(candidate: ABCTreeElement) {
		if ('files' in candidate) {
			const folder = candidate as Folder
			for (let ii = 0; ii < this.folders.length; ii++) {
				if (this.folders[ii].infos.title == folder.infos.title) {
					this.folders.splice(ii, 1)
				}
			}
		}
		if ('content' in candidate) {
			const file = candidate as File
			for (let ii = 0; ii < this.files.length; ii++) {
				if (this.files[ii].infos.title == file.infos.title) {
					this.files.splice(ii, 1)
				}
			}
		}
	}

	/**
	 * For debugging purposes
	 */
	dump() {
		log.debug(`title = ${this.infos.title}`)
		log.debug('CHILD FOLDERS : ')
		for (const childFolder of this.folders) {
			childFolder.dump()
		}
		for (const childF of this.files) {
			log.debug(`child file title = ${childF.infos.title}`)
		}
	}

	static buildClasses(from: any): Folder {
		const leaf = Leaf.decode(from.infos)
		const folder = new Folder(leaf, null)
		if (from.folders != null) {
			from.folders.forEach((element) => folder.add(Folder.buildClasses(element)))
		}
		if (from.files != null) {
			from.files.forEach((element) => folder.add(File.decode(element)))
		}
		return folder
	}

	dealWithParents(add: boolean) {
		for (const childFolder of this.folders) {
			if (add) {
				childFolder.parent = this
			} else {
				childFolder.parent = null
			}
			childFolder.dealWithParents(add)
		}
		for (const childF of this.files) {
			if (add) {
				childF.parent = this
			} else {
				childF.parent = null
			}
		}
	}
}

interface DatabaseDataLoader {
	loadALL(): void | Error
}
class ScoreDataStore {
	root: Folder = null

	constructor() {}

	setRoot(data: any) {
		if (data == null) {
			// build from scratch
			const leaf = new Leaf('This is the root tune')
			this.root = new Folder(leaf, null)
		} else if (typeof data === 'string') {
			// build from JSON
			this.fromJson(data)
		} else {
			// build from object
			this.root = data as Folder
		}
		this.root.dump()
	}

	private canBuildRoot(jsoned: any): JsonedRepository {
		if (jsoned.folders != null) {
			let returned = {
				root: {
					files: jsoned.files,
					folders: jsoned.folders,
					infos: jsoned.infos,
				},
			}
			return returned as JsonedRepository
		}
		return null
	}
	g

	fromJson(jsonStr: string) {
		let jsoned = JSON.parse(jsonStr) as JsonedRepository
		if (jsoned.root == null) {
			jsoned = this.canBuildRoot(jsoned)
		}
		if (jsoned.root != null) {
			this.root = Folder.decode(jsoned.root)
			// this.root = jsoned.root.buildClasses(null)
		} else {
			this.root = null
		}
	}

	unlinkParents() {
		// cleanup parents and stringify
		if (this.root != null) {
			this.root.dealWithParents(false)
		}
	}

	linkParents() {
		// cleanup parents and stringify
		if (this.root != null) {
			this.root.dealWithParents(true)
		}
	}

	fromLocalRepo(loader: DatabaseDataLoader) {
		loader.loadALL()
	}
}

//
// Database Semantics
//

enum DBType {
	Sqlite,
	SqliteMemory,
	MySql, // Not implemented Yet
	PostGreSql, // not implemented Yet
	Undefined,
}

enum DBAction {
	CREATE = 'CreateDatabase' as any,
	DELETE = 'DeleteDatabase' as any,
	LIST = 'ListDatabase' as any,
	STORAGEOP = 'StorageOperation' as any,
}

enum DbOperation {
	ADD = 0,
	UPDATE,
	DELETE,
	LOAD,
}

class ABCOid {
	public oid = 0

	emit(): number {
		let min = Math.ceil(0)
		let max = Math.floor(5120000000)
		let random = Math.floor(Math.random() * (max - min)) + min
		return new Date().getUTCMilliseconds() + random
	}

	FormatNumber(length: number) {
		var r = '' + this.oid
		while (r.length < length) {
			r = '0' + r
		}
		return r
	}

	toString(): string {
		let timestamp = this.oid.toString(16)
		return (
			timestamp +
			'xxxxxxxxxxxxxxxx'
				.replace(/[x]/g, function () {
					return ((Math.random() * 16) | 0).toString(16)
				})
				.toLowerCase()
		)
	}

	constructor(oid: number = null) {
		if (oid == null) {
			this.oid = this.emit()
		} else {
			this.oid = oid
		}
	}

	static decode(source: any): ABCOid {
		if (source == null) return null
		let returned: ABCOid = new ABCOid(source.oid)
		return returned
	}
}

class ABCQuery {
	isFolder: boolean
	oid: ABCOid

	constructor(isFolder: boolean, oid: ABCOid) {
		this.isFolder = isFolder
		this.oid = oid
	}

	static decode(source: any): ABCQuery {
		if (source == null) return null
		return new ABCQuery(source.isFolder, ABCOid.decode(source.oid))
	}
}

class ABCDbCandidate {
	folder: Folder = null
	file: File = null
	private isFolder = false
	private oid: ABCOid = null

	constructor(isFolder: boolean, oid: ABCOid, file: File, folder: Folder) {
		this.isFolder = isFolder
		this.oid = oid
		this.file = file
		this.folder = folder
	}

	static decode(source: any): ABCDbCandidate {
		if (source == null) return null
		let folder = Folder.decode(source.folder)
		let file = File.decode(source.file)
		let oid = ABCOid.decode(source.oid)
		let returned: ABCDbCandidate = new ABCDbCandidate(source.isFolder, oid, file, folder)
		return returned
	}
}

class StorageOperation {
	op: DbOperation
	parent: ABCOid = null
	candidate: ABCDbCandidate = null
	query: ABCQuery = null
	completion: string = null
	driver: DbProtocol = null

	constructor(op: DbOperation, parent: ABCOid, query: ABCQuery) {
		this.op = op
		this.parent = parent
		this.candidate = null
		this.query = query
	}

	static decode(source: any): StorageOperation {
		if (source == null) return
		let parent: ABCOid = ABCOid.decode(source.parent)
		let query: ABCQuery = ABCQuery.decode(source.query)
		let returned: StorageOperation = new StorageOperation(source.op, parent, query)
		returned.candidate = ABCDbCandidate.decode(source.candidate)
		return returned
	}

	private checkCandidate() {
		if (this.candidate == null) {
			throw new Error('Storage operation candidate is null')
		}
	}

	private checkQuery() {
		if (this.query == null) {
			log.error('StorageOperation query is null')
			throw new Error('Storage operation query is null')
		}
	}

	test() {
		log.debug('StorageOperation test ENTERED')
	}

	async proceed(driver: DbProtocol): Promise<SqlResult> {
		log.debug('PROCEEDING with storage operation')
		this.driver = driver
		this.checkCandidate()
		switch (this.op) {
			case DbOperation.ADD:
				return await this.driver.add(this.candidate, this.parent)
			case DbOperation.DELETE:
				return await this.driver.deleteRows(this.candidate)
			case DbOperation.UPDATE:
				return await this.driver.update(this.candidate, this.parent)
			case DbOperation.LOAD:
				this.checkQuery()
				return await this.driver.load(this.query)
		}
	}
}

class FxRet {
	errorCode: number
	error: string
	data: string[]

	constructor(data: string[], error: string, errorCode: number) {
		this.errorCode = errorCode
		this.error = error
		this.data = data
	}

	hasError(): boolean {
		if (this.errorCode < 0) return true
		return false
	}

	hasWarning(): boolean {
		if (this.errorCode > 0) return true
		return false
	}

	static decode(source: any): FxRet {
		return new FxRet(source.data, source.error, source.errorCode)
	}
}

class EntryPointArgs {
	className: string
	fx: string
	fxArgs: [any]
	fxRet: FxRet

	constructor(className: string, fx: string, fxArgs?: [any], fxRet?: FxRet) {
		this.className = className
		this.fx = fx
		this.fxArgs = fxArgs
		this.fxRet = fxRet
	}

	encode(): string {
		return JSON.stringify(this)
	}

	static decode(source: any): EntryPointArgs {
		if (source == null) return
		let fxRet = FxRet.decode(source.fxRet)
		return new EntryPointArgs(source.className, source.fx, source.fxArgs, fxRet)
	}
}

//
// static global shared pino logger
//
var log: pino.Logger = null

function setLogger(logger: pino.Logger) {
	log = logger
}

export {
	ABCOid,
	ABCTreeElement,
	StorageOperation,
	ABCQuery,
	DbOperation,
	ExportOptions,
	ABCDbCandidate,
	DBType,
	File,
	Folder,
	ScoreDataStore,
	DataContent,
	Leaf,
	ABCResType,
	DatabaseAction,
	DatabaseActionUtil,
	DatabaseDataLoader,
	DBAction,
	setLogger,
	log,
	EntryPointArgs,
	FxRet,
}
