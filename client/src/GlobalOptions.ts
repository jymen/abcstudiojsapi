/**
 *
 * Global Options
 *
 */

const globalOptions = {
  abcZoom: 1.2,
  xmlZoom: 1.0
}

export { globalOptions }
