import Vue from 'vue'
import ABCMessage from './ABCMessage.vue'
// Inner Communication Tool
import { EventBus } from '../../event-bus'
import { ErrorEvent } from '../../API/ExternalApi'

class ABCMessageHandler {
  parentRef: any
  msgVue: any

  constructor(mountedRef: any) {
    this.parentRef = mountedRef
    this.msgVue = null
  }

  displayMessage(msg: ErrorEvent) {
    const self = this
    console.log(`NEW global error ${msg.action} ${msg.error} ${msg.type}`)
    const ErrorClass = Vue.extend(ABCMessage)
    this.msgVue = new ErrorClass({
      propsData: {
        title: msg.action,
        message: msg.error,
        type: msg.type
      }
    })
    this.msgVue.$mount()
    this.parentRef.appendChild(this.msgVue.$el)
    this.msgVue.modalOpen = true
    EventBus.$on('ABCMessageIsClosing', () => {
      self.discardMessage()
    })
  }

  discardMessage() {
    this.msgVue.modalOpen = false
    this.parentRef.removeChild(this.msgVue.$el)
  }
}

export { ABCMessageHandler }
