/**
TS Main entrypoint
*/

import Vue from 'vue'
import VueI18n from 'vue-i18n' // internationalization plugin
import VueRouter from 'vue-router'
import VueCodemirror from 'vue-codemirror'

// require styles
import 'codemirror/lib/codemirror.css'

//
// Icons import
//
import { library } from '@fortawesome/fontawesome-svg-core'
import {
  faMinus,
  faMinusSquare,
  faPlus,
  faSearch,
  faSearchMinus,
  faSearchPlus,
  faPaintRoller,
  faAngleDown,
  faAngleUp,
  faHome,
  faUpload,
  faCogs,
  faDatabase,
  faSave,
  faFolderPlus,
  faFolder,
  faTrash,
  faEye,
  faEdit
} from '@fortawesome/free-solid-svg-icons'

import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

import $ from 'jquery'
import store from './store/store'
import ScoreApp from './ScoreApp.vue'
import MainApp from './MainApp.vue'
import ABCAppPreferences from './components/MainApp/ABCAppPreferences.vue'
import ABCStudioHome from './components/MainApp/ABCStudioHome.vue'

import { Globals, ExternalAPI } from './API/ExternalApi'
// deal with recursive components
import ABCScoreLines from './components/MainApp/ABCScoreLines.vue'
import { SampleTest, testValue } from '../../common/lib/samplehello'
import { ABCLogger, logger } from '../../common/lib/ABCLogger'
import { setLogger, log } from '@common/ABCCommon'
import { ABCDbEventHandler } from './API/db/ABCDbEventHandler'

Vue.use(
  VueCodemirror /* {
  options: { theme: 'base16-dark', ... },
  events: ['scroll', ...]
} */
)
Vue.component('ABCScoreLines', ABCScoreLines) // recursive stuff

// set used icons
library.add(faPlus)
library.add(faMinus)
library.add(faMinusSquare)
library.add(faSearch)
library.add(faSearchMinus)
library.add(faSearchPlus)
library.add(faPaintRoller)
library.add(faAngleDown)
library.add(faAngleUp)
library.add(faHome)
library.add(faCogs)
library.add(faUpload)
library.add(faDatabase)
library.add(faSave)
library.add(faFolderPlus)
library.add(faFolder)
library.add(faTrash)
library.add(faEye)
library.add(faEdit)

Vue.component('font-awesome-icon', FontAwesomeIcon)

Vue.config.productionTip = false
// Plugins
Vue.use(VueI18n)
Vue.use(VueRouter)

// setup GLOBAL App logger in common
// to be used in dependencies
// eslint-disable-next-line
const l = new ABCLogger('debug')
setLogger(logger)

log.info('Main.ts has been entered')
log.debug('LOG IS IN DEBUG MODE')

const abcGlobals = new Globals()
// eslint-disable-next-line
const dbHandler = new ABCDbEventHandler()

//
//
//
//
// must cast as any to set property on window
// let abcjs = new AbcStudioJs()

function mainAppView() {
  const routes = [
    {
      path: '/home:title:message',
      component: ABCStudioHome,
      name: 'home',
      // Initial requested props for AbcStudioWelcome vue
      props: () => ({
        title: 'AbcMusicStudio Mobile',
        message: 'Welcome to ABCMusicStudio mobile',
        repoName: localStorage.getItem('repoName')
      })
    },
    {
      path: '/preferences',
      component: ABCAppPreferences,
      name: 'preferences'
    }
  ]

  // Create the router instance and pass the `routes` option
  const router = new VueRouter({
    routes
  })

  const app = new Vue({
    router,
    store,
    render: (h) => h(MainApp)
  }).$mount('#app')
  ;(window as any).abcGlobals.vm = app
  return 'OK'
}

function initAbc() {
  // just verify that references are working
  new SampleTest().hello(testValue.toString())
  initGlobals()
  log.info('ABCJS version' + abcGlobals.abcjs.getVersion())
  // Sample tester
  const ret = (window as any).abcStudioAPI.externalEntryPoint({
    className: 'abcjs',
    fx: 'ping',
    fxArgs: ['hello world ']
  })
  log.debug(`ret = ${ret.fxRet.data}`)

  // main.showHello('greeting','TypeScript')
  // abcjs.showScore("score", score);
  //webViewCallBack('test');
  // mainAppView()
  scoreAppView()
}

/*
function loadAbcjs() {
  let ckeditor = document.createElement('script')
  ckeditor.setAttribute('src', 'js/SingleFileComponent.js')
  document.head.appendChild(ckeditor)
}
*/

function scoreAppView() {
  // loadAbcjs()
  const app = new Vue({
    store,
    render: (h) => h(ScoreApp)
  }).$mount('#app')
  // make global instance for main app vue
  ;(window as any).abcGlobals.vm = app
  return 'OK'
}

//
// JS sample call test function
// may be used for initing
//
function sampleTest() {
  return 'sampleTest has been entered'
}

function androidCallbackTest(msg: string) {
  if ((window as any).androidObj != null) {
    if ((window as any).androidObj.textToAndroid != null) {
      log.debug('android callback received :' + msg)
      ;(window as any).androidObj.textToAndroid(msg)
      log.debug('android callback leaving')
    }
  }
}

/*=================================================*/
/* gentry point for JSCALLERS follows               */
/*=================================================*/
/*
 Apple NSHtmlView specific callback
 NB : Callee will assume that msg is a JSON parsable structure
 */
// eslint-disable-next-line
function webViewCallBack(msg: string) {
  if ((window as any).webkit != null) {
    ;(window as any).webkit.messageHandlers.jsCallBackHandler.postMessage(msg)
  }
}

// eslint-disable-next-line
function htmlStudioCallback(msg: string) {
  if ((window as any).mainAbcStudio != null) {
    ;(window as any).mainAbcStudio()
  }
}

function notifyInited() {
  const msg = 'HtmlViewInited'
  // IOS context
  webViewCallBack(msg)
  // Android context
  androidCallbackTest(msg)
  // browser context
  htmlStudioCallback(msg)
}

function initGlobals() {
  // Store external API in abcGlobals
  const externalAPI = new ExternalAPI()
  ;(window as any).abcGlobals = abcGlobals
  ;(window as any).abcGlobals.externalAPI = externalAPI
  ;(window as any).abcGlobals.abcjs.test(' with message')
  log.info((window as any).abcGlobals.abcjs.getVersion())
  // following is used for android callback implementation
  ;(window as any).androidObj = function AndroidClass() {}
  ;(window as any).abcStudioInjection = function abcStudioInjectorClass() {}
  ;(window as any).abcStudioAPI = new (window as any).abcStudioInjection()
  ;(window as any).abcStudioAPI.sampleTest = sampleTest
  ;(window as any).abcStudioAPI.androidCallbackTest = androidCallbackTest
  ;(window as any).abcStudioAPI.externalEntryPoint = externalAPI.entryPoint
  //
  // make Applications entry point public
  ;(window as any).abcStudioAPI.mainAppView = mainAppView
  ;(window as any).abcStudioAPI.scoreAppView = scoreAppView

  // export jquery as well
  // export for others scripts to use
  ;(window as any).$ = $
}

/*=================================================*/
/* entry point       (jquery init )       */
/*=================================================*/
$(function () {
  initAbc()
  notifyInited()
})

//
// Web html entry point
//
function initStudio() {
  log.debug('web studio initting ...')
  mainAppView()
}

export { initStudio }
