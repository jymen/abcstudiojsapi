//
// centralized localization module
//
const locales = {
  en: {
    message: {
      hello: 'hello world'
    }
  },
  fr: {
    message: {
      hello: 'Bonjour monde'
    }
  }
}

export { locales }
