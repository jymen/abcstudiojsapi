/**
 *
 * DB operation class
 *
 */

import * as _ from 'lodash'
import {
  ABCOid,
  ABCQuery,
  DbOperation,
  StorageOperation,
  ABCDbCandidate
} from '@common/ABCCommon'

import { ABCDb } from './db/ABCDb'
import { AbcIOSPeerAPI } from './ExternalApi'

class ABCDbRequestFactory {
  private db: ABCDb
  private parent: ABCOid
  private file: any
  private folder: any
  private query: ABCQuery
  private oid: ABCOid

  constructor(db: ABCDb) {
    this.db = db // the currently selected one
    this.parent = null
    this.file = null
    this.folder = null
    this.query = null
    this.oid = null
  }

  isFolder(): boolean {
    if (!_.isNil(this.oid)) {
      return this.folder
    }
    if (_.isNil(this.folder)) {
      return false
    } else {
      return true
    }
  }

  build(operation: DbOperation) {
    const candidate = new ABCDbCandidate(
      this.isFolder(),
      this.oid,
      this.file,
      this.folder
    )
    const oper = new StorageOperation(operation, this.parent, this.query)
    oper.candidate = candidate
    this.db.operation = oper
  }

  exec() {
    const api = new AbcIOSPeerAPI()
    api.storageOperation(this.db, this.db.operation.op)
  }

  mount() {
    // request mounting and check for db model
    // update inited
    const api = new AbcIOSPeerAPI()
    api.mount([this.db])
  }

  unmount() {
    const api = new AbcIOSPeerAPI()
    api.unMount([this.db])
  }

  loadALL() {
    this.folder = true
    this.oid = new ABCOid(0)
    this.query = new ABCQuery(this.folder, this.oid)
    this.build(DbOperation.LOAD)
    this.exec()
  }
}

export { ABCDbRequestFactory }
