/*

main entry point for ABCJs  API

*/

// import * as abcjs1 from 'abcjs6'
import * as abcjs from 'abcjs'
// import 'abcjs/abcjs-midi.css'
import 'abcjs/abcjs-audio.css'
import { NoteColoringMessage } from './CommonTypes'

// Inner Communication Tool
import { EventBus } from '../event-bus'

/**
  ABCJS API entry point
*/
class AbcStudioJs {
  mytest: string
  params: any

  constructor(test: string) {
    // default values for params
    this.mytest = test
    this.params = {
      staffwidth: 400
    }
  }

  getVersion(): string {
    return abcjs.signature
  }

  test(msg: string) {
    const ret = 'test API from AbcStudioApi ' + msg
    console.log(ret)
    return ret
  }

  ping(args: string) {
    return args + ' received'
  }

  /**
   * set abcjs arguments
   * @param {*} params
   */
  setParams(params: any) {
    this.params = {} // cleanup
    for (const param in params) {
      this.params[param] = params[param]
    }
    return 'Done'
  }

  highlightNote(start: number, end: number, color: string, highlight: boolean) {
    console.log(' highlight/unhighlight note')
    const message: NoteColoringMessage = {
      highlight: highlight,
      start: start,
      end: end,
      color: color
    }
    EventBus.$emit('NoteColoringChange', message)
    return 'Done'
  }

  showScore(
    id: string,
    score: string,
    tuneNumber: number,
    program: any,
    bpm: number
  ) {
    //
    // Just emit notification event with score information
    // + MIDI selected program
    //
    EventBus.$emit(`ScoreDynamicChange${id}`, {
      type: 'Abc',
      score: score,
      options: this.params,
      midiprogram: program,
      bpm: bpm,
      tuneNumber: tuneNumber
    })
    return 'Done'
  }
}

export { AbcStudioJs }
