//
// define Common / shared exported Types
//

interface ScoreBottomMenuMessage {
  transpose: number
  downLoadMidi: boolean
}

interface NoteColoringMessage {
  highlight: boolean
  start: number
  end: number
  color: string
}

export { ScoreBottomMenuMessage, NoteColoringMessage }
