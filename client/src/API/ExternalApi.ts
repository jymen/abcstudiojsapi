/* eslint-disable no-unused-vars */
import $ from 'jquery'
import _ from 'lodash'

// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { DbOperation, EntryPointArgs } from '@common/ABCCommon'
import { ABCDb, DBCore } from '../API/db/ABCDb'
/* eslint-enable no-unused-vars */
import { AbcStudioJs } from './AbcStudioJs'
import { XmlMusicParser } from './xml2abc'
import { MusicXmlParser } from './MusicXmlParser'
import { sayHello } from '../greet'
import { TransferedScore } from './TransferedScore'
import store from '../store/store'

// Inner Communication Tool
import { EventBus } from '../event-bus'

class FxArguments {
  className: string
  fx: string
  fxArgs: string[]
  fxRet: any

  constructor(className: string, entryPoint: string, args: string[]) {
    this.className = className
    this.fx = entryPoint
    this.fxArgs = args
  }
}

/**
 * Global singleton reference
 */
class Globals {
  public abcjs: AbcStudioJs
  public xmlMusicParser: XmlMusicParser
  public musicXmlParser: MusicXmlParser
  public abcIOSScoreTransferAPI: AbcIOSScoreTransferAPI

  constructor() {
    this.abcjs = new AbcStudioJs('instance')
    this.xmlMusicParser = new XmlMusicParser()
    this.musicXmlParser = new MusicXmlParser()
    this.abcIOSScoreTransferAPI = new AbcIOSScoreTransferAPI()
  }

  helloWorld(test: string): void {
    $('#boo').html('<h1>Hello World ' + test + '</h1>')
  }

  showHello(divName: string, name: string): void {
    const elt: HTMLElement = document.getElementById(divName)
    elt.innerText = sayHello(name)
  }
}

class ExternalAPI {

  public iosOnly : boolean = false

  constructor() {}

  _returned(value: any): string {
    return JSON.stringify({
      args: {
        response: value
      }
    })
  }

  /**
   * use jquery to cleanup children
   * @param {} tag
   */
  cleanupChildren(tag: string): string {
    $(tag).children().remove()
    return 'Done'
  }

  //
  // standard IOS callbak
  //
  iosCallBack(className: string, fx: string, args?: string[]) {
    const message = {
      className: className,
      fx: fx,
      fxArgs: args
    }
    if ((window as any).webkit != null) {
      //
      // IOS webkit context hosting
      //
      const jsoned = JSON.stringify(message)
      ;(window as any).webkit.messageHandlers.jsCallBackHandler.postMessage(
        jsoned
      )
    } else {
      //
      // Try standard http
      //
      if ( !this.iosOnly ) {
        const axios = require('axios')
        const callReporter = new AbcIOSScoreTransferAPI()
        axios
          .post('/API/Repository', message)
          .then(function (response: any) {
            console.log(`SERVER SIDE JS API completed at : ${url}`)
            console.log(response)
            let data = EntryPointArgs.decode(response.data)
            if (data.fxRet.hasWarning()) {
              callReporter.actionWarning(data.fx, data.fxRet.error)
            }
            callReporter.actionCompleted(data.fx, data.fxRet.data[0])
          })
          .catch(function (error: any) {
            console.log(`SERVER SIDE JS API IN ERROR AT : ${url}`)
            console.log(error.response.data)
            callReporter.actionInError(
              `${className}.${fx}`,
              `SERVER SIDE JS API IN ERROR AT : ${url}`
            )
        })
        const url = window.location.host
      }  
    }
  }

  /**
    remote external function entry point
  */
  entryPoint(args: FxArguments) {
    args.fxRet = {} // defaut to empty return
    const ret = args.fxRet
    if (_.isObject(args)) {
      let className = null
      let objClass = null
      if ('className' in args) {
        className = args['className']
        objClass = (window as any).abcGlobals[className]
      }
      if ('fx' in args) {
        const calledFx = args['fx']
        const toCall = objClass[calledFx]
        if (typeof toCall === 'function') {
          const fxArgs = args.fxArgs
          if (_.isEmpty(fxArgs)) {
            ret.data = toCall()
          } else {
            if (_.isArray(fxArgs)) {
              ret.data = toCall.apply(objClass, args.fxArgs)
            } else {
              ret.error = 'arguments ARRAY is expected instead of : ' + fxArgs
            }
          }
        } else {
          ret.error = 'fx property missing in JSON string'
        }
      } else {
        ret.error = 'args property missing in JSON string'
      }
    } else {
      ret.error = 'JSON parse did not return a JS Object'
    }
    return args // return args back will calle data inside
  }
}

class ErrorEvent {
  action: string
  error: string
  type: string

  constructor(action: string, error: string, type: string) {
    this.action = action
    this.error = error
    this.type = type
  }
}

class AbcIOSScoreTransferAPI {
  constructor() {}

  //
  // reflecting peer connection state change
  // On IOS side
  //
  newState(state: string) {
    store.commit('newPeerTransferState', state)
    return 'Done'
  }

  //
  // report action error on current page
  //
  actionInError(action: string, error: string) {
    EventBus.$emit(
      'GlobalMessage',
      new ErrorEvent(action, `ERROR : ${error}`, 'danger')
    )
    return 'Done'
  }

  actionWarning(action: string, error: string) {
    EventBus.$emit(
      'GlobalMessage',
      new ErrorEvent(action, `WARNING : ${error}`, 'warning')
    )
    return 'Done'
  }

  refreshDbList() {
    const api = new AbcIOSPeerAPI()
    api.getScoreRepoList()
  }

  private toABCDbList(source: any[]): ABCDb[] {
    let returned: ABCDb[] = []
    for (let element of source) {
      let core = new DBCore(element.core.name, element.core.type)
      let db = new ABCDb(core)
      returned.push(db)
    }
    return returned
  }

  actionCompleted(action: string, targetJson: string) {
    let target = null
    if (!_.isNil(targetJson)) {
      target = JSON.parse(targetJson)
    }
    if (action == 'CreateDatabase') {
      console.log(`database ${target.name} created`)
      this.refreshDbList()
    } else if (action == 'DeleteDatabase') {
      console.log(`database ${target.name} deleted`)
      this.refreshDbList()
    } else if (action == 'ListDatabase') {
      if (!_.isNil(targetJson)) {
        let list = JSON.parse(targetJson)
        // convert to client side ABCDb
        list = this.toABCDbList(list)
        store.commit('changeRepositories', list)
      }
    }
    return 'Done'
  }

  scoreTransfert(jsonStr: string) {
    console.log(`JSON received : ${jsonStr}`)
    const transferedScore = new TransferedScore(jsonStr)
    transferedScore.populate()
    return 'Done'
  }
}

const DBAPI = 'AbcRepositoryAPI'
const SCOREAPI = 'AbcIOSPeerAPI'

class AbcIOSPeerAPI {
  constructor() {}

  //
  // browing & connecting protocol starts here
  // (IOS function call)
  //
  bind() {
    console.log('Binding ABC Service request started')
    const callback = new ExternalAPI()
    callback.iosCallBack(SCOREAPI, 'bind')
  }

  populateScoreTitleBarID( id : number ) {
    console.log(`populate ScoreTitleBarID : ${id}`)
    const callback = new ExternalAPI()
    callback.iosOnly = true 
    callback.iosCallBack(SCOREAPI, 'ScoreBarID' , [id.toString()])
  }

  getScoreRepoList() {
    console.log('request IOS storage content')
    const callback = new ExternalAPI()
    callback.iosCallBack(DBAPI, 'getScoreRepoList')
  }

  createRepository(repo: ABCDb) {
    console.log(`creating repository ${repo.core.name}`)
    const callback = new ExternalAPI()
    callback.iosCallBack(DBAPI, 'createRepository', [JSON.stringify(repo)])
  }

  deleteRepository(repos: ABCDb[]) {
    console.log(`repos: ${repos} , ${repos[0]} , ${repos[0].core.name}`)
    for (const repo of repos) {
      console.log(`delete repository ${repo.core.name}`)
      // js class is rebuilt as objects sometimes ??
      const jsoned = JSON.stringify(repo)
      console.log(`jsoned= ${jsoned}`)
      const callback = new ExternalAPI()
      callback.iosCallBack(DBAPI, 'deleteRepository', [jsoned])
    }
  }

  mount(repos: ABCDb[]) {
    console.log(
      `mounting repos: ${repos} , ${repos[0]} , ${repos[0].core.name}`
    )
    const jsoned = JSON.stringify(repos)
    console.log(`jsoned= ${jsoned}`)
    const callback = new ExternalAPI()
    callback.iosCallBack(DBAPI, 'mountRepository', [jsoned])
  }

  unMount(repos: ABCDb[]) {
    console.log(
      `unmounting repos: ${repos} , ${repos[0]} , ${repos[0].core.name}`
    )
    const jsoned = JSON.stringify(repos)
    console.log(`jsoned= ${jsoned}`)
    const callback = new ExternalAPI()
    callback.iosCallBack(DBAPI, 'unmountRepository', [jsoned])
  }

  storageOperation(repos: ABCDb, dbOperation: DbOperation) {
    const jsonedRepos = JSON.stringify(repos)
    const jsonedOp = JSON.stringify(dbOperation)
    const callback = new ExternalAPI()
    callback.iosCallBack(DBAPI, 'storageOperation', [jsonedRepos, jsonedOp])
  }

  load(repos: ABCDb, filterTag: string) {
    const jsonedRepos = JSON.stringify(repos)
    const callback = new ExternalAPI()
    callback.iosCallBack(DBAPI, 'loadRepository', [jsonedRepos, filterTag])
  }
}

export {
  ExternalAPI,
  Globals,
  AbcIOSScoreTransferAPI,
  AbcIOSPeerAPI,
  FxArguments,
  ErrorEvent
}
