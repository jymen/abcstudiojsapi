/**
 * Transfered Score class
 */
import * as _ from 'lodash'
import store from '../store/store'

// Inner Communication Tool
import { EventBus } from '../event-bus'
import {
  DBType,
  ScoreDataStore,
  File,
  Folder,
  DataContent,
  ABCTreeElement,
  Leaf
} from '@common/ABCCommon'
import { ABCDb, DBCore } from './db/ABCDb'
class TransferedScore {
  score: any

  constructor(jsonScore: string) {
    this.score = JSON.parse(jsonScore)
  }

  private utf8ToB64(str: string): string {
    return window.btoa(unescape(encodeURIComponent(str)))
  }

  private b64ToUtf8(str: string): string {
    return decodeURIComponent(escape(window.atob(str)))
  }

  buildFileForInsertion(infos: Leaf, parent: Folder): File {
    const title = infos.title
    const abc = this.b64ToUtf8(infos.contents.abc)
    const leaf = new Leaf(title)
    leaf.contents = new DataContent()
    leaf.contents.abc = abc
    return new File(leaf, parent)
  }

  buildFolderForInsertion(infos: Leaf, parent: Folder): Folder {
    const leaf = new Leaf(infos.title)
    const folder = new Folder(leaf, parent)
    if (!_.isNil(folder.files)) {
      for (const file of folder.files) {
        folder.addFile(this.buildFileForInsertion(file.infos, folder).infos)
      }
    }
    if (!_.isNil(folder.folders)) {
      for (const fold of folder.folders) {
        folder.addFolder(this.buildFolderForInsertion(fold.infos, folder).infos)
      }
    }
    return folder
  }

  buildDataForInsertion(): ABCTreeElement {
    const rootData = this.score
    if (rootData.infos.title == 'EXPORT') {
      // get single file
      return this.buildFileForInsertion(rootData.files[0], rootData)
    } else {
      // build normal tree from Folder Root
      return this.buildFolderForInsertion(rootData, rootData)
    }
  }

  /**
   * Populate to current repository
   */
  populate() {
    let curRoot = store.getters.treeData
    const candidate = this.buildDataForInsertion()
    let curRepo = ABCDb.load()
    if (_.isNil(curRepo)) {
      curRepo = new DBCore('newRepository', DBType.Sqlite)
    }
    if (_.isNil(curRoot)) {
      // no Repository presently loaded
      curRoot = new ScoreDataStore()
    }
    let selectedNode = store.getters.selectedNode
    if (_.isNil(selectedNode)) {
      selectedNode = curRoot.root
      store.commit('newNodeSelected', selectedNode)
    }
    selectedNode.add(candidate)
    const curDB = new ABCDb(curRepo, curRoot)
    EventBus.$emit('RepositoryChanged', curDB)
  }
}

export { TransferedScore }
