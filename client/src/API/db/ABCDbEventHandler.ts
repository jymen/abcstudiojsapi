/*
 */
import { EventBus } from '../../event-bus'
import { ABCDb, DBCore } from '../../API/db/ABCDb'

class ABCDbEventHandler {
  constructor() {
    EventBus.$on('LoadRepository', (candidate: ABCDb) => {
      if (candidate != null) {
        console.log(`FULL repository loading: ${candidate.core.name} REQUESTED`)
        candidate.loadALL()
      }
    })
    EventBus.$on('RepositoryLoaded', (candidate: ABCDb) => {
      if (candidate != null) {
        console.log(`loading repository : ${candidate.core.name}`)
      }
    })
  }
}

export { ABCDbEventHandler }
