import { ABCDbRequestFactory } from '@/API/ABCDbRequestFactory'
import {
  DBType,
  StorageOperation,
  DatabaseDataLoader,
  ScoreDataStore
} from '@common/ABCCommon'
import { testData } from '@/API/ABCTest'
import { DBCore } from '@common/DbProtocol'

import store from '../../store/store'
import { EventBus } from '@/event-bus'

class ABCDb implements DatabaseDataLoader {
  core: DBCore
  operation: StorageOperation = null
  data: ScoreDataStore = null

  constructor(core: DBCore, data?: ScoreDataStore | null) {
    this.core = core
    this.data = data
  }

  isLoaded(): boolean {
    if (this.data != null) {
      return true
    }
    return false
  }

  loadFrom(json: string) {
    this.data = new ScoreDataStore()
    this.data.setRoot(json)
  }

  loadFromStorage() {
    this.data = new ScoreDataStore()
    this.data.fromLocalRepo(this)
  }

  loadData() {
    /* eslint-disable indent */
    switch (this.core.type) {
      case DBType.Sqlite:
        this.loadFromStorage()
        break
      case DBType.SqliteMemory:
        // Only sampleScore test available
        this.loadFrom(testData)
        break
    }
    // updata store accordingly
    /* eslint-enable indent */
    store.commit('newTreeData', this.data)
  }

  // DatabaseDataLoader
  loadALL(): void | Error {
    const loader = new ABCDbRequestFactory(this)
    loader.loadALL()
  }

  static load(): DBCore | null {
    const jsonCore: string = localStorage.repoName
    if (jsonCore == null || jsonCore == 'undefined' || jsonCore == 'null') {
      return null
    }
    const core = JSON.parse(jsonCore)
    if (core.name == null) return null
    return new DBCore(core.name, core.type)
  }

  store() {
    const jsoned = JSON.stringify(this.core)
    localStorage.setItem('repoName', jsoned)
  }
}

export { DBCore, ABCDb }
