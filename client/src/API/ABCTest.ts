/**
 * JSON Score test used here and there
 */

const testData = `
{
  "root": {
    "infos": {
      "title": "This is the root tune"
    } ,
    "files": [
      {
        "infos": {
          "title": "Tune 1 at root"
        } 
      },
      {
        "infos": {
          "title": "Tune 2 at root"
        }  
      }
    ],
    "folders": [
      {
        "infos": {
          "title": "Reels"
        },  
        "folders" : [
          {
            "infos" : {
              "title": "Slow reels folder"
            },  
            "files" :[
              {
                "infos" : {
                  "title": "Sleepy Maggie",
                  "type" : 0
                }  
              },
              {
                "infos" : {
                  "title": "Drowsie Maggie",
                  "type" : 0
                }  
              }
            ]
          },
          {
            "infos": {
              "title": "Fast Reels"
            },  
            "files" : [
              {
                "infos": {
                  "title": "The Salamanca",
                  "type" : 3
                }  
              },
              {
                "infos": {
                  "title": "The wind that shakes the barley",
                  "type" : 0
                }  
              }
            ]
          }
        ],
        "files" : [
          {
            "infos": {
              "title": "The Banshee",
              "type" : 0
            }  
          },
          {
            "infos": {
              "title": "Toss the feathers",
              "type" : 0
            }  
          }
        ]
      }
    ]
  }
}
`

const sampleScore = `
X: 1
T:Money Lost
M:3/4
L:1/8
Q:1/4=100
C:Paul Rosen
S:Copyright 2007, Paul Rosen
R:Klezmer
K:Dm
Ade|:"Dm"(f2d)e gf|"A7"e2^c4|"Gm"B>>^c BA BG|"A"A3Ade|"Dm"(f2d)e gf|"A7"e2^c4|
"Gm"A>>B "A7"AG FE|1"Dm"D3Ade:|2"Dm"D3DEF||:"Gm"(G2D)E FG|"Dm"A2F4|"Gm"B>>c "A7"BA BG|
"Dm"A3 DEF|"Gm"(G2D)EFG|"Dm"A2F4|"E°"E>>Fy "(A7)"ED^C2|1"Dm"D3DEF:|2"Dm"D6||
`

export { testData, sampleScore }
