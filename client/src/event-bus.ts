/**
 * Global vue JS event bus to use
 */

import Vue from 'vue'

export const EventBus = new Vue()
