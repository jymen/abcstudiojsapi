import $ from 'jquery'
import { initStudio } from './main'

$(document).ready(() => {
  initStudio()
})
