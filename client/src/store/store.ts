/*
VueX global store
*/

import Vue from 'vue'
import Vuex from 'vuex'
import { GetterTree, MutationTree, ActionTree } from 'vuex'
import { ABCDb } from '../API/db/ABCDb'
import { File, Folder, ScoreDataStore } from '../../../common/lib/ABCCommon'

Vue.use(Vuex)

class State {
  ABCStudioHomeInited: boolean = false
  testData = 'store has been checked OK'
  repositories: ABCDb[] | null = null // no repos on startup
  treeData: ScoreDataStore | null = null
  curNode: Folder | File | null = null
  curDb: ABCDb | null = null
  peerTransferState = 'dormant'
  newRepoContent: Folder | null = null
  scoreTitleBarID: number = 1
}

const getters: GetterTree<State, any> = {
  repositories: (state) => state.repositories,
  treeData: (state) => state.treeData,
  selectedNode: (state) => state.curNode,
  peerTransferState: (state) => state.peerTransferState,
  curDb: (state) => state.curDb,
  studioHomeInited: (state) => state.ABCStudioHomeInited ,
  titleBarID: (state) => state.scoreTitleBarID
}

const actions: ActionTree<State, any> = {}

const mutations: MutationTree<State> = {
  changeRepositories(state, repositories) {
    state.repositories = repositories
  },

  newTreeData(state, newDataStore) {
    state.treeData = newDataStore
  },

  newNodeSelected(state, newNode) {
    state.curNode = newNode
  },

  newPeerTransferState(state, newTransferState) {
    state.peerTransferState = newTransferState
  },

  newCurDb(state, db) {
    state.curDb = db
  },

  setStudioHomeInited(state, inited) {
    state.ABCStudioHomeInited = inited
  },

  incTitleBarID( state ) {
    state.scoreTitleBarID += 1
  }
}

const store = new Vuex.Store({
  state: new State(),
  mutations: mutations,
  actions: actions,
  getters: getters,
  modules: {}
})

export default store
