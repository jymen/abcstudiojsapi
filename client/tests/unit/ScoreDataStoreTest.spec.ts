import { shallowMount } from '@vue/test-utils'

import { ScoreDataStore } from '../../../common/lib/ABCCommon'

import { testData } from '../../src/API/ABCTest'
//
//
describe('Testing DataStore semantics', () => {
  //
  it('MINIMAL test : Should always pass', () => {
    const msg = 'new message'
    expect(msg).toEqual(msg)
  })

  it('test DataStore from Json', () => {
    //
    const dtStore = new ScoreDataStore()
    dtStore.fromJson(testData)
    const root = dtStore.root
    expect(root).not.toEqual(null)
  })
})
