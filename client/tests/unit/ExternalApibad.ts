import * as chai from 'chai'
import * as $ from 'jquery'

import 'chai-http'

declare global {
  namespace NodeJS {
    interface Global {
      performance: any
    }
  }
}

require('jsdom-global')()
// eslint-disable-next-line no-undef
global.performance = window.performance

chai.use(require('chai-http')) // import { shallowMount } from '@vue/test-utils'
// Assertions
chai.should()

import { ExternalAPI, FxArguments } from '../../src/API/ExternalApi'
import { testData } from '../../src/API/ABCTest'
//
//
describe('ExternalAPI testing ', () => {
  it('MINIMAL test : Should always pass', () => {
    const msg = 'new message'
    chai.expect(msg).to.include(msg)
  })

  it('call testSample through ', () => {
    //
    const api = new ExternalAPI()
    //let args = new FxArguments('abcjs', 'test', null)
    //let ret = api.entryPoint(args)
    //chai.expect(ret).not.to.have.property('error')
    //chai.expect(ret).to.have.property('data')
    //let returned = ret.fxRet
    //chai.expect(returned).to.equal('test API from AbcStudioApi')
  })
})
