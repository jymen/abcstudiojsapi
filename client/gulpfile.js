/*
  Gulp project builder
  USED ONLY FOR COPYING TO PRODUCTION ANDROID/OSX
*/
const gulp = require('gulp'),
  userHome = require('user-home'),
  del = require('del')

const paths = {
  android:
    userHome +
    '/AndroidStudioProjects/ABCStudioForAndroid/app/src/main/assets/www',
  // osx: userHome+'/temp/test/test2',
  osx: userHome + '/development/ABCMusicStudio/ABCMusicStudio/webstuff'
}

//------------------------------------ Gulp Clean before build
gulp.task('clean', function () {
  console.log(`cleaning ${paths.android}, ${paths.osx}`)
  return del([paths.android, paths.osx], { force: true })
})

gulp.task('cleanDist', function () {
  console.log('cleaning ./dist')
  return del(['./dist'])
})

gulp.task('cleantest', function () {
  console.log('cleaning test in ./dist')
  return del(['./dist/mocha', './dist/tests', './dist/mocha.html'])
})

gulp.task('copytest', function () {
  console.log(`copy TEST to dist ${paths.android}`)
  return gulp.src('./tests/**.*').pipe(gulp.dest('./dist'))
})

gulp.task('copyprod', function () {
  console.log(`copy to Android production ${paths.android}`)
  gulp.src('./dist/**').pipe(gulp.dest(paths.android))
  console.log(`copy to IOS/OSX production ${paths.osx}`)
  return gulp.src('./dist/**').pipe(gulp.dest(paths.osx))
})

gulp.task('default', gulp.series('copyprod'))
gulp.task('tests', gulp.series('copytest'))
