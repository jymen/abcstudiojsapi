const score1 = `
M:6/8
O:I
R:J

X:1
T:Paddy O'Rafferty
C:Trad.
K:D
dff cee|def gfe|dff cee|dfe dBA|dff cee|def gfe|faf gfe|1 dfe dBA:|2 dfe dcB|]
~A3 B3|gfe fdB|AFA B2c|dfe dcB|~A3 ~B3|efe efg|faf gfe|1 dfe dcB:|2 dfe dBA|]
fAA eAA|def gfe|fAA eAA|dfe dBA|fAA eAA|def gfe|faf gfe|dfe dBA:|


        `

const score2 = `

X:1
T:Paddy O'Rafferty
C:Trad.
K:D
dff cee|def gfe|dff cee|dfe dBA|dff cee|def gfe|faf gfe|1 dfe dBA:|2 dfe dcB|]
~A3 B3|gfe fdB|AFA B2c|dfe dcB|~A3 ~B3|efe efg|faf gfe|1 dfe dcB:|2 dfe dBA|]
fAA eAA|def gfe|fAA eAA|dfe dBA|fAA eAA|def gfe|faf gfe|dfe dBA:|


X:2
T:Kitchen Girl
C:Trad.
E:8
K:D
[c4a4] [B4g4]|efed c2cd|e2f2 gaba|g2e2 e2fg|a4 g4|efed cdef|g2d2 efed|c2A2 A4:|
K:G
ABcA BAGB|ABAG EDEG|A2AB c2d2|e3f edcB|ABcA BAGB|ABAG EGAB|cBAc BAG2|A4 A4:|
`

let xmlScore1 = null
let xmlScore2 = null


let currentXmlScore = xmlScore1
let currentScore = score1
let tuneNumber = 1
let highlight = true

// eslint-disable-next-line no-unused-vars
function abcButtonClicked() {
  if ( currentScore == score1 ) {
    currentScore = score2
    tuneNumber = 1
  } else {
    currentScore = score1
    tuneNumber = 0
  }

  console.log('button clicked')
  let args = [
    currentScore,
    tuneNumber,
  ];
  let fxArguments = {
    'className': 'abcjs',
    'fx': 'showScore',
    'fxArgs': args,
  };
  abcStudioAPI.externalEntryPoint(fxArguments);
}

// eslint-disable-next-line no-unused-vars
function xmlButtonClicked() {
  if ( currentXmlScore == xmlScore1 ) {
    currentXmlScore = xmlScore2
  } else {
    currentXmlScore = xmlScore1
  }

  console.log('xml button clicked')
  let args = [
    currentXmlScore,
  ];
  let fxArguments = {
    'className': 'xmlMusic',
    'fx': 'showScore',
    'fxArgs': args,
  };
  abcStudioAPI.externalEntryPoint(fxArguments);
}

// eslint-disable-next-line no-unused-vars
function highlightButtonClicked() {
  console.log('hilight button clicked')
  let args = [
    312 , // start text pos
    313 , // end
    '#007acc' , // highlight color
    highlight , // highlight
  ]
  let fxArguments = {
    'className': 'abcjs',
    'fx': 'highlightNote',
    'fxArgs': args,
  };
  abcStudioAPI.externalEntryPoint(fxArguments);
}

$(document).ready(function() {
  // Since music xml file are huge we load sample from file
  $.get('./tests/testdata/WallsOfLiscarrol.txt', function(data) {
    xmlScore1 = data
  })
  $.get('./tests/testdata/SwallowTailJig.txt', function(data) {
    xmlScore2 = data
  })
});


