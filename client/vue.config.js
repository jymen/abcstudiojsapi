const path = require('path')

module.exports = {
  publicPath: '',
  runtimeCompiler: true,
  configureWebpack: {
    devtool: 'source-map',
    output: {
      filename: 'bundle_[name].js'
    },
    performance: {
      hints: false
    }
  },
  pages: {
    index: {
      entry: 'src/main.ts',
      template: 'public/index.html',
      filename: 'index.html'
    },
    studio: {
      entry: 'src/mainStudio.ts',
      template: 'public/indexStudio.html',
      filename: 'indexStudio.html'
    }
  },
  chainWebpack: (config) => {
    /*
    config.plugin('html').tap((args) => {
      console.log('TEST=' + process.env.TEST)
      if (process.env.TEST) {
        console.log('YOU ARE IN MOCHA TEST MODE')
        args[0].template = './public/mocha.html'
      }
      console.log('NODE_ENV:' + process.env.NODE_ENV)
      return args
    })
    */

    config.resolve.alias.set(
      '@common',
      path.resolve(__dirname, '../common/lib/')
    )
  },

  pluginOptions: {
    browserSync: {
      files: [
        'dist/bundle.js',
        'public/tests/*.js',
        'src/*.js',
        'src/*.mjs',
        '../common/*.mjs',
        'src/components/*.js'
      ],
      browser: 'google chrome'
    },
    'style-resources-loader': {
      preProcessor: 'scss',
      patterns: [path.resolve(__dirname, './src/sass/global.scss')]
    }
  }
}
