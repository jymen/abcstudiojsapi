module.exports = {
  root: true,
  "parserOptions": {
    "ecmaVersion": 7,
    "sourceType": "module"
  },

  env: {
    node: true,
    "es6": true,
  },
  extends: [
    "plugin:vue/essential",
    "eslint:recommended",
    "@vue/typescript/recommended",
    "@vue/prettier",
    "@vue/prettier/@typescript-eslint",
    // "plugin:prettier/recommended",
  ],

  rules: {
    semi: ['error', 'never'],
    'no-console': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
    'no-debugger': process.env.NODE_ENV === 'production' ? 'warn' : 'off',
    'max-len': [1, 120, 2, { ignoreComments: true }],
    'no-const-assign': 'warn',
    'no-this-before-super': 'warn',
    'no-undef': 'warn',
    'no-unreachable': 'warn',
    'no-unused-vars': 'warn',
    'constructor-super': 'warn',
    'valid-typeof': 'warn',
    'no-extra-semi': 'off',
    'indent': 'off',
    'linebreak-style': ['error', 'unix'],
    quotes: ['error', 'single'],
    'comma-dangle': ['error', 'always-multiline'],
    'no-cond-assign': ['error', 'always'],
    'import/prefer-default-export': 'off',
    'comma-dangle': 'off',
    'import/extensions': 'off',
    'no-unused-vars': 'off',
    'no-reserved-keys': 'off',
    '@typescript-eslint/no-explicit-any': 'off',
    '@typescript-eslint/no-empty-function': 'off',
    '@typescript-eslint/explicit-module-boundary-types': 'off',
    '@typescript-eslint/no-var-requires': 'off',
    '@typescript-eslint/no-this-alias': 'off',
    '@typescript-eslint/no-use-before-define': 'off',
    '@typescript-eslint/no-empty-interface': 'off',
    '@typescript-eslint/indent': ['off'],
    'vue/script-indent': ['off'],
    "no-unused-vars": [
      "warn",
      {
        "vars": "all",
        "varsIgnorePattern": "[I]\\w+"
      }
    ]

  },

  /*
  globals: {
    it: false,
    describe: false,
    SyntheticEvent: false,
    test: true,
    expect: true,
    Vex: true,
    ABCVexFlow: true,
    ABCJS: true,
    $: true,
    QUnit: true,
    abcStudioAPI: true,
    _: true,
    chai: true,
  },

  parserOptions: {
    parser: 'babel-eslint',
    ecmaFeatures: {
      legacyDecorators: true
    },
    ecmaVersion: 2020
  },

  'extends': [
    'plugin:vue/essential',
    'eslint:recommended',
    '@vue/typescript/recommended',
    '@vue/prettier',
    '@vue/prettier/@typescript-eslint'
  ]
  */
}
