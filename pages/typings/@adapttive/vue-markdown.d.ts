import { Vue } from 'vue-property-decorator'

declare module 'vue-markdown' {
  export type VueMarkdown = Vue.Component
}
