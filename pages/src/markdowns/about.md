# About ABCMusicStudio

<div class="PageTopImage">
  <img src="./images/Screenshot_audio.png" alt="audio screenshot" />
</div>


## ABCMusicStudio is a MACOSX music editor dedicated to ABC score music formatting.

I am a traditional celtic fiddler , banjoist and mandolinist and a macosx developper as well.

I am the developer of ABCMusicStudio ; and at the beginning of 2020 , I started crafting ABCMusicStudio for my own usage first.

The reasons why I started :

- As an irish,Breton music player I was heavily using ABC standard music notation since most of traditional music repositories are ABC based.
- ABC standard is a very concise easy to use notation compared to XMLMusic format which is too verbose.
- As a macosx programmer I was willing also to build a tool based on MAC cocoa graphical standard and ergonomy.
- As time goes on I have enhanced the tool in order to be able also to attach other elements to scores : Pdf notes or images and MP3 sound
