# Contributing and technical details

<div class="PageTopImage">
  <img src="./images/Screenshot_coateval.png" alt="extra screenshot" />
</div>

## So you want to contribute to ABCMusicStudio development.


Of course contributions to ABCMusicStudio are welcomed and you'll find bellow the necessary technical details       
<br>
**Q : How technically send code updates  ?**

Code source update is accepted through **pull requests** on repositories on the dev branch.
The main branch is containing the last stable distributed version => When the dev branch is stable enough the dev branch is merged on the main trunk the main trunk is tagged, and a new version of the MACOS app is made available on the distribution site at : www.jymengant.org
<br>
**Q : Where is the git repos located ?**

ABCMusicStudio is using multiple technologies which implies to have 2 repositories hosted by Gitlab :

- [MacOs Cocoa Swift / Objective C stuff repository](https://gitlab.com/jymen/abcmusicstudio) named ABCMusicStudio
- [TypeScript Vue NodeJs stuff repository](https://gitlab.com/jymen/abcstudiojsapi) named ABCStudioJSAPI 
  
More details below
<br>
**Q : More details about product architecture ...**

ABCMusicStudio Graphical User Interface has two main parts :

- Score View : Which is an Hosted browser view (a WKWebView instance) which contains Typescript Vue components which uses the [abcjs library developped by Paul Rosen ](https://www.abcjs.net) (To which I am also contributiing from times to times). This typescript part is managed through NodeJs Vue projects(I am using visual studio as editor for this part)
- Score Tree , Abc editor ... and other components which are pure Cocoa Swift Controllers NIB or SwiftUI components which are managed through XCODE base workspace and some associated [CocoaPods ](https://www.cocoapods.org) (see podfile in project for more info about used pods)
<br>

**Q : How to build product ?**

The product is built in two phases :

- PHASE 1 : Building Javascript Vue componnents 
  - clone abcjs git repository (www.abcjs.net) (use dev branch)
  - clone ABCStudioJSAPI git repository 
  - cd to ABCStudioJSAPI 
  - run : yarn build install + yarn build prod   

- PHASE 2 : Building Javascript Vue componnents 
  - install cocoapods
  - clone ABCMusicStudio git repository 
  - cd to ABCMusicStudio
  - run pod install 
  - build workspace through XCODE as usual

**NB :** You only need to deal with PHASE 1 only if you made changes in Typescript VUE code ; the last production JS Bundle is automatically copied in XCODE project 'webstuf' directory    
