# Download ABCMusicStudio

<div class="PageTopImage">
  <img src="./images/Screenshot_light.png" alt="audio screenshot" />
</div>


## Click [HERE](./distribution/ABCMusicStudio.zip) to download ABCMusicStudio

**Install as follow  :**

- Using Finder double click on downloaded zip file to uncompress it.
- Drag and drop Unzipped ABCMusicStudio.App to Applications directory.
- you're done click on ABCMusicStudio App to start it
