# Some external links

<div class="PageTopImage">
  <img src="./images/Screenshot_preferences.png" alt="preferences screenshot" />
</div>

## you can Customize colors and fonts through preferences.

This page contains technical links to components I have been using to build ABCMusicStudio.


## Technical links about library used by ABCMusicStudio 

* [AbcJs](https://abcjs.net) javascript ABC viewer 
* [Swift](https://swift.org) language used for macOS development 
* [TypeScript](https://www.typescriptlang.org) Modern extension to javascript language 
* [CocoaLumberJack](https://cocoalumberjack.github.io) logging library for Objective C and Swift
* [Node.js](https://nodejs.org) Javascript runtime
* [Vue](https://vuejs.org) Javascript/typescript framework

## Musical links around ABC notation

* [ABC](https://abcnotation.com) Music notation standard
* [TheSession](https://thesession.org) Traditional irish scores in ABC notation
* [Folk tune Abc finder](http://www.folktunefinder.com) ABC Score search engine
* [MuseScore](https://musescore.org/) Music composition notation software
* [MusicXml](https://www.musicxml.com) MusicXml standard notation