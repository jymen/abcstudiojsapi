# Frequently Asked Questions

<div class="PageTopImage">
  <img src="./images/Screenshot_extra.png" alt="extra screenshot" />
</div>

## You can add extra Pdf or Images to ABC catalog.

**Q  : is ABCMusicStudio safe to install on MAC ?**

As a Apple certified developer I am following Apple software security rules and I am submitting ABCMusicStudio to Apple security notarization process which generate a safe signature protecting again viruses and trojans      

**Q : On Which version of MacOS can I install ABCMusicStudio ?**

Minimum level os MacOS for ABCMusicStudio is 11.0

**Q : Is ABCMusicStudio available for other OSes ?**

Although some part of ABCMusicStudio are portables it is only available for MACOs platforms since the current Graphical User Interface is targeted to Cocoa(MacOS GUI)
A port to IOS for Ipad will be done later ... But not available yet.

**Q : What are the features in progress or not fully completed ?**

- French translations is not fully completed
- Storage of catalog in Database 
- Score transfert to IPhone/IPad IOS
