# Documentation User Guide

<div class="PageTopImage">
   <iframe height="350" width="600"
      src="https://www.youtube.com/embed/veD4L5goNpM">
   </iframe>
</div>

## A Tiny Video as Primer Guide.

The short eight minutes video Above will introduce you to the main functionalities of ABCMusicStudio.

## Complementary tips and tricks

**Q : where are newly created scores stored on MAC ?**

When dragging ABC files from disk into MuseScore catalog, the original location on disk is used and kept.


When creating new ABC score using 'new Score' menu or dragging **MuseScore** or **MusicXml** Files a new .abc score file is created inside ABCMusicStudio home directory located at : $HOME/Library/Application Support/ABCMusicStudio/abcdata/**CATALOGNAME** where **CATALOGNAME** is the name of the catalog where 'new score' is belonging to  

