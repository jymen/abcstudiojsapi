# What's inside V1.0.3

<div class="PageTopImage">
  <img src="../images/Screenshot_light.png" alt="light screeshot" />
</div>


## Custom coloring using light or black theme

V1.0.3 is the first public version made available for ABCMusicStudio
( last build is : BUILD 5)

*   ABC Music standard Editor 
*   Syntax validation
*   Opening XmlMusic and MuseScore files automatically convert them to ABC
*   MIDI player with note progress coloring 
*   Custom coloring scheme 
*   English & French language support
*   Drag & drop ABC , XmlMusic and MuseScore format accepted
*   SVG Score rendering 
*   Score transposition 
*   Score templates definitions
*   Drag and drop MP3 , AIFF ... recordings
*   Drag and Pdf and Images as complementary documents to score
