/*
Vue js configuration stuff
*/
const path = require('path')

module.exports = {
  publicPath: process.env.NODE_ENV === 'production' ? '/abcstudiojsapi/' : '/',
  configureWebpack: {
    devtool: 'source-map',
    output: {
      filename: 'bundle_pages.js'
    },
    performance: {
      hints: false
    }
  },

  //
  chainWebpack: (config) => {
    if (process.env.NODE_ENV === 'production') {
      // mutate config for production...
      console.log('production')
    } else {
      // mutate for development...
      console.log('development')
    }
    config.module
      .rule('raw')
      .test(/\.md$/)
      .use('raw-loader')
      .loader('raw-loader')
      .end()
  },

  pluginOptions: {
    browserSync: {
      files: [
        'dist/bundle_page.js',
        'public/tests/*.js',
        'src/*.js',
        'src/*.mjs',
        '../common/*.mjs',
        'src/components/*.js'
      ],
      browser: 'google chrome'
    },
    'style-resources-loader': {
      preProcessor: 'scss',
      patterns: [path.resolve(__dirname, './src/sass/glbal.scss')]
    }
  }
}
